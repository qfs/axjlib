# AXJLib

Library to use the macOS accessibility functions from Java.

Copyright 2018 by QFS, Pascal Bihler

## Usage

There are two abstraction levels available:
* **AXJLib** - Low level method calls to the macOS API
* **AXAPI** - Static high level API to interact with the UI Elements

### Example AXAPI

```java
System.out.println("Trusted? " + AXAPI.isProcessTrusted(true));

{
    final AXSystemWide sw = AXAPI.getSystemWideAccessibilityObject();
    final AXApplication focusedApplication = sw.getFocusedApplication();
    System.out.println(focusedApplication);
    if (focusedApplication != null) {
        final AXMenuBar menuBar = focusedApplication.getMenuBar();
        System.out.println(menuBar);
    }
}

{
    final AXUIElement element = AXAPI.elementAtPosition(100, 100);
    System.out.println(element);
    System.out.println(element.getAttributeNames());
    final AXWindow window = element instanceof AXWindow ? (AXWindow) element : (AXWindow) element.getWindow();
    window.setSize(new CGSize(600,600));
    window.setMinimized(true);
}

{
    for(final NSRunningApplication app: AXJLib.INSTANCE.runningApplications()) {
        System.out.println(app);
    }
}

{
    NSRunningApplication[] finders = AXJLib.INSTANCE.runningApplicationsWithBundleIdentifier("com.apple.finder");
    int pid = Integer.valueOf(finders[0].getProcessIdentifier());
    final AXApplication app = AXAPI.getApplicationAccessibilityObject(pid);
    app.setFrontmost(true);
    final List<AXUIElement> windows = app.getWindows();
    System.out.println(windows);
    final AXUIElement window = windows.get(0);
    if (window instanceof AXWindow) {
        ((AXWindow)window).raise();
    }
}
```

## Build & Develop

Required:
* XCode (with command line tools installed)
* Java 1.8
* Eclipse (optional, for easier development)
* Environment variable `$JAVA_HOME` set to your JDK Homedir, e.g. `/Library/Java/JavaVirtualMachines/jdk1.8.0.jdk/Contents/Home`

### Commands
* `./gradlew compileJava` - compiles the java sources
* `./gradlew nativeHeaders` - regenerate the header files for the JNI calls
* `./gradlew nativeLib` - creates the libaxjlib.dylib using `/usr/bin/xcodebuild`
* `./gradlew nativeLibLocal` - creates the libaxjlib.dylib and copies it to the base directory, so it is easily loadable during tests
* `./gradlew test` - runs the tests
* `./gradlew javadoc` - generates javadoc in `build/docs`
* `./gradlew build` - builds the final jar in `buid/libs`
* `./gradlew qbuild` - builds the final jar and copys it to `../../libs/axjlib.jar`

## License

The code is licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0).

## Acknowledgements

* Native test running with the sample application from https://developer.apple.com/library/content/samplecode/AccessibleTicTacToe
