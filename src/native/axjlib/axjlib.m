/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
//
//  axjlib.m
//  libaxjlib
//
//  Created by Pascal Bihler on 07.05.18.
//  Copyright © 2018 Pascal Bihler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <JavaNativeFoundation/JavaNativeFoundation.h>

#include "axjlib.h"
#include "javatypes.h"


jint throwIllegalArgumentException( JNIEnv *env, char *message, ...)
{
    jclass exClass;
    char *className = "java/lang/IllegalArgumentException";

    exClass = (*env)->FindClass( env, className);
    if (exClass == NULL) return 0;

    char msgBuf[255];
    va_list args;
    va_start(args, message);
    vsnprintf(msgBuf, sizeof(msgBuf), message, args);
    va_end(args);

    return (*env)->ThrowNew( env, exClass, msgBuf );
}



JNIEXPORT jboolean JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXIsProcessTrustedWithOptions
(JNIEnv * env, jobject that, jlong options) {
    JNF_COCOA_ENTER(env);
    Boolean result = AXIsProcessTrustedWithOptions((CFDictionaryRef) options);
    return (jboolean) result;
    JNF_COCOA_EXIT(env);
    return (jboolean) NULL;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_kAXTrustedCheckOptionPrompt
(JNIEnv * env, jobject that) {
    return (jlong) kAXTrustedCheckOptionPrompt;
}


JNIEXPORT jboolean JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXIsProcessTrusted
(JNIEnv * env, jobject that) {
    JNF_COCOA_ENTER(env);
    Boolean result = AXIsProcessTrusted();
    return (jboolean) result;
    JNF_COCOA_EXIT(env);
    return (jboolean) NULL;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) AXUIElementGetTypeID();
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCopyAttributeNames
(JNIEnv * env, jobject that, jlong element, jlongArray cfArrayRefArray) {
    JNF_COCOA_ENTER(env);
    jlong *p_cfArrayRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jsize len = cfArrayRefArray ? (*env)->GetArrayLength(env, cfArrayRefArray) : 0;
    if (len != 1) return throwIllegalArgumentException(env, "Argument 2 must be a 1-element array");
    
    jboolean isCfArrayRefArrayCopy;
    if (cfArrayRefArray) p_cfArrayRefArray = (*env)->GetLongArrayElements(env, cfArrayRefArray, &isCfArrayRefArrayCopy);
    
    result = AXUIElementCopyAttributeNames((AXUIElementRef) element, (CFArrayRef *) p_cfArrayRefArray);
    
    if (cfArrayRefArray && p_cfArrayRefArray) (*env)->ReleaseLongArrayElements(env, cfArrayRefArray, p_cfArrayRefArray, isCfArrayRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCopyAttributeValue
(JNIEnv * env, jobject that, jlong element, jlong attribute, jlongArray valueRefArray) {
    JNF_COCOA_ENTER(env);
    jlong *p_valueRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jsize len = valueRefArray ? (*env)->GetArrayLength(env, valueRefArray) : 0;
    if (len != 1) return throwIllegalArgumentException(env, "Argument 3 must be a 1-element array");
    
    jboolean isValueRefArrayCopy;
    if (valueRefArray) p_valueRefArray = (*env)->GetLongArrayElements(env, valueRefArray, &isValueRefArrayCopy);
    
    result = AXUIElementCopyAttributeValue((AXUIElementRef) element, (CFStringRef) attribute, (CFTypeRef *) p_valueRefArray);
    
    if (valueRefArray && p_valueRefArray) (*env)->ReleaseLongArrayElements(env, valueRefArray, p_valueRefArray, isValueRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementIsAttributeSettable
(JNIEnv * env, jobject that, jlong element, jlong attribute, jbooleanArray cfBooleanRefArray) {
    JNF_COCOA_ENTER(env);
    jboolean *p_cfBooleanRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jsize len = cfBooleanRefArray ? (*env)->GetArrayLength(env, cfBooleanRefArray) : 0;
    if (len != 1) return throwIllegalArgumentException(env, "Argument 3 must be a 1-element array");
    
    jboolean isCfBooleanRefArrayCopy;
    if (cfBooleanRefArray) p_cfBooleanRefArray = (*env)->GetBooleanArrayElements(env, cfBooleanRefArray, &isCfBooleanRefArrayCopy);
    
    result = AXUIElementIsAttributeSettable((AXUIElementRef) element, (CFStringRef) attribute, (Boolean *) p_cfBooleanRefArray);
    
    if (cfBooleanRefArray && p_cfBooleanRefArray) (*env)->ReleaseBooleanArrayElements(env, cfBooleanRefArray, p_cfBooleanRefArray, isCfBooleanRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementSetAttributeValue
(JNIEnv * env, jobject that, jlong element, jlong attribute, jlong value) {
    JNF_COCOA_ENTER(env);
    AXError result = kAXErrorIllegalArgument;
    
    result = AXUIElementSetAttributeValue((AXUIElementRef) element, (CFStringRef) attribute, (CFTypeRef) value);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCopyParameterizedAttributeNames
(JNIEnv * env, jobject that, jlong element, jlongArray cfArrayRefArray) {
    JNF_COCOA_ENTER(env);
    jlong *p_cfArrayRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jboolean isCfArrayRefArrayCopy;
    if (cfArrayRefArray) p_cfArrayRefArray = (*env)->GetLongArrayElements(env, cfArrayRefArray, &isCfArrayRefArrayCopy);
    
    result = AXUIElementCopyParameterizedAttributeNames((AXUIElementRef) element, (CFArrayRef *) p_cfArrayRefArray);
    
    if (cfArrayRefArray && p_cfArrayRefArray) (*env)->ReleaseLongArrayElements(env, cfArrayRefArray, p_cfArrayRefArray, isCfArrayRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCopyActionNames
(JNIEnv * env, jobject that, jlong element, jlongArray namesRefArray) {
    JNF_COCOA_ENTER(env);
    jlong *p_namesRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jsize len = namesRefArray ? (*env)->GetArrayLength(env, namesRefArray) : 0;
    if (len != 1) return throwIllegalArgumentException(env, "Argument 2 must be a 1-element array");
    
    jboolean isNamesRefArrayCopy;
    if (namesRefArray) p_namesRefArray = (*env)->GetLongArrayElements(env, namesRefArray, &isNamesRefArrayCopy);
    
    result = AXUIElementCopyActionNames((AXUIElementRef) element, (CFArrayRef *) p_namesRefArray);
    
    if (namesRefArray && p_namesRefArray) (*env)->ReleaseLongArrayElements(env, namesRefArray, p_namesRefArray, isNamesRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
    
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCopyActionDescription
(JNIEnv * env, jobject that, jlong element, jlong actionName, jlongArray descRefArray) {
    JNF_COCOA_ENTER(env);
    jlong *p_descRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jsize len = descRefArray ? (*env)->GetArrayLength(env, descRefArray) : 0;
    if (len != 1) return throwIllegalArgumentException(env, "Argument 3 must be a 1-element array");
    
    jboolean isDescRefArrayCopy;
    if (descRefArray) p_descRefArray = (*env)->GetLongArrayElements(env, descRefArray, &isDescRefArrayCopy);
    
    result = AXUIElementCopyActionDescription((AXUIElementRef) element, (CFStringRef) actionName, (CFStringRef *) p_descRefArray);
    
    if (descRefArray && p_descRefArray) (*env)->ReleaseLongArrayElements(env, descRefArray, p_descRefArray, isDescRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementPerformAction
(JNIEnv * env, jobject that, jlong element, jlong actionName) {
    JNF_COCOA_ENTER(env);
    AXError result = kAXErrorIllegalArgument;
    
    result = AXUIElementPerformAction((AXUIElementRef) element, (CFStringRef) actionName);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCopyElementAtPosition
(JNIEnv * env, jobject that, jlong applicationRef, jfloat x, jfloat y, jlongArray elementRefArray) {
    JNF_COCOA_ENTER(env);
    jlong *p_elementRefArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jboolean isElementRefArrayCopy;
    if (elementRefArray) p_elementRefArray = (*env)->GetLongArrayElements(env, elementRefArray, &isElementRefArrayCopy);
    
    result = AXUIElementCopyElementAtPosition((AXUIElementRef) applicationRef, x, y, (AXUIElementRef *) p_elementRefArray);
    
    if (elementRefArray && p_elementRefArray) (*env)->ReleaseLongArrayElements(env, elementRefArray, p_elementRefArray, isElementRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCreateApplication
(JNIEnv * env, jobject that, jint pid) {
    JNF_COCOA_ENTER(env);
    AXUIElementRef result = AXUIElementCreateApplication((pid_t) pid);
    return (jlong) result;
    JNF_COCOA_EXIT(env);
    return (jlong) 0;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementCreateSystemWide
(JNIEnv * env, jobject that) {
    JNF_COCOA_ENTER(env);
    AXUIElementRef result = AXUIElementCreateSystemWide();
    return (jlong) result;
    JNF_COCOA_EXIT(env);
    return (jlong) 0;
}

JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementGetPid
(JNIEnv * env, jobject that, jlong elementRef, jintArray pidArray) {
    JNF_COCOA_ENTER(env);
    jint *p_pidArray=NULL;
    AXError result = kAXErrorIllegalArgument;
    
    jboolean isPidRefArrayCopy;
    if (pidArray) p_pidArray = (*env)->GetIntArrayElements(env, pidArray, &isPidRefArrayCopy);
    
    result = AXUIElementGetPid((AXUIElementRef) elementRef, (pid_t *) p_pidArray);
    
    if (pidArray && p_pidArray) (*env)->ReleaseIntArrayElements(env, pidArray, p_pidArray, isPidRefArrayCopy ? 0 : JNI_ABORT);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}


JNIEXPORT jint JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXUIElementSetMessagingTimeout
(JNIEnv * env, jobject that, jlong elementRef, jfloat timeout) {
    JNF_COCOA_ENTER(env);
    AXError result = kAXErrorIllegalArgument;
    
    result = AXUIElementSetMessagingTimeout((AXUIElementRef) elementRef, (float) timeout);
    
    return (jint) result;
    JNF_COCOA_EXIT(env);
    return (jint) kAXErrorFailure;
}

/*
 * AXValue methods
 */

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXValueGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) AXValueGetTypeID();
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXValueCreate
(JNIEnv * env, jobject that, jobject value) {
    JNF_COCOA_ENTER(env);
    
    if (! value) return throwIllegalArgumentException(env, "value must not be null");
    
    if (cacheCGPointClassInfo(env) && (*env)->IsInstanceOf(env, value, CGPointCls.clazz)) {
        CGPoint point;
        point.x = (CGFloat)(*env)->GetFloatField(env, value, CGPointCls.x);
        point.y = (CGFloat)(*env)->GetFloatField(env, value, CGPointCls.y);
        
        return (jlong) AXValueCreate((AXValueType) kAXValueCGPointType, &point);
    } else if (cacheCGSizeClassInfo(env) && (*env)->IsInstanceOf(env, value, CGSizeCls.clazz)) {
        CGSize size;
        
        size.width = (CGFloat)(*env)->GetFloatField(env, value, CGSizeCls.width);
        size.height = (CGFloat)(*env)->GetFloatField(env, value, CGSizeCls.height);

        return (jlong) AXValueCreate((AXValueType) kAXValueCGSizeType, &size);
    } else if (cacheCGRectClassInfo(env) && (*env)->IsInstanceOf(env, value, CGRectCls.clazz)) {
        CGRect rect;

        jobject origin = (*env)->GetObjectField(env, value, CGRectCls.origin);
        if (origin == NULL) return 0;

        jobject size = (*env)->GetObjectField(env, value, CGRectCls.size);
        if (size == NULL) return 0;


        rect.origin.x = (CGFloat)(*env)->GetFloatField(env, origin, CGPointCls.x);
        rect.origin.y = (CGFloat)(*env)->GetFloatField(env, origin, CGPointCls.y);
        rect.size.width = (CGFloat)(*env)->GetFloatField(env, size, CGSizeCls.width);
        rect.size.height = (CGFloat)(*env)->GetFloatField(env, size, CGSizeCls.height);

        return (jlong) AXValueCreate((AXValueType) kAXValueCGRectType, &rect);
    } else if (cacheCFRangeClassInfo(env) && (*env)->IsInstanceOf(env, value, CFRangeCls.clazz)) {
        CFRange range;

        range.location = (CFIndex)(*env)->GetLongField(env, value, CFRangeCls.location);
        range.length = (CFIndex)(*env)->GetLongField(env, value, CFRangeCls.length);

        return (jlong) AXValueCreate((AXValueType) kAXValueCFRangeType, &range);
    }
    
    return throwIllegalArgumentException(env, "value must be an instance of AXValue");
    JNF_COCOA_EXIT(env);
    return 0;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXValueGetType
(JNIEnv * env, jobject that, jlong valueRef) {
    JNF_COCOA_ENTER(env);
    AXValueType type = AXValueGetType((AXValueRef) valueRef);
    return (jlong) type;
    
    JNF_COCOA_EXIT(env);
    return (jlong) kAXValueIllegalType;
}

JNIEXPORT jobject JNICALL Java_de_qfs_lib_axjlib_AXJLib_AXValueGetValue
(JNIEnv * env, jobject that, jlong valueRef) {
    JNF_COCOA_ENTER(env);
    
    Boolean result = NO;
    jobject newObject = NULL;
    
    AXValueType valueType = AXValueGetType((AXValueRef) valueRef);
    
    switch (valueType) {
        case kAXValueCGPointType: {
            CGPoint point;
            result = AXValueGetValue((AXValueRef) valueRef, (AXValueType) valueType, &point);
            if (result) {
                if (! cacheCGPointClassInfo(env)) return NULL;
                
                CGFloat x = point.x;
                CGFloat y = point.y;
                newObject = (*env)->NewObject(env, CGPointCls.clazz, CGPointCls.constructor, (jfloat) x, (jfloat) y);
            }
            break;
        }
        case kAXValueCGSizeType: {
            CGSize size;
            result = AXValueGetValue((AXValueRef) valueRef, (AXValueType) valueType, &size);
            if (result) {
                if (! cacheCGSizeClassInfo(env)) return NULL;
                
                CGFloat width = size.width;
                CGFloat height = size.height;

                newObject = (*env)->NewObject(env, CGSizeCls.clazz, CGSizeCls.constructor, (jfloat) width, (jfloat) height);
            }
            break;
        }
        case kAXValueCGRectType: {
            CGRect rect;
            result = AXValueGetValue((AXValueRef) valueRef, (AXValueType) valueType, &rect);
            if (result) {
                if (! cacheCGRectClassInfo(env)) return NULL;
                
                CGFloat x = rect.origin.x;
                CGFloat y = rect.origin.y;
                CGFloat width = rect.size.width;
                CGFloat height = rect.size.height;

                newObject = (*env)->NewObject(env, CGRectCls.clazz, CGRectCls.constructor, (jfloat) x, (jfloat) y, (jfloat) width, (jfloat) height);
            }
            break;
        }
        case kAXValueCFRangeType: {
            CFRange range;
            result = AXValueGetValue((AXValueRef) valueRef, (AXValueType) valueType, &range);
            if (result) {
                if (! cacheCFRangeClassInfo(env)) return NULL;

                CFIndex location = range.location;
                CFIndex length = range.length;

                newObject = (*env)->NewObject(env, CFRangeCls.clazz, CFRangeCls.constructor, (jlong) location, (jlong) length);
            }
            break;
        }
        default: newObject = NULL;
    }
done:
    
    return (jobject) newObject;
    JNF_COCOA_EXIT(env);
    return (jobject) NULL;
}


/*
 * Helper methods to create / interact with macOS datatypes
 *
 */

JNIEXPORT void JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFRetain
(JNIEnv * env, jobject that, jlong cfRef) {
    if (! cfRef) return;
    JNF_COCOA_ENTER(env);
    CFRetain((CFTypeRef) cfRef);
    JNF_COCOA_EXIT(env);
}

JNIEXPORT void JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFRelease
(JNIEnv * env, jobject that, jlong cfRef) {
    if (! cfRef) return;
    JNF_COCOA_ENTER(env);
    CFRelease((CFTypeRef) cfRef);
    JNF_COCOA_EXIT(env);
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFGetRetainCount
(JNIEnv * env, jobject that, jlong cfRef) {
    if (! cfRef) return 0;
    JNF_COCOA_ENTER(env);
    return (jlong) CFGetRetainCount((CFTypeRef) cfRef);
    JNF_COCOA_EXIT(env);
    return -1;
}


JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFStringCreateWithJavaString
(JNIEnv * env, jobject that, jstring javaString) {
    JNF_COCOA_ENTER(env);
    NSString* nativeString = [JNFJavaToNSString(env, javaString) retain];
    CFStringRef cfString = (__bridge CFStringRef)nativeString;
    return (jlong) cfString;
    JNF_COCOA_EXIT(env);
    return 0;
}


JNIEXPORT jstring JNICALL Java_de_qfs_lib_axjlib_AXJLib_StringCreateWithCFStringRef
(JNIEnv * env, jobject that, jlong cfStringRef) {
    JNF_COCOA_ENTER(env);
    // We don't free this reference by calling DeleteLocalRef because it's when it's passed back to Java.
    return (jstring) JNFNSToJavaString(env, (__bridge NSString*)cfStringRef);
    JNF_COCOA_EXIT(env);
    return NULL;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFBooleanCreate
(JNIEnv * env, jobject that, jboolean booleanValue) {
    JNF_COCOA_ENTER(env);
    NSNumber * nativeNumber = [[NSNumber numberWithBool:(booleanValue ? YES : NO)] retain];
    CFBooleanRef cfBooleanRef = (__bridge CFBooleanRef)nativeNumber;
    return (jlong) cfBooleanRef;
    JNF_COCOA_EXIT(env);
    return 0;
}

JNIEXPORT jboolean JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFBooleanGetValue
(JNIEnv * env, jobject that, jlong cfBooleanRef) {
    JNF_COCOA_ENTER(env);
    if (! cfBooleanRef) return (jboolean) NO;
    Boolean booleanValue = CFBooleanGetValue((CFBooleanRef)cfBooleanRef);
    return (jboolean) booleanValue;
    JNF_COCOA_EXIT(env);
    return (jboolean) 0;
}


JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFNumberCreateWithJavaNumber
(JNIEnv * env, jobject that, jobject javaNumber) {
    JNF_COCOA_ENTER(env);
    NSNumber* nativeNumber = [JNFJavaToNSNumber(env, javaNumber) retain];
    CFNumberRef cfNumber = (__bridge CFNumberRef)nativeNumber;
    return (jlong) cfNumber;
    JNF_COCOA_EXIT(env);
    return 0;
}

JNIEXPORT jobject JNICALL Java_de_qfs_lib_axjlib_AXJLib_NumberCreateWithCFNumberRef
(JNIEnv * env, jobject that, jlong cfNumberRef) {
    JNF_COCOA_ENTER(env);
    // We don't free this reference by calling DeleteLocalRef because it's when it's passed back to Java.
    return (jobject) JNFNSToJavaNumber(env, (__bridge NSNumber*)cfNumberRef);
    JNF_COCOA_EXIT(env);
    return NULL;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFURLCreateWithJavaURL
(JNIEnv * env, jobject that, jobject javaURL) {
    JNF_COCOA_ENTER(env);
    
    if (! javaURL) return 0;
    
    NSString* nativeString = JNFObjectToString(env, javaURL); // autoreleased
    CFStringRef cfURLString = (__bridge CFStringRef)nativeString;
    CFURLRef cfURL = CFURLCreateWithString(NULL, cfURLString, NULL);
    
    return (jlong) cfURL;
    JNF_COCOA_EXIT(env);
    return 0;
}

JNIEXPORT jobject JNICALL Java_de_qfs_lib_axjlib_AXJLib_StringCreateWithCFURLRef
(JNIEnv * env, jobject that, jlong cfURLRef) {
    JNF_COCOA_ENTER(env);
    
    if (! cfURLRef) return NULL;
    
    CFStringRef cfURLString = CFURLGetString((CFURLRef)cfURLRef);
    if (! cfURLString) return NULL;
    
    // We don't free this reference by calling DeleteLocalRef because it's when it's passed back to Java.
    return (jstring) JNFNSToJavaString(env, (__bridge NSString*)cfURLString);
    JNF_COCOA_EXIT(env);
    return (jobject) NULL;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFArrayCreate
(JNIEnv * env, jobject that, jlongArray values) {
    JNF_COCOA_ENTER(env);
    if (! values) return (jlong) 0;
    
    jlong *p_values=NULL;
    jsize len = (*env)->GetArrayLength(env, values);
    if (! len) return (jlong) NULL;
    
    if (values) p_values = (*env)->GetLongArrayElements(env, values, NULL);
    
    CFArrayRef anArray = CFArrayCreate(NULL, (const void **)p_values, len, &kCFTypeArrayCallBacks);
    
    if (values && p_values) (*env)->ReleaseLongArrayElements(env, values, p_values, JNI_ABORT); // content was not changed -> no copy required
    
    return (jlong) anArray;
    JNF_COCOA_EXIT(env);
    return 0;
}

JNIEXPORT jlongArray JNICALL Java_de_qfs_lib_axjlib_AXJLib_ArrayCopyFromCFArrayRef
(JNIEnv * env, jobject that, jlong cfArrayRef) {
    JNF_COCOA_ENTER(env);
    
    jlongArray result;
    jsize size = cfArrayRef ? (jsize) CFArrayGetCount((CFArrayRef)cfArrayRef) : 0;
    
    result = (*env)->NewLongArray(env, size);
    if (result == NULL) return NULL; /* out of memory error thrown */
    
    int i;
    jlong fill[size];
    for (i = 0; i < size; i++) {
        fill[i] = (jlong) CFArrayGetValueAtIndex((CFArrayRef)cfArrayRef, i);
        if (fill[i] != 0) {
            CFRetain((CFTypeRef)fill[i]);
        }
    }
    
    // move from the temp structure to the java structure
    (*env)->SetLongArrayRegion(env, result, 0, size, fill);
    return result;
    JNF_COCOA_EXIT(env);
    return NULL;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFDictionaryCreate
(JNIEnv * env, jobject that, jlongArray keys, jlongArray values) {
    JNF_COCOA_ENTER(env);
    jlong *p_keys=NULL;
    jlong *p_values=NULL;
    jsize len = keys ? (*env)->GetArrayLength(env, keys) : 0;
    jsize val_len = values ? (*env)->GetArrayLength(env, values) : 0;
    if (val_len > len) {
        return throwIllegalArgumentException(env, "Size of keys (%d) and values (%d) differ.", len, val_len);
    }
    
    if (! val_len) return (jlong) NULL;
    
    if (keys) p_keys = (*env)->GetLongArrayElements(env, keys, NULL);
    if (values) p_values = (*env)->GetLongArrayElements(env, values, NULL);
    
    CFDictionaryRef aDict = CFDictionaryCreate(NULL, (const void **)p_keys, (const void **)p_values, val_len, &kCFCopyStringDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    //CFShow(aDict);
    
    if (keys && p_keys) (*env)->ReleaseLongArrayElements(env, keys, p_keys, JNI_ABORT); // content was not changed -> no copy required
    if (values && p_values) (*env)->ReleaseLongArrayElements(env, values, p_values, JNI_ABORT); // content was not changed -> no copy required
    
    return (jlong) aDict;
    JNF_COCOA_EXIT(env);
    return 0;
}

JNIEXPORT jobjectArray JNICALL Java_de_qfs_lib_axjlib_AXJLib_ArraysCopyFromCFDictionaryRef
(JNIEnv * env, jobject that, jlong cfDictRef) {
    JNF_COCOA_ENTER(env);
    
    jobjectArray result;
    jsize size = cfDictRef ? (jsize) CFDictionaryGetCount((CFDictionaryRef)cfDictRef) : 0;
    
    int i;
    jlong fill_keys[size];
    jlong fill_values[size];
    
    jlongArray keys = (*env)->NewLongArray(env, size);
    if (keys == NULL) return NULL; /* out of memory error thrown */
    jlongArray values = (*env)->NewLongArray(env, size);
    if (values == NULL) return NULL; /* out of memory error thrown */
    
    if (cfDictRef) CFDictionaryGetKeysAndValues((CFDictionaryRef)cfDictRef, (const void**) &fill_keys, (const void**) &fill_values);
    for (i = 0; i < size; i++) {
        if (fill_keys[i] != 0) {
            CFRetain((CFTypeRef)fill_keys[i]);
        }
        if (fill_values[i] != 0) {
            CFRetain((CFTypeRef)fill_values[i]);
        }
    }
    
    // move from the temp structure to the java structure
    (*env)->SetLongArrayRegion(env, keys, 0, size, fill_keys);
    (*env)->SetLongArrayRegion(env, values, 0, size, fill_values);
    
    jclass longArrayClass = (*env)->FindClass(env, "[J");
    if (longArrayClass == NULL) return NULL;
    result = (*env)->NewObjectArray(env, (jsize) 2, longArrayClass, NULL);
    
    (*env)->SetObjectArrayElement(env, result, 0, keys);
    (*env)->SetObjectArrayElement(env, result, 1, values);
    
    (*env)->DeleteLocalRef(env, keys);
    (*env)->DeleteLocalRef(env, values);
    
    return result;
    JNF_COCOA_EXIT(env);
    return NULL;
}


JNIEXPORT void JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFShow
(JNIEnv * env, jobject that, jlong obj) {
    JNF_COCOA_ENTER(env);
    CFShow((CFTypeRef)obj);
    JNF_COCOA_EXIT(env);
}

JNIEXPORT void JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFShowStr
(JNIEnv * env, jobject that, jlong str) {
    JNF_COCOA_ENTER(env);
    CFShowStr((CFStringRef)str);
    JNF_COCOA_EXIT(env);
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFCopyDescription
(JNIEnv * env, jobject that, jlong obj) {
    JNF_COCOA_ENTER(env);
    CFStringRef description = CFCopyDescription((CFTypeRef)obj);
    return (jlong) description;
    JNF_COCOA_EXIT(env);
    return (jlong) 0;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFGetTypeID
(JNIEnv * env, jobject that, jlong obj) {
    if (! obj) return 0;
    JNF_COCOA_ENTER(env);
    return (jlong) CFGetTypeID((CFTypeRef) obj);
    JNF_COCOA_EXIT(env);
    return (jlong) 0;
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFStringGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) CFStringGetTypeID();
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFBooleanGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) CFBooleanGetTypeID();
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFNumberGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) CFNumberGetTypeID();
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFURLGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) CFURLGetTypeID();
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFArrayGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) CFArrayGetTypeID();
}

JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_CFDictionaryGetTypeID
(JNIEnv * env, jobject that) {
    return (jlong) CFDictionaryGetTypeID();
}

jobject NSRunningApplicationToJava(JNIEnv * env, NSRunningApplication * app) {
    if (! app) return NULL;
    
    cacheNSRunningApplicationClassInfo(env);
    jobject newObject;
    
    jstring localizedName = (jstring) JNFNSToJavaString(env, (__bridge NSString*)[app localizedName]);
    jstring bundleIdentifier = (jstring) JNFNSToJavaString(env, (__bridge NSString*)[app bundleIdentifier]);
    
    NSString* bundleURL = [[app bundleURL] absoluteString];
    jstring bundleURLString = bundleURL ? (jstring) JNFNSToJavaString(env, (__bridge NSString*)bundleURL): 0;
    
    NSString* executableURL = [[app executableURL] absoluteString];
    jstring executableURLString = executableURL ? (jstring) JNFNSToJavaString(env, (__bridge NSString*)executableURL): 0;
    
    newObject = (*env)->NewObject(env, NSRunningApplicationCls.clazz, NSRunningApplicationCls.constructor,
                                  (jboolean) [app isActive],
                                  (jboolean) [app isHidden],
                                  localizedName,
                                  bundleIdentifier,
                                  bundleURLString,
                                  (jint) [app executableArchitecture],
                                  executableURLString,
                                  (jlong) [app.launchDate timeIntervalSince1970]*1000,
                                  (jboolean) [app isFinishedLaunching],
                                  (jint) [app processIdentifier],
                                  (jboolean) [app ownsMenuBar],
                                  (jboolean) [app isTerminated]);
    (*env)->DeleteLocalRef(env, executableURLString);
    (*env)->DeleteLocalRef(env, bundleURLString);
    (*env)->DeleteLocalRef(env, bundleIdentifier);
    (*env)->DeleteLocalRef(env, localizedName);
    
    return newObject;
}

jobjectArray NSRunningApplicationArrayToJava(JNIEnv * env, NSArray * apps) {
    cacheNSRunningApplicationClassInfo(env);
    
    jobjectArray result;
    jobject newObject;
    
    long size = apps ? [apps count] : 0;
    result = (*env)->NewObjectArray(env, (jsize) size, NSRunningApplicationCls.clazz, NULL);
    if (! result) return NULL;
    if (! apps) return result;
    
    int index = 0;
    
    for (NSRunningApplication * app in apps) {
        newObject = NSRunningApplicationToJava(env, app);
        if (! newObject) return NULL;
        
        (*env)->SetObjectArrayElement(env, result, index, newObject);
        (*env)->DeleteLocalRef(env, newObject);
        index++;
    }
    
    return result;
}

JNIEXPORT jobjectArray JNICALL Java_de_qfs_lib_axjlib_AXJLib_runningApplications
(JNIEnv * env, jobject that) {
    JNF_COCOA_ENTER(env);
    
    NSArray * apps = [[NSWorkspace sharedWorkspace] runningApplications];
    
    return NSRunningApplicationArrayToJava(env, apps);
    JNF_COCOA_EXIT(env);
    return NULL;
}


JNIEXPORT jobject JNICALL Java_de_qfs_lib_axjlib_AXJLib_runningApplicationWithProcessIdentifier
(JNIEnv * env, jobject that, jint pid) {
    JNF_COCOA_ENTER(env);
    jobject result;
    
    NSRunningApplication * app = [NSRunningApplication runningApplicationWithProcessIdentifier:(pid_t) pid];
    result = NSRunningApplicationToJava(env, app);
    return result;
    JNF_COCOA_EXIT(env);
    return NULL;
}


JNIEXPORT jobjectArray JNICALL Java_de_qfs_lib_axjlib_AXJLib_runningApplicationsWithBundleIdentifier
(JNIEnv * env, jobject that, jstring bundleIdentifier) {
    JNF_COCOA_ENTER(env);
    NSArray * apps = nil;
    
    if (bundleIdentifier) {
        NSString* nativeString = JNFJavaToNSString(env, bundleIdentifier);
        apps = [NSRunningApplication runningApplicationsWithBundleIdentifier:nativeString];
    }
    
    return NSRunningApplicationArrayToJava(env, apps);
    JNF_COCOA_EXIT(env);
    return NULL;
}


JNIEXPORT jobject JNICALL Java_de_qfs_lib_axjlib_AXJLib_currentApplication
(JNIEnv * env, jobject that) {
    JNF_COCOA_ENTER(env);
    jobject result;
    
    NSRunningApplication * app = [NSRunningApplication currentApplication];
    result = NSRunningApplicationToJava(env, app);
    return result;
    JNF_COCOA_EXIT(env);
    return NULL;
}


JNIEXPORT jlong JNICALL Java_de_qfs_lib_axjlib_AXJLib_NSSearchPathForDirectoriesInDomains
(JNIEnv * env, jobject that, jint directory, jint domainMask, jboolean expandTilde) {
    JNF_COCOA_ENTER(env);
    NSArray * paths = NSSearchPathForDirectoriesInDomains((NSSearchPathDirectory)directory, (NSSearchPathDomainMask)domainMask, (expandTilde ? YES : NO));
    CFRetain((__bridge CFArrayRef)paths);
    return (jlong)(__bridge CFArrayRef)paths;
    JNF_COCOA_EXIT(env);
    return 0;
}
/**
 memory management
 */

void JNI_OnUnload(JavaVM *vm, void *reserved) {
    JNIEnv* env;
    if ((*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        // Something is wrong
        return;
    } else {
        uncacheNSRunningApplicationClassInfo(env);
        uncacheURLClassInfo(env);
        uncacheCFRangeClassInfo(env);
        uncacheCGRectClassInfo(env);
        uncacheCGSizeClassInfo(env);
        uncacheCGPointClassInfo(env);
    }
}
