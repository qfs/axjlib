/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
//
//  javatypes.h
//  axjlib
//
//  Created by Pascal Bihler on 12.05.18.
//  Copyright © 2018 Pascal Bihler. All rights reserved.
//
// Get signature with javap -s <class>
//

#ifndef javatypes_h
#define javatypes_h

typedef struct CGPoint_CLS_CACHE {
    Boolean cached;
    jclass clazz;
    jfieldID x, y;
    jmethodID constructor;
} CGPoint_CLS_CACHE;

CGPoint_CLS_CACHE CGPointCls;

Boolean cacheCGPointClassInfo(JNIEnv *env)
{
    if (CGPointCls.cached) return YES;
    
    jclass clazz = (*env)->FindClass(env, "de/qfs/lib/axjlib/values/CGPoint");
    if (clazz == NULL) return NO;
    CGPointCls.clazz = (*env)->NewGlobalRef(env,clazz);
    if (CGPointCls.clazz == NULL) return NO;
    CGPointCls.constructor = (*env)->GetMethodID(env, CGPointCls.clazz, "<init>", "(FF)V"); // Get signature with javap -s <classfile>
    if (CGPointCls.constructor == NULL) return NO;
    CGPointCls.x = (*env)->GetFieldID(env, CGPointCls.clazz, "x", "F");
    if (CGPointCls.x == NULL) return NO;
    CGPointCls.y = (*env)->GetFieldID(env, CGPointCls.clazz, "y", "F");
    if (CGPointCls.y == NULL) return NO;
    CGPointCls.cached = YES;
    return YES;
}

void uncacheCGPointClassInfo(JNIEnv *env) {
    if (! CGPointCls.cached) return;
    if (CGPointCls.clazz != NULL) (*env)->DeleteGlobalRef(env, CGPointCls.clazz);
    CGPointCls.cached = NO;
}

typedef struct CGSize_CLS_CACHE {
    Boolean cached;
    jclass clazz;
    jfieldID width, height;
    jmethodID constructor;
} CGSize_CLS_CACHE;

CGSize_CLS_CACHE CGSizeCls;

Boolean cacheCGSizeClassInfo(JNIEnv *env)
{
    if (CGSizeCls.cached) return YES;
    
    jclass clazz = (*env)->FindClass(env, "de/qfs/lib/axjlib/values/CGSize");
    if (clazz == NULL) return NO;
    CGSizeCls.clazz = (*env)->NewGlobalRef(env,clazz);
    if (CGSizeCls.clazz == NULL) return NO;
    CGSizeCls.constructor = (*env)->GetMethodID(env, CGSizeCls.clazz, "<init>", "(FF)V"); // Get signature with javap -s <classfile>
    if (CGSizeCls.constructor == NULL) return NO;
    CGSizeCls.width = (*env)->GetFieldID(env, CGSizeCls.clazz, "width", "F");
    if (CGSizeCls.width == NULL) return NO;
    CGSizeCls.height = (*env)->GetFieldID(env, CGSizeCls.clazz, "height", "F");
    if (CGSizeCls.height == NULL) return NO;
    CGSizeCls.cached = YES;
    return YES;
}

void uncacheCGSizeClassInfo(JNIEnv *env) {
    if (! CGSizeCls.cached) return;
    if (CGSizeCls.clazz != NULL) (*env)->DeleteGlobalRef(env, CGSizeCls.clazz);
    CGSizeCls.cached = NO;
}

typedef struct CGRect_CLS_CACHE {
    Boolean cached;
    jclass clazz;
    jfieldID origin, size;
    jmethodID constructor;
} CGRect_CLS_CACHE;

CGRect_CLS_CACHE CGRectCls;

Boolean cacheCGRectClassInfo(JNIEnv *env)
{
    if (CGRectCls.cached) return YES;
    if (! cacheCGPointClassInfo(env)) return NO;
    if (! cacheCGSizeClassInfo(env)) return NO;

    jclass clazz = (*env)->FindClass(env, "de/qfs/lib/axjlib/values/CGRect");
    if (clazz == NULL) return NO;
    CGRectCls.clazz = (*env)->NewGlobalRef(env,clazz);
    if (CGRectCls.clazz == NULL) return NO;
    CGRectCls.constructor = (*env)->GetMethodID(env, CGRectCls.clazz, "<init>", "(FFFF)V"); // Get signature with javap -s <classfile>
    if (CGRectCls.constructor == NULL) return NO;
    CGRectCls.origin = (*env)->GetFieldID(env, CGRectCls.clazz, "origin", "Lde/qfs/lib/axjlib/values/CGPoint;");
    if (CGRectCls.origin == NULL) return NO;
    CGRectCls.size = (*env)->GetFieldID(env, CGRectCls.clazz, "size", "Lde/qfs/lib/axjlib/values/CGSize;");
    if (CGRectCls.size == NULL) return NO;
    CGRectCls.cached = YES;
    return YES;
}

void uncacheCGRectClassInfo(JNIEnv *env) {
    if (! CGRectCls.cached) return;
    if (CGRectCls.clazz != NULL) (*env)->DeleteGlobalRef(env, CGRectCls.clazz);
    CGRectCls.cached = NO;
}

typedef struct CFRange_CLS_CACHE {
    Boolean cached;
    jclass clazz;
    jfieldID location, length;
    jmethodID constructor;
} CFRange_CLS_CACHE;

CFRange_CLS_CACHE CFRangeCls;

Boolean cacheCFRangeClassInfo(JNIEnv *env)
{
    if (CFRangeCls.cached) return YES;
    
    jclass clazz = (*env)->FindClass(env, "de/qfs/lib/axjlib/values/CFRange");
    if (clazz == NULL) return NO;
    CFRangeCls.clazz = (*env)->NewGlobalRef(env,clazz);
    if (CFRangeCls.clazz == NULL) return NO;
    CFRangeCls.constructor = (*env)->GetMethodID(env, CFRangeCls.clazz, "<init>", "(JJ)V"); // Get signature with javap -s <classfile>
    if (CFRangeCls.constructor == NULL) return NO;
    CFRangeCls.location = (*env)->GetFieldID(env, CFRangeCls.clazz, "location", "J");
    if (CFRangeCls.location == NULL) return NO;
    CFRangeCls.length = (*env)->GetFieldID(env, CFRangeCls.clazz, "length", "J");
    if (CFRangeCls.length == NULL) return NO;
    CFRangeCls.cached = YES;
    return YES;
}

void uncacheCFRangeClassInfo(JNIEnv *env) {
    if (! CFRangeCls.cached) return;
    if (CFRangeCls.clazz != NULL) (*env)->DeleteGlobalRef(env, CFRangeCls.clazz);
    CFRangeCls.cached = NO;
}

typedef struct URL_CLS_CACHE {
    Boolean cached;
    jclass clazz;
    jmethodID constructor;
} URL_CLS_CACHE;

URL_CLS_CACHE URLCls;

Boolean cacheURLClassInfo(JNIEnv *env)
{
    if (URLCls.cached) return YES;
    
    jclass clazz = (*env)->FindClass(env, "java/net/URL");
    if (clazz == NULL) return NO;
    URLCls.clazz = (*env)->NewGlobalRef(env,clazz);
    if (URLCls.clazz == NULL) return NO;
    URLCls.constructor = (*env)->GetMethodID(env, URLCls.clazz, "<init>", "(Ljava/lang/String;)V"); // Get signature with javap -s <classfile>
    if (URLCls.constructor == NULL) return NO;
    URLCls.cached = YES;
    return YES;
}

void uncacheURLClassInfo(JNIEnv *env) {
    if (! URLCls.cached) return;
    if (URLCls.clazz != NULL) (*env)->DeleteGlobalRef(env, URLCls.clazz);
    URLCls.cached = NO;
}

typedef struct NS_RUNNING_APPLICATION_CLS_CACHE {
    Boolean cached;
    jclass clazz;
    jmethodID constructor;
} NS_RUNNING_APPLICATION;

NS_RUNNING_APPLICATION NSRunningApplicationCls;

Boolean cacheNSRunningApplicationClassInfo(JNIEnv *env)
{
    if (NSRunningApplicationCls.cached) return YES;
    
    jclass clazz = (*env)->FindClass(env, "de/qfs/lib/axjlib/appkit/NSRunningApplication");
    if (clazz == NULL) return NO;
    NSRunningApplicationCls.clazz = (*env)->NewGlobalRef(env,clazz);
    if (NSRunningApplicationCls.clazz == NULL) return NO;
    NSRunningApplicationCls.constructor = (*env)->GetMethodID(env, NSRunningApplicationCls.clazz, "<init>", "(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JZIZZ)V"); // Get signature with javap -s <classfile>
    if (NSRunningApplicationCls.constructor == NULL) return NO;
    NSRunningApplicationCls.cached = YES;
    return YES;
}

void uncacheNSRunningApplicationClassInfo(JNIEnv *env) {
    if (! NSRunningApplicationCls.cached) return;
    if (NSRunningApplicationCls.clazz != NULL) (*env)->DeleteGlobalRef(env, NSRunningApplicationCls.clazz);
    NSRunningApplicationCls.cached = NO;
}

#endif /* javatypes_h */
