/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.qfs.lib.axjlib.AXError;
import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTestWithNullReturnUponMissingAttribute;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTestWithoutMissingAttribute;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AXUIElementHelper;

abstract class SpecificGetParentTest {

    abstract MockedString getParentType();
    long ref = 12312345L;
    AXUIElement element;

    @Nested
    class getParent {

        @Test
        @DisplayName("returns the parent correctly")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long objRef = 12874L;
            MockedAXJLib.mockAXUIElement(axjlib, objRef, getParentType());
            MockedAXJLib.mockAttributeValue(axjlib, ref, MockedString.AXParent, objRef);

            AXUIElement result = element.getParent();

            assertEquals(objRef,AXUIElementHelper.getReference(result));
        }

        @ParameterizedAXErrorTestWithNullReturnUponMissingAttribute
        void nullCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyAttributeValue(eq(ref), any(long.class), any(long[].class))).thenReturn(error.getIntValue());

            Object value = element.getParent();

            assertNull(value);
        }

        @ParameterizedAXErrorTestWithoutMissingAttribute
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyAttributeValue(eq(ref), any(long.class), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getParent());
            assertEquals(error,ex.getError());
        }
    }
}