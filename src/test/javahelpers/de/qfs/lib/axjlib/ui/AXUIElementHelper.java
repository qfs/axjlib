/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import java.io.IOException;

import java.lang.reflect.Field;

import java.net.URI;
import java.net.URISyntaxException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.stream.Stream;

import lombok.SneakyThrows;

public class AXUIElementHelper
{
    public static long getReference(final AXUIElement element) {
        return element.reference;
    }

    public static Stream<Class<?>> getElementClasses() throws URISyntaxException, IOException
    {
        return getElementClassNames().map(AXUIElementHelper::loadClass);
    }

    public static Stream<String> getElementClassNames() throws URISyntaxException, IOException
    {
        final String elementsPackage = AXUIElementHelper.class.getPackage().getName() + ".elements";
        final URI codeSource = AXUIElementHelper.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        final Path elementsDir = Paths.get(codeSource).getParent().resolve("main/" + elementsPackage.replace('.', '/'));
        final Stream<String> map = Files.list(elementsDir).map(
                (path) -> {
                    final String fileName = path.getFileName().toString();
                    final String className = fileName.substring(0, fileName.indexOf('.'));
                    return elementsPackage + "." + className;
                });
        return map;
    }

    @SneakyThrows
    private static Class<?> loadClass(final String classname) {
        return AXUIElementHelper.class.getClassLoader().loadClass(classname);
    }

    public static String getRole(final Class<?> elementClass) {
        final String simpleName = elementClass.getSimpleName();
        final String role = getStringFieldValue(elementClass,"ROLE");
        return role == null ? simpleName : role;
    }

    public static String getSubrole(final Class<?> elementClass) {
        final String role = getStringFieldValue(elementClass,"SUB_ROLE");
        return role;
    }

    private static String getStringFieldValue(final Class<?> clazz, final String fieldName)
    {
        try {
            final Field field = clazz.getField(fieldName);
            return (String) field.get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            /* silentIgnore */
        }

        return null;
    }

}
