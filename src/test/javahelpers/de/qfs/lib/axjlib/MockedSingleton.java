/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.mockito.Mockito;

public class MockedSingleton<T> implements BeforeEachCallback, AfterEachCallback, ParameterResolver
{
    final private Class<T> classT;
    private Field instanceField;

    T mockedSingleton;
    T realSingleton;

    @SuppressWarnings("unchecked")
    public MockedSingleton() {
        classT = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        try {
            instanceField = classT.getDeclaredField("instance");
            instanceField.setAccessible(true);

        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void beforeEach(ExtensionContext context) throws Exception {
        realSingleton = (T) instanceField.get(null);
        mockedSingleton = Mockito.mock(classT);
        instanceField.set(null, mockedSingleton);
    }

    public void afterEach(ExtensionContext context) throws Exception {

        instanceField.set(null, realSingleton);
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        return parameterContext.getParameter().getType() == classT;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        return mockedSingleton;
    }
}
