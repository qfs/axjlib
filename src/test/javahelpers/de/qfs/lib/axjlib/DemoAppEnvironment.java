/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import java.io.IOException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

import de.qfs.lib.axjlib.ui.elements.AXApplication;
import de.qfs.lib.axjlib.ui.elements.AXWindow;
import de.qfs.lib.axjlib.values.CGPoint;


public class DemoAppEnvironment implements ParameterResolver, BeforeAllCallback, AfterAllCallback
{
    static class DemoApp {
        Process process;
        int pid;
        Number x;
        Number y;
    }

    static DemoApp demoApp;
    static ExtensionContext outmostContext;

    static void startDemoApp() throws IOException, NoSuchFieldException, IllegalAccessException, ScriptException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, InterruptedException, InstantiationException
    {
        if (demoApp != null) return;

        demoApp = new DemoApp();
        ProcessBuilder pb = new ProcessBuilder("resources/TicTacToe.app/Contents/MacOS/TicTacToe");
        demoApp.process = pb.start();

        Thread.sleep(500);

        Field f = demoApp.process.getClass().getDeclaredField("pid");
        f.setAccessible(true);
        demoApp.pid = f.getInt(demoApp.process);

        // raise the window using applescript

        try {
            ScriptEngineFactory engineFactory = (ScriptEngineFactory) Class.forName("apple.applescript.AppleScriptEngineFactory").newInstance();
            ScriptEngine engine = engineFactory.getScriptEngine();

            String script = "tell application \"System Events\"\n"
                    + "tell (first process whose unix id is " + demoApp.pid + ")\n"
                    + "set frontmost to true\n"
                    + "tell (first window)\n"
                    + "perform action \"AXRaise\"\n"
                    + "return the position\n"
                    + "end tell\n"
                    + "end tell\n"
                    + "end tell\n";


            @SuppressWarnings("unchecked")
            List<Number> pos = (List<Number>)(engine.eval(script));
            demoApp.x = pos.get(0);
            demoApp.y = pos.get(1);
        } catch (final Exception ex) {
            // something went wrong
            System.err.println("Could not interact indirectly with demo app" + ex + " -> Using workaround.");
            // Use as workaround own api (which should be tested, but if everything is ok, that must also work fine):
            final AXApplication app = AXAPI.getApplicationAccessibilityObject(demoApp.pid);
            app.setFrontmost(true);
            final AXWindow window = (AXWindow) app.getWindows().get(0);
            window.raise();
            final CGPoint position = window.getPosition();
            demoApp.x = position.getX();
            demoApp.y = position.getY();
        }
    }

    static void stopDemo() {
        if (demoApp != null) {
            demoApp.process.destroy();
            demoApp = null;
        }
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        final boolean usesDemoApp = parameterContext.getParameter().getType() == DemoApp.class;
        if (usesDemoApp) {
            try {
                startDemoApp();
            } catch (final Exception ex) {
                throw new ParameterResolutionException("Could not start demo app", ex);
            }
        }
        return usesDemoApp;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        return demoApp;
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception
    {
        // remember first nested context to shut down app as late as possible
        if (outmostContext == null) outmostContext = context;
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception
    {
        final boolean onTopLevel = context == outmostContext;
        if (onTopLevel) {
            stopDemo();
        }
    }

}
