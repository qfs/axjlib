/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import java.awt.MouseInfo;
import java.awt.Point;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParser;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParserSettings;

import de.qfs.lib.axjlib.annotations.GeneralReturnType;
import de.qfs.lib.axjlib.appkit.NSRunningApplication;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXSections;
import de.qfs.lib.axjlib.ui.elements.AXApplication;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

/**
 * Helper program to generate / extend the /src/test/resources/de/qfs/lib/axjlib/ui/AXUIElements.csv
 * file. The Scraper walks over all windows of all processes currently running on the system and extends
 * the information found in th AXUIElements.csv file by the components, attributes and actions it finds.
 *
 * In addition, missing Actions/Attributes interfaces and Element classes are autogenerated.
 *
 * @author Pascal Bihler
 *
 */
@RequiredArgsConstructor
public class Scraper
{
    static final File RES_BASE = new File("src/test/resources/de/qfs/lib/axjlib/ui");
    static final File SRC_BASE = new File("src/main/java/de/qfs/lib/axjlib/ui");
    static final File CSV_FILE = new File(RES_BASE,"AXUIElements.csv");

    static final boolean CROSS_CHECK = true;
    static final boolean CROSS_CHECK_SHOW_PRECISION = false; // Show recommendations for concrete subtypes?

    final int pid;

    final Map<String, ElementData> data = new TreeMap<>();
    final SortedSet<String> allAttributes = new TreeSet<>();
    final SortedSet<String> allActions = new TreeSet<>();
    final Map<Integer, NSRunningApplication> processInfos = new TreeMap<>();
    private NSRunningApplication[] processes;

    private long elementCounter = 0;
    private long appCounter = 0;

    @Data
    static class ElementData {
        final String role;
        final String subrole;
        final SortedSet<String> attributes = new TreeSet<>();
        final SortedSet<String> actions = new TreeSet<>();

        String getKey() {
            return getKey(role,subrole);
        }

        String getName() {
            return subrole == null ? role : subrole;
        }

        static String getKey(final String role, final String subrole) {
            return role + "," + (subrole == null ? "" : subrole);
        }

        @Override
        public String toString() {
            return getKey() + ","
                    + "\"" + commaSeparated(attributes) + "\","
                    + "\"" + commaSeparated(actions) + "\"";
        }

        public boolean isAXElement()
        {
            return role.startsWith("AX") && (subrole==null || subrole.startsWith("AX"));
        }

        public boolean isAXUnknown()
        {
            return "AXUnknown".equals(role) || "AXUnknown".equals(subrole);
        }
    }

    void scrape()
    {
        readCSV();

        processes = AXJLib.INSTANCE.runningApplications();
        for (final NSRunningApplication process: processes) {
            final int processIdentifier = process.getProcessIdentifier();
            if (processIdentifier <= 0) continue;
            this.processInfos.put(processIdentifier, process);
        }

        if (pid == 0) {
            for (final Integer processIdentifier: this.processInfos.keySet()) {
                scrape(processIdentifier);
            }
        } else if (pid > 0) {
            scrape(this.pid);
        }

        writeCSV();
        generateClasses();
        printConstants();
        generateFactorySwitch();

    }

    void scrape(final int pid)
    {
        final NSRunningApplication processInfo = processInfos.get(pid);
        if (processInfo != null) {
            System.out.printf("Scraping process %d (%s)...", pid, processInfo.getName());
        } else {
            System.out.printf("Scraping process %d...", pid);
        }
        try {
            final AXApplication application = AXAPI.getApplicationAccessibilityObject(pid);
            appCounter++;
            scrape(application, "");
            System.out.println("done");
        } catch (final AXException ex) {
            System.out.println(ex.getMessage());
        }
    }

    void scrape(final AXUIElement element, String context)
    {
        if (element == null) return;

        context += getContextExtension(element);

        addElement(element, context);

        try {
            final List<? extends AXUIElement> children = element.getChildren();
            if (children != null) {
                for (final AXUIElement child: children) {
                    scrape(child, context);
                }
            }
        } catch (final AXException ex) {
            System.err.println(context + ": " + ex.getMessage());
        }
    }

    private String getContextExtension(final AXUIElement element)
    {
        String role = element.getClass().getSimpleName();
        if (element.getClass() == AXUIElement.class) {
            try {
                role = "'" + element.getRole() + "'";
            } catch (Exception ex) {
                // ignore
            }
        }
        String title = null;
        try {
            title = element.getTitle();
            if (title != null) {
                title = title.replaceAll("\\v", " ");
                if (title.length() > 10) {
                    title = title.substring(0, 7) + "...";
                }
            }
        } catch (Exception ex) {
            // ignore
        }
        return ">" + role + (title == null ? "" : "(" + title + ")");
    }

    void addElement(final AXUIElement element, final String context)
    {
        elementCounter++;

        final String role = element.getRole();
        final String subrole = element.getSubrole();

        final ElementData thisElementData = new ElementData(role, subrole);

        ElementData elementData = new ElementData(role, subrole);
        if (! elementData.isAXElement()) return;
        if (elementData.isAXUnknown()) return; // we don't like :)

        final String key = elementData.getKey();
        elementData = data.getOrDefault(key, elementData);
        data.put(key, elementData);

        final List<String> attributes = element.getAttributeNames();
        final List<String> flaggedAttributes = new LinkedList<>();
        for (final String attribute: attributes) {
            boolean settable = false;
            try {
                if (Boolean.TRUE.equals(element.isAttributeSettable(attribute))) {
                    settable = true;
                }
            } catch (final AXException ex) {/* ignore */}

            flaggedAttributes.add(attribute + (settable ? "!" : ""));
        }

        addAttributes(elementData, flaggedAttributes);
        addActions(elementData, element.getActionNames());

        if (CROSS_CHECK) {
            doCrossCheckElement(element, context, thisElementData, attributes);
        }
    }

    private void addAttributes(final ElementData elementData, final List<String> attributes)
    {
        addAll(elementData.attributes, attributes, allAttributes);
    }

    private void addActions(final ElementData elementData, final List<String> actions)
    {
        addAll(elementData.actions, actions, allActions);
    }

    private void addAll(final SortedSet<String> target, final List<String> source, final SortedSet<String> allSet)
    {
        if (source != null) {
            target.addAll(source);
            allSet.addAll(source);
        }
    }

    private void addAll(final SortedSet<String> set, final String input, final SortedSet<String> allSet)
    {
        if (input == null) return;
        final String[] array = input.split(",",-1);
        final List<String> list = Arrays.asList(array);
        addAll(set, list, allSet);
    }


    /**
     * An approach to check if the current API has errors or could be more precise with respect to the currently
     * inspected application(s)
     */
    private void doCrossCheckElement(final AXUIElement element, final String context,
                                     final ElementData thisElementData, final List<String> attributes)
    {
        final List<String> errors = new LinkedList<>();
        final String className = element.getClass().getSimpleName();
        if (! className.equals(thisElementData.getName())) {
            errors.add(String.format("Classname is %s, should be %s -> ignoring attribute tests", className, thisElementData.getName()));
        }

        for (String attributeName: attributes) {
            final Method getter = getAttributeGetter(element, attributeName);
            if (getter == null) {
                errors.add(String.format("Misses get%s() method", attributeName.substring(2)));
            } else {
                Object value = null;
                try {
                    value = element.getAttributeValueOrNull(attributeName);
                } catch (final AXException ex) {
                    if (ex.getError() != AXError.kAXErrorFailure) {
                        errors.add(String.format("Error getting attribute %s: %s", attributeName, ex.getMessage()));
                    }
                }
                if (value != null) {
                    try {
                        final Object getterValue = getter.invoke(element);
                        assert getterValue != null : "Since value != null and it should be equal to value";

                        final boolean hasGeneralReturnType = hasGeneralReturnType(getter);
                        final Class<?> returnClass = getter.getReturnType();
                        final String getterName = getter.getName();

                        checkMethodReturnType(getterName, value, returnClass, hasGeneralReturnType, errors);

                        if (value instanceof List) {
                            final ParameterizedType getterReturnListType = (ParameterizedType) getter.getGenericReturnType();
                            final Type genericType = getterReturnListType.getActualTypeArguments()[0];
                            final Class<?> genericClass = (Class<?>) (genericType instanceof WildcardType ? ((WildcardType) genericType).getUpperBounds()[0] : genericType);
                            final Set<String> listErrors = new HashSet<>();
                            for (final Object valueMember: (List<?>) value) {
                                checkMethodReturnType(getterName+"<ListMember>", valueMember, genericClass, hasGeneralReturnType, listErrors);
                            }
                            errors.addAll(listErrors);

                        }
                    } catch (InvocationTargetException e) {
                        errors.add(String.format("InvocationTargetException on calling getter %s: %s", getter.getName(), e.getCause()));
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        errors.add(String.format("Exception on calling getter %s: %s", getter.getName(), e));
                    }
                }
            }
        }

        if (! errors.isEmpty()) {
            System.out.println(context);
            System.out.println(errors.stream().map((line) -> "  " + line).collect(Collectors.joining("\n")));
        }
    }

    private boolean hasGeneralReturnType(final Method getter)
    {
        if (getter.getAnnotation(GeneralReturnType.class) != null) return true;
        final Class<?> superclass = getter.getDeclaringClass().getSuperclass();
        if (superclass == null) return false;
        try {
            Method superGetter = superclass.getMethod(getter.getName());
            return hasGeneralReturnType(superGetter);
        } catch (NoSuchMethodException | SecurityException e) {
            return false;
        }
    }

    private Method getAttributeGetter(final AXUIElement element, final String attributeName)
    {
        final Method getter = getAttributeGetter(element.getClass().getDeclaredMethods(),attributeName);
        if (getter != null) return getter;
        return getAttributeGetter(element.getClass().getMethods(),attributeName);
    }

    private Method getAttributeGetter(final Method[] methods, final String attributeName)
    {
        for (final Method method: methods) {
            if (! Modifier.isPublic(method.getModifiers())) continue;

            final String methodName = method.getName();
            final String simpleAttributeName = attributeName.substring(2);
            if (methodName.equals("get" + simpleAttributeName)
                    || methodName.equals("is" + simpleAttributeName)
                    || methodName.equals("has" + simpleAttributeName)
                    || methodName.equals("does" + simpleAttributeName)) {
                return method;
            }
        }
        return null;
    }

    private void checkMethodReturnType(final String getterName, final Object receivedObject,
                                       final Class<?> expectedType,
                                       final boolean hasGeneralReturnType, final Collection<String> errors)
    {
        if (receivedObject == null) {
            errors.add(String.format("Result of %s is null, when it should not be so.", getterName));
            return;
        }
        if (expectedType == EnumSet.class) return;
        if (expectedType == AXSections.AXSection.class) return;

        final Class<?> receivedType = receivedObject.getClass();
        if (! expectedType.isInstance(receivedObject)) {
            errors.add(String.format("Value of %s is not an instance of %s, but of %s.", getterName, expectedType.getSimpleName(), receivedType.getSimpleName()));
            return;
        }
        if (hasGeneralReturnType) return;
        if (! CROSS_CHECK_SHOW_PRECISION) return;

        if ((receivedType != expectedType) && expectedType.isAssignableFrom(receivedType)
                && expectedType != List.class // filter...
                && receivedType.getSuperclass() == AXUIElement.class // only check Roles, not subrole classes
                ) {
            errors.add(String.format("Return type of %s might be more precise: %s (is %s)", getterName, receivedType.getSimpleName(), expectedType.getSimpleName()));
        }
    }

    void readCSV()
    {
        if (! CSV_FILE.exists()) return;

        final CsvParser csvParser = new CsvParser(new CsvParserSettings());
        csvParser.beginParsing(CSV_FILE, "UTF-8");
        final List<String[]> content = csvParser.parseAll();
        for (final String[] line: content) {
            final ElementData elementData = new ElementData(line[0], line[1]);
            if (line.length > 2) addAll(elementData.attributes, line[2], allAttributes);
            if (line.length > 3) addAll(elementData.actions, line[3], allActions);
            data.put(elementData.getKey(), elementData);
        }
    }

    @SneakyThrows
    void writeCSV()
    {
        try(final PrintStream stream = new PrintStream(new FileOutputStream(CSV_FILE))) {
            stream.println("# Role, SubRole, Attributes, Actions");
            for (final ElementData elementData: data.values()) {
                stream.println(elementData);
            }
        }
    }

    void generateClasses() {
        System.out.println("Generating Classes:");
        generateAttributeClasses();
        generateActionClasses();
        generateElementClasses();
    }

    @SneakyThrows
    void generateAttributeClasses()
    {
        final String readOnlyTemplate = getTemplate("attributes/ReadOnlyAttribute.template");
        final String readWriteTemplate = getTemplate("attributes/ReadWriteAttribute.template");

        final File classesFolder = new File(SRC_BASE, "attributes");

        final List<String> existing = Arrays.asList(classesFolder.listFiles()).stream()
                .map((file) -> file.getName())
                .map((name) -> name.substring(0, name.indexOf("."))).collect(Collectors.toList());

        for (String attribute: allAttributes) {
            if (allAttributes.contains(attribute + "!")) continue;

            final boolean isSettable = attribute.endsWith("!");
            if (isSettable) attribute = attribute.substring(0,attribute.length()-1);

            if (existing.contains(attribute)) continue;

            final File classFile = new File(classesFolder,attribute + ".java");

            System.out.println("Generating " + classFile);

            final String simpleAttribute = getSimpleAttributeName(attribute);
            final String capitalizedSimpleAttribute = attribute.substring(2);

            final String code = (isSettable ? readWriteTemplate : readOnlyTemplate).replaceAll(Pattern.quote("${attribute}"), attribute)
                    .replaceAll(Pattern.quote("${simpleAttribute}"), simpleAttribute)
                    .replaceAll(Pattern.quote("${capitalizedSimpleAttribute}"), capitalizedSimpleAttribute);

            try(final PrintStream stream = new PrintStream(new FileOutputStream(classFile))) {
                stream.println(code);
            }
        }
    }

    private String getSimpleAttributeName(String attribute)
    {
        return attribute.substring(2, 3).toLowerCase() + attribute.substring(3);
    }

    @SneakyThrows
    void generateActionClasses()
    {
        final String template = getTemplate("actions/Action.template");

        final File classesFolder = new File(SRC_BASE, "actions");

        final List<File> existing = Arrays.asList(classesFolder.listFiles());

        for (final String action: allActions) {
            final File classFile = new File(classesFolder,action + ".java");
            if (existing.contains(classFile)) continue;

            System.out.println("Generating " + classFile);

            final String simpleAction = getSimpleAttributeName(action);

            final String code = template.replaceAll(Pattern.quote("${action}"), action)
                    .replaceAll(Pattern.quote("${simpleAction}"), simpleAction);

            try(final PrintStream stream = new PrintStream(new FileOutputStream(classFile))) {
                stream.println(code);
            }
        }
    }

    @SneakyThrows
    void generateElementClasses()
    {
        final String primaryTemplate = getTemplate("elements/PrimaryElement.template");
        final String secondaryTemplate = getTemplate("elements/SecondaryElement.template");

        final String classPrefix = this.getClass().getPackage().getName() + ".ui";

        final File classesFolder = new File(SRC_BASE, "elements");

        final List<String> existingElements = Arrays.asList(classesFolder.listFiles()).stream()
                .map((file) -> file.getName())
                .map((name) -> name.substring(0, name.indexOf("."))).collect(Collectors.toList());

        for (final ElementData elementData: data.values()) {
            final boolean isPrimary = (elementData.subrole == null);
            final boolean isSecondary = ! isPrimary;

            final String role = elementData.role;
            final String subrole = elementData.subrole;
            final String elementName = isPrimary ? role : subrole;

            if (existingElements.contains(elementName)) continue;

            final File classFile = new File(classesFolder,elementName + ".java");
            System.out.println("Generating " + classFile);

            final SortedSet<String> attributes = new TreeSet<>();
            final SortedSet<String> attributeImports = new TreeSet<>();
            SortedSet<String> primaryAttributesSet = Collections.emptySortedSet();

            final SortedSet<String> actions = new TreeSet<>();
            final SortedSet<String> actionImports = new TreeSet<>();
            SortedSet<String> primaryActionsSet = Collections.emptySortedSet();

            if (isSecondary) {
                ElementData primaryData = data.get(ElementData.getKey(role, null));
                if (primaryData != null) {
                    primaryAttributesSet = primaryData.attributes;
                    primaryActionsSet = primaryData.actions;
                }
            }

            manageInterfaces(elementData.attributes, primaryAttributesSet, existingElements, classPrefix + ".attributes.", attributes, attributeImports);
            manageInterfaces(elementData.actions, primaryActionsSet, existingElements, classPrefix + ".actions.", actions, actionImports);

            final String breakIfActions = actions.size() == 0 ? "" : ",\n               ";

            String code = (isPrimary ? primaryTemplate : secondaryTemplate)
                    .replaceAll(Pattern.quote("${role}"), role)
                    .replaceAll(Pattern.quote("${subrole}"), subrole)
                    .replaceAll(Pattern.quote("${breakIfActions}"), breakIfActions)
                    .replaceAll(Pattern.quote("${attributeImports}"), formattedImports(attributeImports))
                    .replaceAll(Pattern.quote("${attributes}"), commaSpaceSeparated(attributes))
                    .replaceAll(Pattern.quote("${actionImports}"), formattedImports(actionImports))
                    .replaceAll(Pattern.quote("${actions}"), commaSpaceSeparated(actions));

            try(final PrintStream stream = new PrintStream(new FileOutputStream(classFile))) {
                stream.println(code);
            }
        }
    }

    private void manageInterfaces(@NonNull final SortedSet<String> set, final SortedSet<String> primarySet,
                                  @NonNull final List<String> existingElements, final String classPrefix,
                                  @NonNull final SortedSet<String> interfaces,
                                  @NonNull final SortedSet<String> imports)
    {
        for (String member: set) {
            if (set.contains(member + "!")) continue;
            if (member.endsWith("!")) member = member.substring(0,member.length()-1);

            final String className = classPrefix + member;
            if (isInterfaceOnAXUIElement(className)) continue;

            if (primarySet.contains(member)) continue;

            if (existingElements.contains(member)) { // Name clash, happens unfortunately
                interfaces.add(className);
            } else {
                imports.add(className);
                interfaces.add(member);
            }
        }
    }

    private boolean isInterfaceOnAXUIElement(final String className)
    {
        for (final Class<?> interfc: AXUIElement.class.getInterfaces()) {
            if (className.equals(interfc.getName())) return true;
        }
        return false;
    }

    private static String formattedImports(final Collection<String> imports)
    {
        return imports.stream().map((name) -> String.format("import %s;", name))
                .collect(Collectors.joining("\n"));
    }

    private static String commaSeparated(final Collection<String> elements)
    {
        return elements.stream().collect(Collectors.joining(","));
    }

    private static String commaSpaceSeparated(final Collection<String> elements)
    {
        return elements.stream().collect(Collectors.joining(", "));
    }

    @SneakyThrows
    private String getTemplate(final String file)
    {
        final File templateFile = new File(RES_BASE,file);
        try(final BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(templateFile)))) {
            return in.lines().collect(Collectors.joining("\n"));
        }
    }

    private void printConstants()
    {
        System.out.println("------- All used contants -----");
        final SortedSet<String> constants = new TreeSet<>();
        for (final ElementData elementData: data.values()) {
            constants.add(elementData.role);
            if (elementData.subrole != null) constants.add(elementData.subrole);
            constants.addAll(elementData.attributes.stream()
                    .map((name) -> name.endsWith("!") ? name.substring(0, name.length()-1) : name)
                    .collect(Collectors.toSet()));
            constants.addAll(elementData.actions);
        }
        System.out.println(constants.stream().collect(Collectors.joining(",\n")));
        System.out.println("--------------------------------");
    }

    private void generateFactorySwitch()
    {
        final String defaultName = "XXX";

        System.out.println("------- FactorySwitchStatement -----");
        final Map<String,Map<String,String>> cases = new TreeMap<>();
        for (final ElementData elementData: data.values()) {
            cases.putIfAbsent(elementData.role, new TreeMap<>());

            final String keyName = elementData.subrole == null ? defaultName : elementData.subrole;
            final String elementName = elementData.getName();

            cases.get(elementData.role).put(keyName, elementName);
        }
        System.out.println("switch (role) {");

        System.out.println(cases.entrySet().stream()
                .map((entry) ->
                    "case " + entry.getKey() + ".ROLE: " +
                        ( entry.getValue().size() == 1 ? "return new " + entry.getValue().get(defaultName)  + "(reference);"
                                : String.format("switch (subrole) {\n%s}",
                                        entry.getValue().entrySet().stream()
                                        .map((subEntry) ->
                                          (defaultName.equals(subEntry.getKey()) ? "default: ": "case " + subEntry.getKey() + ".SUB_ROLE: ")
                                          + "return new " + subEntry.getValue() + "(reference);")
                                        .collect(Collectors.joining("\n"))))

                )
                .collect(Collectors.joining("\n")));

        System.out.println("}");
        System.out.println("--------------------------------");
    }

    /**
     * Start without argument to scrape the whole system, or specify pid or "mouse"
     */
    public static void main(final String[] args)
    {
        Scraper scraper;

        if (! AXAPI.isProcessTrusted(true)) {
            System.err.println("You have to trust the scraper process in order to inspect your system");
        }

        if (args.length > 0) {
            final int pid;
            if ("mouse".equals(args[0]) ) {
                final Point p = MouseInfo.getPointerInfo().getLocation();
                pid = AXAPI.elementAtPosition(p.x, p.y).getPid();
                System.out.printf("Scraping process at (%d,%d)\n", p.x, p.y);
            } else {
                pid = Integer.parseInt(args[0]);
            }
            scraper = new Scraper(pid);
        } else {
            System.out.println("Scraping all processes");
            scraper = new Scraper(0); // all processes
        }

        scraper.scrape();

        System.out.printf("Done. Scraped %d elements in %d applications.\n",
                scraper.elementCounter, scraper.appCounter);
    }

}
