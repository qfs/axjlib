/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.net.URL;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import de.qfs.lib.axjlib.utils.AXUtilsHelper;
import de.qfs.lib.axjlib.utils.TypeConverter;
import de.qfs.lib.axjlib.utils.TypeConverterHelper;

import lombok.SneakyThrows;


public class MockedAXJLib implements BeforeEachCallback, AfterEachCallback, ParameterResolver {
    public static enum MockedString {
        EMPTY,
        AXAccessKey,
        AXAlternateUIVisible,
        AXApplication,
        AXAuditIssues,
        AXBlockQuoteLevel,
        AXBrowser,
        AXBusyIndicator,
        AXButton,
        AXCancel,
        AXCancelButton,
        AXCaretBrowsingEnabled,
        AXCell,
        AXCheckBox,
        AXChildren,
        AXChildrenInNavigationOrder,
        AXClearButton,
        AXCloseButton,
        AXColumn,
        AXColumnIndexRange,
        AXColumns,
        AXComboBox,
        AXConfirm,
        AXContents,
        AXDOMClassList,
        AXDOMIdentifier,
        AXDecrementArrow,
        AXDecrementPage,
        AXDefaultButton,
        AXDescription,
        AXDialog,
        AXDisclosedByRow,
        AXDisclosedRows,
        AXDisclosing,
        AXDisclosureLevel,
        AXDisclosureTriangle,
        AXDocument,
        AXEditableAncestor,
        AXEdited,
        AXElementBusy,
        AXEnabled,
        AXEndTextMarker,
        AXEnhancedUserInterface,
        AXExtrasMenuBar,
        AXFilename,
        AXFocusableAncestor,
        AXFocused,
        AXFocusedApplication,
        AXFocusedUIElement,
        AXFocusedWindow,
        AXFrame,
        AXFrontmost,
        AXFullScreen,
        AXFullScreenButton,
        AXFunctionRowTopLevelElements,
        AXGroup,
        AXGrowArea,
        AXHeader,
        AXHeading,
        AXHelp,
        AXHidden,
        AXHighestEditableAncestor,
        AXHorizontalScrollBar,
        AXIdentifier,
        AXImage,
        AXIncrementArrow,
        AXIncrementPage,
        AXIndex,
        AXInsertionPointLineNumber,
        AXLanguage,
        AXLayoutCount,
        AXLink,
        AXLinkUIElements,
        AXLinkedUIElements,
        AXList,
        AXLoaded,
        AXLoadingProgress,
        AXMain,
        AXMainWindow,
        AXMaxValue,
        AXMenu,
        AXMenuBar,
        AXMenuBarItem,
        AXMenuButton,
        AXMenuExtra,
        AXMenuItem,
        AXMenuItemCmdChar,
        AXMenuItemCmdGlyph,
        AXMenuItemCmdModifiers,
        AXMenuItemCmdVirtualKey,
        AXMenuItemMarkChar,
        AXMenuItemPrimaryUIElement,
        AXMinValue,
        AXMinimizeButton,
        AXMinimized,
        AXModal,
        AXNextContents,
        AXNumberOfCharacters,
        AXNumberOfMatches,
        AXOpen,
        AXOrientation,
        AXOutline,
        AXOutlineRow,
        AXOverflowButton,
        AXParent,
        AXPick,
        AXPlaceholderValue,
        AXPopUpButton,
        AXPosition,
        AXPress,
        AXPreventKeyboardDOMEventDispatch,
        AXPreviousContents,
        AXProxy,
        AXRadioButton,
        AXRadioGroup,
        AXRaise,
        AXRole,
        AXRoleDescription,
        AXRow,
        AXRowIndexRange,
        AXRows,
        AXSafariAddressAndSearchField,
        AXScrollArea,
        AXScrollBar,
        AXScrollDownByPage,
        AXScrollLeftByPage,
        AXScrollRightByPage,
        AXScrollToVisible,
        AXScrollUpByPage,
        AXSearchButton,
        AXSearchField,
        AXSections,
        AXSelected,
        AXSelectedCells,
        AXSelectedChildren,
        AXSelectedColumns,
        AXSelectedRows,
        AXSelectedText,
        AXSelectedTextMarkerRange,
        AXSelectedTextRange,
        AXServesAsTitleForUIElements,
        AXSheet,
        AXShowAlternateUI,
        AXShowDefaultUI,
        AXShowMenu,
        AXSize,
        AXSplitGroup,
        AXSplitter,
        AXSplitters,
        AXStandardWindow,
        AXStartTextMarker,
        AXStaticText,
        AXSubrole,
        AXSystemDialog,
        AXSystemWide,
        AXTabGroup,
        AXTable,
        AXTableRow,
        AXTabs,
        AXTextArea,
        AXTextField,
        AXTitle,
        AXTitleUIElement,
        AXToolbar,
        AXToolbarButton,
        AXTopLevelUIElement,
        AXURL,
        AXValue,
        AXValueDescription,
        AXValueIndicator,
        AXVerticalScrollBar,
        AXVisibleCharacterRange,
        AXVisibleChildren,
        AXVisibleColumns,
        AXVisibleRows,
        AXVisited,
        AXWebArea,
        AXWindow,
        AXWindows,
        AXZoomButton,
        AXZoomWindow,

        Unknown;

        public long getCfRef() {
            return 0x1000 + this.ordinal();
        }

        public static MockedString valueOfOrNull(final String name) {
            try {
                return valueOf(name);
            } catch (NullPointerException ex) {}
            return null;
        }
    }

    public static enum MockedBoolean {
        False,
        True;

        public long getCfRef() {
            return 0x2000 + this.ordinal();
        }
    }

    public static enum MockedNumber {
        Number1(1),
        Number23(23),
        Number815(815);

        Number number;

        MockedNumber(Number number) {
            this.number = number;
        }

        public long getCfRef() {
            return 0x3000 + this.ordinal();
        }
    }

    public static enum MockedTypeId {
        AXUIElementTypeID,
        AXValueTypeID,
        CFStringTypeID,
        CFBooleanTypeID,
        CFNumberTypeID,
        CFURLTypeID,
        CFArrayTypeID,
        CFDictionaryTypeID;

        public long getCfRef() {
            return 0x4000 + this.ordinal();
        }
    }


    public static enum MockedConstant {
        kAXTrustedCheckOptionPrompt,
        AXUIElementCreateSystemWide;

        public long getCfRef() {
            return 0x5000 + this.ordinal();
        }
    }

    public static enum MockedURL {
        WWW_QFS_DE("http://www.qfs.de");

        URL url;

        @SneakyThrows
        MockedURL(String url) {
            this.url = new URL(url);
        }

        public long getCfRef() {
            return 0x6000 + this.ordinal();
        }
    }
    public AXJLib axmock = null;
    public AXJLib realLib = null;

    public void beforeEach(ExtensionContext context) throws Exception {
        AXUtilsHelper.clearCfStringCache();
        realLib = AXJLib.INSTANCE;
        axmock = Mockito.mock(AXJLib.class);
        AXJLib.INSTANCE = axmock;
        resetMock();
        TypeConverterHelper.initTypeConstants();
    }

    public void afterEach(ExtensionContext context) throws Exception {
        AXUtilsHelper.clearCfStringCache();
        AXJLib.INSTANCE = realLib;
        TypeConverterHelper.initTypeConstants();
    }


    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        return parameterContext.getParameter().getType() == AXJLib.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        return axmock;
    }

    private void resetMock() throws Exception {
        Mockito.reset(axmock);

        // Initialize Type Ids on mock
        for (MockedTypeId mockedId: MockedTypeId.values()) {
            final String idName = mockedId.name();
            final String methodName = idName.replace("TypeID", "GetTypeID");
            final long cfRef = mockedId.getCfRef();

            whenMethodCallThenReturn(axmock, methodName, cfRef);
        }

        // Initialize ConstantCalls on mock
        for (MockedConstant mockedConstant: MockedConstant.values()) {
            final String constant = mockedConstant.name();
            final long cfRef = mockedConstant.getCfRef();

            whenMethodCallThenReturn(axmock, constant, cfRef);
        }

        // Initialize Strings on mock
        for (MockedString mockedString: MockedString.values()) {
            String string = mockedString.name();
            if ("EMPTY".equals(string)) {
                string= "";
            }
            final long cfRef = mockedString.getCfRef();
            mockString(axmock, cfRef, string);
        }

        // Initialize Booleans on mock
        for (MockedBoolean mockedBoolean: MockedBoolean.values()) {
            final Boolean bool = Boolean.valueOf(mockedBoolean.name());
            final long cfRef = mockedBoolean.getCfRef();
            mockBoolean(axmock, cfRef, bool);
        }

        // Initialize Numbers on mock
        for (MockedNumber mockedNumber: MockedNumber.values()) {
            final Number number = mockedNumber.number;
            final long cfRef = mockedNumber.getCfRef();
            mockNumber(axmock, cfRef, number);
        }

        // Initialize URLs on mock
        for (MockedURL mockedURL: MockedURL.values()) {
            final URL url = mockedURL.url;
            final long cfRef = mockedURL.getCfRef();
            mockURL(axmock, cfRef, url);
        }
        mockAXUIElement(axmock, MockedConstant.AXUIElementCreateSystemWide.getCfRef(), MockedString.AXSystemWide);
    }

    public static void whenMethodCallThenReturn(final AXJLib axmock, final String methodName, final Object cfRef)
        throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        final Method method = AXJLib.class.getDeclaredMethod(methodName);
        method.invoke(axmock); // call mock once to make next when working
        when(null).thenReturn(cfRef); // define value for last call
    }

    public static void mockBoolean(final AXJLib axmock, final long cfRef, final Boolean bool)
    {
        when(axmock.CFBooleanCreateWithJavaBoolean(bool)).thenReturn(cfRef);
        when(axmock.BooleanCreateWithCFBoolean(cfRef)).thenReturn(bool);
        when(axmock.CFGetTypeID(cfRef)).thenReturn(MockedTypeId.CFBooleanTypeID.getCfRef());
    }

    public static void mockNumber(final AXJLib axmock, final long cfRef, final Number number)
    {
        when(axmock.CFNumberCreateWithJavaNumber(number)).thenReturn(cfRef);
        when(axmock.NumberCreateWithCFNumberRef(cfRef)).thenReturn(number);
        when(axmock.CFGetTypeID(cfRef)).thenReturn(MockedTypeId.CFNumberTypeID.getCfRef());
    }

    public static void mockURL(final AXJLib axmock, final long cfRef, final URL url)
    {
        when(axmock.CFURLCreateWithJavaURL(url)).thenReturn(cfRef);
        when(axmock.URLCreateWithCFURLRef(cfRef)).thenReturn(url);
        when(axmock.CFGetTypeID(cfRef)).thenReturn(MockedTypeId.CFURLTypeID.getCfRef());
    }

    public static void mockString(final AXJLib axmock, final long cfRef, String string)
    {
        when(axmock.CFStringCreateWithJavaString(string)).thenReturn(cfRef);
        when(axmock.StringCreateWithCFStringRef(cfRef)).thenReturn(string);
        when(axmock.CFGetTypeID(cfRef)).thenReturn(MockedTypeId.CFStringTypeID.getCfRef());
    }

    public static void mockArray(final AXJLib axmock, final long cfRef, long[] array) {
        when(axmock.CFArrayCreate(eq(array))).thenReturn(cfRef);
        when(axmock.ArrayCopyFromCFArrayRef(cfRef)).thenReturn(array);
        when(axmock.CFGetTypeID(cfRef)).thenReturn(MockedTypeId.CFArrayTypeID.getCfRef());
    }

    public static void mockDictionary(final AXJLib axmock, final long cfRef, long[] keys, long[] values) {
        when(axmock.CFDictionaryCreate(eq(keys), eq(values))).thenReturn(cfRef);
        when(axmock.ArraysCopyFromCFDictionaryRef(cfRef)).thenReturn(new long[][] {keys,values});
        when(axmock.CFGetTypeID(cfRef)).thenReturn(MockedTypeId.CFDictionaryTypeID.getCfRef());
    }

    public static void mockAXUIElement(final AXJLib axmock, final long objRef, final MockedString mockedRole)
    {
        mockAXUIElement(axmock, objRef, mockedRole, null);
    }

    public static void mockAXUIElement(final AXJLib axmock, final long objRef, final MockedString mockedRole, final MockedString mockedSubRole)
    {
        mockAttributeValue(axmock, objRef, MockedString.AXRole, mockedRole.getCfRef());
        if (mockedSubRole != null) {
            mockAttributeValue(axmock, objRef, MockedString.AXSubrole, mockedSubRole.getCfRef());
        }
        when(axmock.CFGetTypeID(objRef)).thenReturn(MockedTypeId.AXUIElementTypeID.getCfRef());
    }

    public static void mockAttributeValue(final AXJLib axmock, final long objRef, final MockedString attributeString,
                                      final long valueRef)
    {
        when(axmock.AXUIElementCopyAttributeValue(eq(objRef),eq(attributeString.getCfRef()), any(long[].class))).thenAnswer(
                answerSuccessWithRefReturn(valueRef));
    }

    public static void mockElementAtPosition(final AXJLib axmock, final long appRef, final float x, final float y,
                                                             final long objRef)
    {
        when(axmock.AXUIElementCopyElementAtPosition(eq(appRef), eq(x), eq(y), any(long[].class))).thenAnswer(
                answerSuccessWithRefReturn(objRef));
    }

    @SuppressWarnings("unchecked")
    public static <T> Answer<?> answerSuccessWithRefReturn(T ref)
    {
        return (invocation) -> {
            final Object[] arguments = invocation.getArguments();
            final int numberOfArguments = arguments.length;
            final int lastIndex = numberOfArguments-1;
            if (ref instanceof Long) {
                ((long[])arguments[lastIndex])[0] = (Long)ref;
            } else if (ref instanceof Integer) {
                ((int[])arguments[lastIndex])[0] = (Integer)ref;
            } else if (ref instanceof Boolean) {
                ((boolean[])arguments[lastIndex])[0] = (Boolean)ref;
            } else {
                ((T[])arguments[lastIndex])[0] = ref;
            }
            return AXError.kAXErrorSuccess.getIntValue();
        };
    }



    @Target({ ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @ParameterizedTest(name="upon {0} error")
    @DisplayName("throws Exception")
    @EnumSource(value=AXError.class, names= {"kAXErrorSuccess"}, mode=Mode.EXCLUDE)
    public @interface ParameterizedAXErrorTest { }

    @Target({ ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @ParameterizedTest(name="upon {0} error")
    @DisplayName("returns false")
    @EnumSource(value=AXError.class, names= {"kAXErrorSuccess"}, mode=Mode.EXCLUDE)
    public @interface ParameterizedAXErrorTestWithFalseUponMissingValue { }

    @Target({ ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @ParameterizedTest(name="upon {0} error")
    @DisplayName("returns null")
    @EnumSource(value=AXError.class, names={"kAXErrorNoValue","kAXErrorAttributeUnsupported","kAXErrorCannotComplete"})
    public @interface ParameterizedAXErrorTestWithNullReturnUponMissingAttribute { }

    @Target({ ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @ParameterizedTest(name="upon {0} error")
    @DisplayName("throws Exception")
    @EnumSource(value=AXError.class, names= {"kAXErrorSuccess","kAXErrorNoValue","kAXErrorAttributeUnsupported","kAXErrorCannotComplete"}, mode=Mode.EXCLUDE)
    public @interface ParameterizedAXErrorTestWithoutMissingAttribute { }
}
