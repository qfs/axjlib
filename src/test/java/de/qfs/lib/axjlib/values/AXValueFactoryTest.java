/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.values;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;

@ExtendWith(MockedAXJLib.class)
public class AXValueFactoryTest
{

    @Nested
    class getValue {

        @Test
        @DisplayName("returns a proper value for a reference for which a AXValue class exists")
        void known(final AXJLib axjlib) throws Exception
        {
            long ref = 12345;
            AXValue value = new CFRange(1,2);

            when(axjlib.AXValueGetValue(ref)).thenReturn(value);

            final AXValue result = AXValueFactory.instance().getValue(ref);

            assertEquals(value,result);
        }

        @Test
        @DisplayName("returns unknown for a reference for which no AXValue class exists")
        void unknown() throws Exception
        {
            long ref = 12345;

            final AXValue result = AXValueFactory.instance().getValue(ref);

            assertEquals(new AXValue.Unknown(ref),result);
        }

        @Test
        @DisplayName("throws exception when AXJLib value creation throws exception")
        void exception(final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXValueGetValue(anyLong())).thenThrow(new RuntimeException("Bad c exception"));

            assertThrows(RuntimeException.class, () -> AXValueFactory.instance().getValue(12));

        }

    }
}
