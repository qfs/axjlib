/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.values;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedTypeId;
import de.qfs.lib.axjlib.values.AXValue.Unknown;

@ExtendWith(MockedAXJLib.class)
public class AXValueTest
{

    @Test
    void createAXValueRef(final AXJLib axmock) throws Exception
    {
        Unknown sut = new Unknown(1234);
        when(axmock.AXValueCreate(sut)).thenReturn(12345L);
        final long ref = sut.createAXValueRef();
        assertEquals(12345L,ref);
        verify(axmock).AXValueCreate(sut);
    }

    @Nested
    @DisplayName("Unknown Class Constructor")
    class UnknownClass {

        @BeforeEach
        void setupMock(final AXJLib axmock) {
            when(axmock.StringCreateWithCFStringRef(104711L)).thenReturn("Bla Object");
            when(axmock.CFGetTypeID(104711L)).thenReturn(MockedTypeId.CFStringTypeID.getCfRef());
            when(axmock.CFCopyDescription(anyLong())).thenReturn(104711L);
        }

        @Test
        @DisplayName("stores reference")
        void reference() throws Exception
        {
            Unknown sut = new Unknown(1234);
            assertEquals(1234,sut.reference);
        }

        @Test
        @DisplayName("requests description")
        void description() throws Exception
        {
            Unknown sut = new Unknown(1234);
            assertEquals("Bla Object",sut.description);
        }

        @Test
        @DisplayName("description String is released after requesting")
        void descriptionRelease(final AXJLib axmock) throws Exception
        {
            new Unknown(1234);
            verify(axmock).CFRelease(104711L);
        }
    }
}
