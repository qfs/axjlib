/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class AXErrorTest
{
    @ParameterizedTest
    @EnumSource(AXError.class)
    @DisplayName("getIntValue returns correct value")
    void getIntValue(final AXError error) {
        assertEquals((int)error.intValue, error.getIntValue());
    }

    @ParameterizedTest
    @EnumSource(AXError.class)
    @DisplayName("forIntValue returns correct object")
    void forIntValue(final AXError error) {
        assertEquals(error,AXError.forIntValue((int)error.intValue));
    }
}
