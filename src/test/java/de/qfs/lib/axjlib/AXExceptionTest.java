/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class AXExceptionTest
{
    @DisplayName("Constructor with integer, getError")
    @Nested
    class IntConstructor {
        @ParameterizedTest
        @EnumSource(AXError.class)
        void valid(final AXError error) {
            AXException ex = new AXException((int)error.intValue, null);
            assertEquals(error,ex.getError());
        }

        @Test
        void invalid() throws Exception
        {
            AXException ex = new AXException(815, null);
            assertNull(ex.getError());
        }

    }

    @ParameterizedTest
    @EnumSource(AXError.class)
    @DisplayName("Constructor with error")
    void ErrorConstructor(final AXError error) {
        AXException ex = new AXException(error, "description");
        assertEquals(error,ex.getError());
    }

    @Nested
    class getMessage {
        @Test
        @DisplayName("contains error and action")
        void errorAndAction() throws Exception
        {
            AXException ex = new AXException(AXError.kAXErrorAPIDisabled, "method");
            assertEquals("kAXErrorAPIDisabled on method", ex.getMessage());
        }

        @Test
        @DisplayName("contains no action when action is null")
        void actionNull() throws Exception
        {
            AXException ex = new AXException(AXError.kAXErrorAPIDisabled, null);
            assertEquals("kAXErrorAPIDisabled", ex.getMessage());
        }

        @Test
        @DisplayName("contains action when error is null")
        void errorNull() throws Exception
        {
            AXException ex = new AXException(null, "bla");
            assertEquals("On bla", ex.getMessage());
        }

        @Test
        @DisplayName("is null when no error an no action is specified")
        void allNull() throws Exception
        {
            AXException ex = new AXException(null, null);
            assertNull(ex.getMessage());
        }

    }
}
