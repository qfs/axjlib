/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.qfs.lib.axjlib.AXError;
import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTest;
import de.qfs.lib.axjlib.utils.AXUtils.CFHashMap;

@ExtendWith(MockedAXJLib.class)
public class AXUtilsTest
{

    @Nested
    class getDescription {
        @Test
        void validDescription(final AXJLib axjlib) throws Exception
        {
            when(axjlib.CFCopyDescription(3012)).thenReturn(MockedString.AXWindow.getCfRef());
            final String description = AXUtils.getDescription(3012);
            assertEquals("AXWindow", description);
        }

        @Test
        void withException(final AXJLib axjlib) throws Exception
        {
            when(axjlib.CFCopyDescription(anyLong())).thenThrow(new RuntimeException("bla test"));
            final ByteArrayOutputStream errOut = new ByteArrayOutputStream();
            final PrintStream err = System.err;
            try {
                System.setErr(new PrintStream(errOut));
                final String description = AXUtils.getDescription(0);
                assertEquals("getDescription excepted: bla test", description);
            } finally {
                System.setErr(err);
            }
            assertTrue(errOut.toString("UTF-8").startsWith("java.lang.RuntimeException: bla test"));
        }

    }

    @Nested
    class callMethodReturningCFTypeRef {

        @Test
        void withProperType() throws Exception
        {
            final Object result = AXUtils.callMethodReturningCFTypeRef(() -> MockedString.AXRole.getCfRef());
            assertEquals("AXRole", result);
        }

        @Test
        void withNullResult() throws Exception
        {
            final Object result = AXUtils.callMethodReturningCFTypeRef(() -> 0);
            assertNull(result);
        }

        @Test
        void withException() throws Exception
        {
            assertThrows(IllegalArgumentException.class, () ->
                AXUtils.callMethodReturningCFTypeRef(() -> {throw new IllegalArgumentException("bla test");}));
        }
    }

    @Nested
    class callAXMethod {

        @Test
        @DisplayName("returning kAXErrorSuccess")
        void kAXErrorSuccess() throws Exception
        {
            AXUtils.callAXMethod(null, () -> AXError.kAXErrorSuccess.getIntValue());
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error) {
            final AXException ex = assertThrows(AXException.class, () ->
                AXUtils.callAXMethod(null, () -> error.getIntValue()));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class callAXMethodWithResult {

        @Test
        @DisplayName("returning 0")
        void nullCase() throws Exception
        {
            final Object result = AXUtils.callAXMethodWithResult(null, (res) -> {
                res[0] = 0;
                return AXError.kAXErrorSuccess.getIntValue();
            });

            assertNull(result);
        }

        @Test
        @DisplayName("returning Object")
        void objectCase() throws Exception
        {
            final Object result = AXUtils.callAXMethodWithResult(null, (res) -> {
                res[0] = MockedAXJLib.MockedBoolean.True.getCfRef();
                return AXError.kAXErrorSuccess.getIntValue();
            });

            assertEquals(Boolean.TRUE, result);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error) {
            final AXException ex = assertThrows(AXException.class, () ->
                AXUtils.callAXMethodWithResult(null, (res) -> error.getIntValue()));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class callAXMethodWithBooleanResult {

        @Test
        @DisplayName("returning true")
        void trueCase() throws Exception
        {
            final boolean result = AXUtils.callAXMethodWithBooleanResult(null, (res) -> {
                res[0] = true;
                return AXError.kAXErrorSuccess.getIntValue();
            });

            assertTrue(result);
        }

        @Test
        @DisplayName("returning false")
        void falseCase() throws Exception
        {
            final boolean result = AXUtils.callAXMethodWithBooleanResult(null, (res) -> {
                res[0] = false;
                return AXError.kAXErrorSuccess.getIntValue();
            });

            assertFalse(result);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error) {
            final AXException ex = assertThrows(AXException.class, () ->
                AXUtils.callAXMethodWithBooleanResult(null, (res) -> error.getIntValue()));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class getCachedCFString {

        @Test
        @DisplayName("returns correct reference")
        void correctRef() throws Exception
        {
            final long ref = AXUtils.getCachedCFString("AXWindow");

            assertEquals(MockedAXJLib.MockedString.AXWindow.getCfRef(), ref);
        }

        @Test
        @DisplayName("caches result")
        void doesCaching(final AXJLib axjlib) throws Exception
        {
            final long ref1 = AXUtils.getCachedCFString("AXRole");
            verify(axjlib,times(1)).CFStringCreateWithJavaString("AXRole");

            final long ref2 = AXUtils.getCachedCFString("AXRole");

            assertEquals(ref1,ref2);
            verify(axjlib,times(1)).CFStringCreateWithJavaString("AXRole");
        }

        @Test
        @DisplayName("with empty string")
        void empty() throws Exception
        {
            final long ref = AXUtils.getCachedCFString("");

            assertEquals(MockedAXJLib.MockedString.EMPTY.getCfRef(), ref);
        }

        @Test
        @DisplayName("with null string")
        void nullString() throws Exception
        {
            final long ref = AXUtils.getCachedCFString(null);

            assertEquals(0, ref);
        }

    }

    @Nested
    @DisplayName("CFHashMap")
    class CFHashMapTest {

        CFHashMap<Long> map = new CFHashMap<>();

        @BeforeEach
        void setupMap() {
            for(long i = 0; i < 50; i++) {
                map.put(i,1000 + i);
            }
        }

        @Test
        @DisplayName("clear releases all entries")
        void clear(final AXJLib axjlib) throws Exception
        {
            map.clear();

            for(long i = 0; i < 50; i++) {
                verify(axjlib).CFRelease(1000+i);
            }

        }

        @Test
        @DisplayName("remove releases entry")
        void remove(final AXJLib axjlib) throws Exception
        {
            final Long removed = map.remove(2L);

            assertEquals(1002L,(long) removed);

            verify(axjlib).CFRelease(1002);
            verify(axjlib).CFRelease(anyLong());
        }

        @Test
        @DisplayName("remove with 2 arguments releases entry")
        void remove2(final AXJLib axjlib) throws Exception
        {
            final boolean removed = map.remove(3L, 1003L);

            assertTrue(removed);

            verify(axjlib).CFRelease(1003);
            verify(axjlib).CFRelease(anyLong());
        }

        @Test
        @DisplayName("remove with 2 arguments does not remove entry with wrong value")
        void notRemove2(final AXJLib axjlib) throws Exception
        {
            final boolean removed = map.remove(3L, 3L);

            assertFalse(removed);

            verify(axjlib, never()).CFRelease(anyLong());
        }


        @Test
        @DisplayName("replace releases entry")
        void replace(final AXJLib axjlib) throws Exception
        {
            final Long replaced = map.replace(2L, 2002L);

            assertEquals(1002L,(long) replaced);

            verify(axjlib).CFRelease(1002);
            verify(axjlib).CFRelease(anyLong());
        }

        @Test
        @DisplayName("replace does not release entry when currently not mapped")
        void notReplace(final AXJLib axjlib) throws Exception
        {
            final Long replaced = map.replace(102L, 2002L);

            assertNull(replaced);

            verify(axjlib, never()).CFRelease(anyLong());
        }

        @Test
        @DisplayName("replace with 3 arguments releases entry")
        void replace3(final AXJLib axjlib) throws Exception
        {
            final boolean replaced = map.replace(3L, 1003L, 2003L);

            assertTrue(replaced);

            verify(axjlib).CFRelease(1003);
            verify(axjlib).CFRelease(anyLong());
        }

        @Test
        @DisplayName("replace with 3 arguments does not remove entry with wrong value")
        void notReplace3(final AXJLib axjlib) throws Exception
        {
            final boolean removed = map.replace(3L, 3L, 2003L);

            assertFalse(removed);

            verify(axjlib, never()).CFRelease(anyLong());
        }

    }


}
