/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.utils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URL;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedBoolean;
import de.qfs.lib.axjlib.MockedAXJLib.MockedNumber;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;
import de.qfs.lib.axjlib.MockedAXJLib.MockedTypeId;
import de.qfs.lib.axjlib.MockedAXJLib.MockedURL;
import de.qfs.lib.axjlib.ui.MockedAXUIElementFactory;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AXUIElementFactory;
import de.qfs.lib.axjlib.utils.TypeConverter.UnknownType;
import de.qfs.lib.axjlib.values.AXValue;
import de.qfs.lib.axjlib.values.AXValueFactory;
import de.qfs.lib.axjlib.values.CFRange;
import de.qfs.lib.axjlib.values.CGPoint;
import de.qfs.lib.axjlib.values.CGRect;
import de.qfs.lib.axjlib.values.MockedAXValueFactory;

@ExtendWith(MockedAXJLib.class)
public class TypeConverterTest
{

    TypeConverter typeConverter;

    @BeforeEach
    void setTypeConverter() {
        typeConverter = TypeConverter.instance();
    }

    @Nested
    class referenceIsOfType {
        @Test
        @DisplayName("with matching type")
        void testTrue(final AXJLib axjlib) throws Exception
        {
            final boolean result = typeConverter.referenceIsOfType(MockedString.AXRole.getCfRef(), MockedTypeId.CFStringTypeID.getCfRef());
            assertTrue(result);
        }

        @Test
        @DisplayName("without matching type")
        void testFalse() throws Exception
        {
            final boolean result = typeConverter.referenceIsOfType(MockedString.AXRole.getCfRef(), MockedTypeId.CFArrayTypeID.getCfRef());
            assertFalse(result);
        }

    }

    @Nested
    class convertFromCFTypeRef {

        @Test
        @DisplayName("converts 0")
        void convert0() throws Exception
        {
            assertNull(typeConverter.convertFromCFTypeRef(0));
        }

        @Test
        @DisplayName("converts String")
        void string() throws Exception
        {
            final Object result = typeConverter.convertFromCFTypeRef(MockedString.AXMenuBar.getCfRef());
            assertEquals("AXMenuBar", result);
        }

        @Test
        @DisplayName("converts Boolean")
        void bool() throws Exception
        {
            final Object result = typeConverter.convertFromCFTypeRef(MockedBoolean.True.getCfRef());
            assertEquals(Boolean.TRUE, result);
        }

        @Test
        @DisplayName("converts Number")
        void number() throws Exception
        {
            final Object result = typeConverter.convertFromCFTypeRef(MockedNumber.Number1.getCfRef());
            assertEquals(1, ((Number)result).intValue());
        }

        @Test
        @DisplayName("converts URL")
        void url() throws Exception
        {
            final Object result = typeConverter.convertFromCFTypeRef(MockedURL.WWW_QFS_DE.getCfRef());
            assertEquals(new URL("http://www.qfs.de"), ((URL)result));
        }

        @Test
        @ExtendWith(MockedAXValueFactory.class)
        @DisplayName("converts Value")
        void value(final AXJLib axmock, final AXValueFactory valueFactory) throws Exception
        {
            final long ref = 10000;

            when(axmock.CFGetTypeID(ref)).thenReturn(MockedTypeId.AXValueTypeID.getCfRef());
            when(valueFactory.getValue(ref)).thenReturn(new CFRange(1,2));

            final Object result = typeConverter.convertFromCFTypeRef(ref);
            assertEquals(new CFRange(1,2), result);
        }

        @Test
        @ExtendWith(MockedAXUIElementFactory.class)
        @DisplayName("converts Element")
        void element(final AXJLib axmock, final AXUIElementFactory elementFactory) throws Exception
        {
            final AXUIElement element = Mockito.mock(AXUIElement.class);

            final long ref = 1002;

            when(axmock.CFGetTypeID(ref)).thenReturn(MockedTypeId.AXUIElementTypeID.getCfRef());
            when(elementFactory.getElement(ref)).thenReturn(element);

            final Object result = typeConverter.convertFromCFTypeRef(ref);
            assertEquals(element, result);
        }

        @Test
        @DisplayName("converts Array")
        void array(final AXJLib axmock) throws Exception
        {
            final long ref = 1002;

            MockedAXJLib.mockArray(axmock, ref, new long[] {MockedString.AXSystemWide.getCfRef()});

            final Object result = typeConverter.convertFromCFTypeRef(ref);

            assertEquals(Arrays.asList("AXSystemWide"), result);
        }

        @Test
        @DisplayName("converts Dictionary")
        void dict(final AXJLib axmock) throws Exception
        {
            final long ref = 1005;
            MockedAXJLib.mockDictionary(axmock, ref,
                    new long[] {MockedString.AXApplication.getCfRef()},
                    new long[] {MockedBoolean.True.getCfRef()});

            final Object result = typeConverter.convertFromCFTypeRef(ref);

            Map<String,Boolean> expected = new HashMap<>();
            expected.put("AXApplication", Boolean.TRUE);
            assertEquals(expected, result);

        }

        @Test
        @DisplayName("does not fail on unknown type")
        void unknown(final AXJLib axmock) throws Exception
        {
            final long ref = 1003;

            when(axmock.CFGetTypeID(ref)).thenReturn(1234567L);

            final Object result = typeConverter.convertFromCFTypeRef(ref);

            assertTrue(result instanceof UnknownType);
            assertEquals(ref, ((UnknownType) result).getReference());
        }

    }

    @Nested
    class createCFTypeRef {

        @Test
        @DisplayName("creates CFString")
        void string(final AXJLib axmock) throws Exception
        {
            typeConverter.createCFTypeRef("Bla");

            verify(axmock).CFStringCreateWithJavaString("Bla");
        }

        @Test
        @DisplayName("creates CFBoolean")
        void bool(final AXJLib axmock) throws Exception
        {
            typeConverter.createCFTypeRef(Boolean.TRUE);

            verify(axmock).CFBooleanCreateWithJavaBoolean(Boolean.TRUE);
        }

        @Test
        @DisplayName("creates CFNumber")
        void number(final AXJLib axmock) throws Exception
        {
            typeConverter.createCFTypeRef(23);

            verify(axmock).CFNumberCreateWithJavaNumber(23);
        }

        @Test
        @DisplayName("creates CFURL")
        void url(final AXJLib axmock) throws Exception
        {
            typeConverter.createCFTypeRef(new URL("http://www.qfs.de"));

            verify(axmock).CFURLCreateWithJavaURL(new URL("http://www.qfs.de"));
        }

        @Test
        @DisplayName("creates Value")
        void value(final AXJLib axmock) throws Exception
        {
            final CGRect value = new CGRect(1,2,3,4);
            typeConverter.createCFTypeRef(value);
            verify(axmock).AXValueCreate(value);
        }

        @Test
        @DisplayName("creates CFArray from collection")
        void array(final AXJLib axmock) throws Exception
        {
            when(axmock.CFStringCreateWithJavaString("Bla")).thenReturn(12345L);
            Queue<String> queue = new LinkedList<>();
            queue.add("Bla");

            typeConverter.createCFTypeRef(queue);
            verify(axmock).CFStringCreateWithJavaString("Bla");
            verify(axmock).CFArrayCreate(eq(new long[] {12345L}));
            verify(axmock).CFRelease(12345L);
        }

        @Test
        @DisplayName("creates CFDictionary from Map")
        void map(final AXJLib axmock) throws Exception
        {
            when(axmock.CFStringCreateWithJavaString("Point")).thenReturn(12345L);
            when(axmock.AXValueCreate(eq(new CGPoint(1,2)))).thenReturn(121212L);
            Map<String, AXValue> map = new HashMap<>();
            map.put("Point", new CGPoint(1,2));

            typeConverter.createCFTypeRef(map);

            verify(axmock).CFStringCreateWithJavaString("Point");
            verify(axmock).AXValueCreate(eq(new CGPoint(1,2)));
            verify(axmock).CFDictionaryCreate(eq(new long[] {12345L}),eq(new long[] {121212}));
            verify(axmock).CFRelease(12345L);
            verify(axmock).CFRelease(121212L);
        }

        @Test
        @DisplayName("creates null reference")
        void nullRef() throws Exception
        {
            assertEquals(0,typeConverter.createCFTypeRef(null));
        }


        @Test
        @DisplayName("Throws exception upon unknown types")
        void except() throws Exception
        {
            assertThrows(IllegalArgumentException.class, () ->
                typeConverter.createCFTypeRef(new Object())
            );
        }

    }
}
