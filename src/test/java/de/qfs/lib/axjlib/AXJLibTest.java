/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.File;

import java.net.URL;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.LongConsumer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import de.qfs.lib.axjlib.DemoAppEnvironment.DemoApp;
import de.qfs.lib.axjlib.appkit.NSRunningApplication;
import de.qfs.lib.axjlib.utils.TypeConverter;
import de.qfs.lib.axjlib.values.AXValue;
import de.qfs.lib.axjlib.values.CFRange;
import de.qfs.lib.axjlib.values.CGPoint;
import de.qfs.lib.axjlib.values.CGRect;
import de.qfs.lib.axjlib.values.CGSize;

import lombok.RequiredArgsConstructor;


/**
 * Tests the native library, so it only runs on macos
 *
 * @author Pascal Bihler
 *
 */
@ExtendWith(MacOsExecutor.class)
public class AXJLibTest
{

    abstract class WithName {

        final String name;
        long cfNameRef = 0;

        WithName() {
            name = "Name";
        }

        WithName(String name) {
            this.name = name;
        }

        @BeforeEach
        void setUpCfNameRef() {
            cfNameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString(name);
        }

        @AfterEach
        void releaseCfNameRef() {
            AXJLib.INSTANCE.CFRelease(cfNameRef);
            cfNameRef = 0;
        }
    }

    @Test
    @DisplayName("AXIsProcessTrustedWithOptions, option true")
    void AXIsProcessTrustedWithOptions() throws Exception
    {
        testTruestedWithOptions(true);
    }

    protected void testTruestedWithOptions(Boolean informUser)
    {
        long booleanRef = 0;
        long options = 0;

        try {
            booleanRef = informUser == null ? 0 : AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(informUser);
            try {
            options = booleanRef == 0 ? 0 : AXJLib.INSTANCE.CFDictionaryCreate(
                    new long[] {AXJLib.INSTANCE.kAXTrustedCheckOptionPrompt()}, new long[] {booleanRef});

            boolean trusted = AXJLib.INSTANCE.AXIsProcessTrustedWithOptions(options);
            assertTrue(trusted);

            } finally {
                AXJLib.INSTANCE.CFRelease(booleanRef);
            }
        } finally {
            AXJLib.INSTANCE.CFRelease(options);
        }
    }

    @Nested
    @ExtendWith(DemoAppEnvironment.class)
    public class AXMethods {

        @BeforeEach
        void assertProcessTrustedAndStartDemo() throws Exception {
            assumeTrue(AXJLib.INSTANCE.AXIsProcessTrusted(), "Process is trusted");
        }

        @Test
        void AXIsProcessTrusted() throws Exception
        {
            final boolean trusted = AXJLib.INSTANCE.AXIsProcessTrusted();
            assertTrue(trusted, "Otherwise we cannot run other tests.");
        }

        @Nested
        public class AXIsProcessTrustedWithOptions {
            @Test
            @DisplayName("options false")
            void optionsFalse() throws Exception
            {
                testTruestedWithOptions(false);
            }

            @Test
            @DisplayName("options null")
            void optionsNull() throws Exception
            {
                testTruestedWithOptions(false);
            }
        }


        @Test
        void AXUIElementGetTypeID(final DemoApp demoApp) throws Exception
        {
            withApplication(demoApp, (appRef) -> {
                long typeId = AXJLib.INSTANCE.AXUIElementGetTypeID();
                assertEquals(typeId,AXJLib.INSTANCE.CFGetTypeID(appRef));
            });
        }

        @Nested
        public class AXUIElementCopyAttributeNames {

            @Test
            @DisplayName("with proper arguments")
            void proper(final DemoApp demoApp) throws Exception
            {
                Set<String> names = new HashSet<>();
                withButton(demoApp, (ref) -> {
                    long[] arrayRef = new long[1];
                    try {
                        int result = AXJLib.INSTANCE.AXUIElementCopyAttributeNames(ref, arrayRef);
                        assertEquals(0,result);
                        long[] nameRefs = AXJLib.INSTANCE.ArrayCopyFromCFArrayRef(arrayRef[0]);
                        for (long nameRef: nameRefs) {
                            assertNotEquals(0,nameRef);
                            try {
                                String name = AXJLib.INSTANCE.StringCreateWithCFStringRef(nameRef);
                                names.add(name);
                            } finally {
                                AXJLib.INSTANCE.CFRelease(nameRef);
                            }
                        }

                    } finally {
                        AXJLib.INSTANCE.CFRelease(arrayRef[0]);
                    }
                });
                assertTrue(names.size() > 5);
                assertTrue(names.contains("AXRole"));
                assertTrue(names.contains("AXWindow"));
                assertTrue(names.contains("AXParent"));
                assertTrue(names.contains("AXDescription"));
            }

            @Test
            @DisplayName("with null element")
            void nullElement() throws Exception
            {
                long[] arrayRef = new long[1];
                try {
                    int result = AXJLib.INSTANCE.AXUIElementCopyAttributeNames(0, arrayRef);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                } finally {
                    AXJLib.INSTANCE.CFRelease(arrayRef[0]);
                }
            }

            @Test
            @DisplayName("with null array")
            void nullArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyAttributeNames(ref, null)));
            }

            @Test
            @DisplayName("with 0 sized array")
            void nullSizedArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyAttributeNames(ref, new long[0])));
            }

        }

        @Nested
        class AXUIElementCopyAttributeValue extends WithName {

            AXUIElementCopyAttributeValue() {
                super("AXRole");
            }

            @Test
            @DisplayName("Proper usage")
            void properUsage(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    String value = getStringValue(ref, "AXRole");
                    assertEquals("AXButton",value);
                });
            }

            @Test
            @DisplayName("with null ref")
            void nullRef() throws Exception
            {
                long[] valueRef = new long[1];
                try {
                    int result = AXJLib.INSTANCE.AXUIElementCopyAttributeValue(0, cfNameRef, valueRef);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                } finally {
                    AXJLib.INSTANCE.CFRelease(valueRef[0]);
                }
            }

            @Test
            @DisplayName("with null name")
            void nullName(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long[] valueRef = new long[1];
                    try {
                        int result = AXJLib.INSTANCE.AXUIElementCopyAttributeValue(ref, 0, valueRef);
                        assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                    } finally {
                        AXJLib.INSTANCE.CFRelease(valueRef[0]);
                    }
                });
            }

            @Test
            @DisplayName("with invalid name")
            void invalidName(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long[] valueRef = new long[1];
                    try {
                        long nameRef = 0;
                        try {
                            nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("Blubber");
                            int result = AXJLib.INSTANCE.AXUIElementCopyAttributeValue(ref, nameRef, valueRef);
                            assertEquals(AXError.kAXErrorAttributeUnsupported,AXError.forIntValue(result));
                        } finally {
                            AXJLib.INSTANCE.CFRelease(nameRef);
                        }
                    } finally {
                        AXJLib.INSTANCE.CFRelease(valueRef[0]);
                    }
                });
            }

            @Test
            @DisplayName("with null array")
            void nullArray(final DemoApp demoApp) throws Exception
            {

                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyAttributeValue(0, cfNameRef, null)));
            }

            @Test
            @DisplayName("with 0 sized array")
            void nullSizedArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyAttributeValue(0, cfNameRef, new long[0])));
            }
        }

        @Nested
        class AXUIElementIsAttributeSettable extends WithName {

            AXUIElementIsAttributeSettable() {
                super("AXRole");
            }

            @Test
            @DisplayName("for readonly attributes")
            void settableFalse(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    boolean[] settable = new boolean[1];
                    int result = AXJLib.INSTANCE.AXUIElementIsAttributeSettable(ref, cfNameRef, settable);
                    assertEquals(0,result);
                    assertFalse(settable[0]);

                });
            }

            @Test
            @DisplayName("for readwrite attributes")
            void settableTrue(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long nameRef = 0;
                    boolean[] settable = new boolean[1];
                    try {
                        nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("AXFocused");
                        int result = AXJLib.INSTANCE.AXUIElementIsAttributeSettable(ref, nameRef, settable);
                        assertEquals(0,result);
                        assertTrue(settable[0]);
                    } finally {
                        AXJLib.INSTANCE.CFRelease(nameRef);
                    }
                });
            }

            @Test
            @DisplayName("with null reference")
            void nullRef() throws Exception
            {
                long nameRef = 0;
                boolean[] settable = new boolean[1];
                try {
                    nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("AXRole");
                    int result = AXJLib.INSTANCE.AXUIElementIsAttributeSettable(0, nameRef, settable);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                } finally {
                    AXJLib.INSTANCE.CFRelease(nameRef);
                }
            }

            @Test
            @DisplayName("with null name")
            void nullName(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    boolean[] settable = new boolean[1];
                    int result = AXJLib.INSTANCE.AXUIElementIsAttributeSettable(ref, 0, settable);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                });
            }

            @Test
            @DisplayName("with invalid name")
            void invalidName(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    boolean[] settable = new boolean[1];
                    long nameRef = 0;
                    try {
                        nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("Blubber");
                        int result = AXJLib.INSTANCE.AXUIElementIsAttributeSettable(ref, nameRef, settable);
                        assertEquals(0,result);
                        assertFalse(settable[0]);
                    } finally {
                        AXJLib.INSTANCE.CFRelease(nameRef);
                    }
                });
            }

            @Test
            @DisplayName("with null array")
            void nullArray(final DemoApp demoApp) throws Exception
            {

                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementIsAttributeSettable(0, cfNameRef, null)));
            }

            @Test
            @DisplayName("with 0 sized array")
            void nullSizedArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementIsAttributeSettable(0, cfNameRef, new boolean[0])));
            }
        }

        @Nested
        class AXUIElementSetAttributeValue extends WithName {

            AXUIElementSetAttributeValue() {
                super("AXFocused");
            }

            @Test
            void properly(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long[] valueRef = new long[1];
                    int result = AXJLib.INSTANCE.AXUIElementCopyAttributeValue(ref, cfNameRef, valueRef);
                    assertEquals(0,result);

                    Boolean isFocused = AXJLib.INSTANCE.BooleanCreateWithCFBoolean(valueRef[0]);
                    assertFalse(isFocused);
                    AXJLib.INSTANCE.CFRelease(valueRef[0]);

                    long newValueRef = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(true);
                    result = AXJLib.INSTANCE.AXUIElementSetAttributeValue(ref, cfNameRef, newValueRef);
                    assertEquals(0,result);

                    result = AXJLib.INSTANCE.AXUIElementCopyAttributeValue(ref, cfNameRef, valueRef);
                    assertEquals(0,result);

                    isFocused = AXJLib.INSTANCE.BooleanCreateWithCFBoolean(valueRef[0]);
                    assertTrue(isFocused);
                    AXJLib.INSTANCE.CFRelease(valueRef[0]);

                });
            }

            @Test
            @DisplayName("with null reference")
            void nullRef() throws Exception
            {
                long newValueRef = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(true);
                int result = AXJLib.INSTANCE.AXUIElementSetAttributeValue(0, cfNameRef, newValueRef);
                assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));

            }
            @Test
            @DisplayName("with null name")
            void nullName(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long newValueRef = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(true);
                    int result = AXJLib.INSTANCE.AXUIElementSetAttributeValue(ref, 0, newValueRef);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                });
            }

            @Test
            @DisplayName("with invalid name")
            void invalidName(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long nameRef = 0;
                    try {
                        nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("Bla");
                        long newValueRef = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(true);
                        int result = AXJLib.INSTANCE.AXUIElementSetAttributeValue(ref, nameRef, newValueRef);
                        assertEquals(AXError.kAXErrorAttributeUnsupported,AXError.forIntValue(result));
                    } finally {
                        AXJLib.INSTANCE.CFRelease(nameRef);
                    }
                });
            }

            @Test
            @DisplayName("with null value reference")
            void nullValueReference(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    int result = AXJLib.INSTANCE.AXUIElementSetAttributeValue(ref, cfNameRef, 0);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                });
            }
        }


/*
        public native int AXUIElementCopyParameterizedAttributeNames (long element, long[] arrayRef); // Demo?
*/

        @Nested
        class AXUIElementCopyActionNames {
            @Test
            @DisplayName("proper usage")
            void properly(final DemoApp demoApp) throws Exception
            {
                Set<String> names = new HashSet<>();
                withButton(demoApp, (ref) -> {
                    long[] arrayRef = new long[1];
                    try {
                        int result = AXJLib.INSTANCE.AXUIElementCopyActionNames(ref, arrayRef);
                        assertEquals(0,result);
                        long[] nameRefs = AXJLib.INSTANCE.ArrayCopyFromCFArrayRef(arrayRef[0]);
                        for (long nameRef: nameRefs) {
                            assertNotEquals(0,nameRef);
                            try {
                                String name = AXJLib.INSTANCE.StringCreateWithCFStringRef(nameRef);
                                names.add(name);
                            } finally {
                                AXJLib.INSTANCE.CFRelease(nameRef);
                            }
                        }

                    } finally {
                        AXJLib.INSTANCE.CFRelease(arrayRef[0]);
                    }
                });
                assertTrue(names.size() > 0);
                assertTrue(names.contains("AXPress"));
            }


            @Test
            @DisplayName("with null element")
            void nullElement() throws Exception
            {
                long[] arrayRef = new long[1];
                try {
                    int result = AXJLib.INSTANCE.AXUIElementCopyActionNames(0, arrayRef);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                } finally {
                    AXJLib.INSTANCE.CFRelease(arrayRef[0]);
                }
            }

            @Test
            @DisplayName("with null array")
            void nullArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyActionNames(ref, null)));
            }

            @Test
            @DisplayName("with 0 sized array")
            void nullSizedArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyActionNames(ref, new long[0])));
            }

        }

        @Nested
        class AXUIElementCopyActionDescription extends WithName {

            AXUIElementCopyActionDescription() {
                super("AXPress");
            }

            @Test
            @DisplayName("proper usage")
            void properUsage(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long[] valueRef = new long[1];
                    try {
                        int result = AXJLib.INSTANCE.AXUIElementCopyActionDescription(ref, cfNameRef, valueRef);
                        assertEquals(0,result);
                        final String value = AXJLib.INSTANCE.StringCreateWithCFStringRef(valueRef[0]);
                        assertEquals("press",value);
                    } finally {
                        AXJLib.INSTANCE.CFRelease(valueRef[0]);
                    }
                });
            }


            @Test
            @DisplayName("with null ref")
            void nullRef() throws Exception
            {
                long[] valueRef = new long[1];
                try {
                    int result = AXJLib.INSTANCE.AXUIElementCopyActionDescription(0, cfNameRef, valueRef);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                } finally {
                    AXJLib.INSTANCE.CFRelease(valueRef[0]);
                }
            }

            @Test
            @DisplayName("with null action")
            void nullAction(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long[] valueRef = new long[1];
                    try {
                        int result = AXJLib.INSTANCE.AXUIElementCopyActionDescription(ref, 0, valueRef);
                        assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                    } finally {
                        AXJLib.INSTANCE.CFRelease(valueRef[0]);
                    }
                });
            }

            @Test
            @DisplayName("with invalid action")
            void invalidAction(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long[] valueRef = new long[1];
                    try {
                        long nameRef = 0;
                        try {
                            nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("Blubber");
                            int result = AXJLib.INSTANCE.AXUIElementCopyActionDescription(ref, nameRef, valueRef);
                            assertTrue(EnumSet.of(AXError.kAXErrorActionUnsupported,AXError.kAXErrorAttributeUnsupported).contains(AXError.forIntValue(result)));
                        } finally {
                            AXJLib.INSTANCE.CFRelease(nameRef);
                        }
                    } finally {
                        AXJLib.INSTANCE.CFRelease(valueRef[0]);
                    }
                });
            }

            @Test
            @DisplayName("with null array")
            void nullArray(final DemoApp demoApp) throws Exception
            {

                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyActionDescription(0, cfNameRef, null)));
            }

            @Test
            @DisplayName("with 0 sized array")
            void nullSizedArray(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> assertThrows(IllegalArgumentException.class,
                        () -> AXJLib.INSTANCE.AXUIElementCopyActionDescription(0, cfNameRef, new long[0])));
            }
        }

        @Nested
        class AXUIElementPerformAction extends WithName {

            AXUIElementPerformAction() {
                super("AXPress");
            }

            @Test
            @DisplayName("Proper usage")
            void properUsage(final DemoApp demoApp) throws Exception
            {
                withElement(demoApp, 450, 85, (labelRef) -> {
                    String label = getStringValue(labelRef, "AXValue");
                    assertEquals("X's turn", label);

                    withElement(demoApp, 170, 200, (buttonRef) -> {
                        int result = AXJLib.INSTANCE.AXUIElementPerformAction(buttonRef, cfNameRef);
                        assertEquals(0,result);
                    });

                    label = getStringValue(labelRef, "AXValue");
                    assertEquals("O's turn", label);

                });
            }


            @Test
            @DisplayName("with null ref")
            void nullRef() throws Exception
            {
                int result = AXJLib.INSTANCE.AXUIElementPerformAction(0, cfNameRef);
                assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
            }

            @Test
            @DisplayName("with null action")
            void nullAction(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    int result = AXJLib.INSTANCE.AXUIElementPerformAction(ref, 0);
                    assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
                });
            }

            @Test
            @DisplayName("with invalid action")
            void invalidAction(final DemoApp demoApp) throws Exception
            {
                withButton(demoApp, (ref) -> {
                    long nameRef = 0;
                    try {
                        nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("Blubber");
                        int result = AXJLib.INSTANCE.AXUIElementPerformAction(ref, nameRef);
                        assertTrue(EnumSet.of(AXError.kAXErrorActionUnsupported,AXError.kAXErrorAttributeUnsupported).contains(AXError.forIntValue(result)));
                    } finally {
                        AXJLib.INSTANCE.CFRelease(nameRef);
                    }
                });
            }
        }

        @Test
        void AXUIElementCopyElementAtPosition(final DemoApp demoApp) throws Exception
        {
            withButton(demoApp, (ref) -> assertNotEquals(0,ref));
        }

        @Test
        void AXUIElementCreateApplication(final DemoApp demoApp) {
            withApplication(demoApp, (ref) -> {
                assertTrue(ref > 0);
                assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(ref));
            });
        }

        @Test
        void AXUIElementGetPid(final DemoApp demoApp) throws Exception
        {
            withApplication(demoApp, (appRef) ->
            {
                int[] p_pid = new int[1];
                int result = AXJLib.INSTANCE.AXUIElementGetPid(appRef, p_pid);
                assertEquals(0,result);
                assertEquals(demoApp.pid, p_pid[0]);
            });
        }

        @Test
        public void AXUIElementCreateSystemWide() {
            long ref = 0;
            try {
                ref = AXJLib.INSTANCE.AXUIElementCreateSystemWide();
                assertTrue(ref > 0);
                assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(ref));
            } finally {
                AXJLib.INSTANCE.CFRelease(ref);
            }
        }

        @Test
        void AXUIElementSetMessagingTimeout(final DemoApp demoApp) throws Exception
        {
            // check if negative value is invalid (all other checks are system dependent and therefore flaky)
            withApplication(demoApp, (appRef) -> {
                int result = AXJLib.INSTANCE.AXUIElementSetMessagingTimeout(appRef,-1f);
                assertEquals(AXError.kAXErrorIllegalArgument,AXError.forIntValue(result));
            });
        }


        void withApplication(final DemoApp demoApp, LongConsumer method) {
            long ref = 0;
            try {
                ref = AXJLib.INSTANCE.AXUIElementCreateApplication(demoApp.pid);
                method.accept(ref);
            } finally {
                AXJLib.INSTANCE.CFRelease(ref);
            }
        }

        void withButton(final DemoApp demoApp, LongConsumer method) {
            withElement(demoApp, 470, 320, method);
        }

        void withElement(final DemoApp demoApp, int x, int y, LongConsumer method) {
            withApplication(demoApp,(appRef) ->
            {
                long[] p_element = new long[1];
                try {
                    float absoluteX = demoApp.x.floatValue() + x;
                    float absoluteY = demoApp.y.floatValue() + y;
                    int result = AXJLib.INSTANCE.AXUIElementCopyElementAtPosition(appRef,absoluteX,absoluteY,p_element);
                    assertEquals(0,result);
                    method.accept(p_element[0]);
                } finally {
                    if (p_element[0] != 0)  AXJLib.INSTANCE.CFRelease(p_element[0]);
                }
            });
        }

        String getStringValue(long ref, String name) {
            long nameRef = 0;
            long[] valueRef = new long[1];
            try {
                nameRef = AXJLib.INSTANCE.CFStringCreateWithJavaString(name);
                int res = AXJLib.INSTANCE.AXUIElementCopyAttributeValue(ref, nameRef, valueRef);
                assertEquals(0,res);
                return AXJLib.INSTANCE.StringCreateWithCFStringRef(valueRef[0]);
            } finally {
                AXJLib.INSTANCE.CFRelease(valueRef[0]);
                AXJLib.INSTANCE.CFRelease(nameRef);
            }
        }
    }

    @Nested
    public class CFValueMethods {

        @Test
        void AXValueGetTypeID() throws Exception
        {
            long valRef = 0;
            try {
                AXValue value = new CFRange(1,1);

                valRef = AXJLib.INSTANCE.AXValueCreate(value);
                assertNotEquals(0, valRef);
                long valueTypeId = AXJLib.INSTANCE.CFGetTypeID(valRef);
                assertEquals(valueTypeId, AXJLib.INSTANCE.AXValueGetTypeID());
            } finally {
                AXJLib.INSTANCE.CFRelease(valRef);
            }
        }

        // wrong type tests (AXValueCreate, AXValueGetValue)

        @Nested
        class AXValueCreate {

            @RequiredArgsConstructor
            abstract class CreationTests {
                final long valueType;
                final AXValue value;
                final AXValue wrongValue;

                @Test
                @DisplayName("proper usage")
                void properUsage() throws Exception
                {
                    long valueRef = 0;
                    try {
                        valueRef = AXJLib.INSTANCE.AXValueCreate(value);
                        assertNotEquals(0, valueRef);
                        long type = AXJLib.INSTANCE.AXValueGetType(valueRef);
                        assertEquals(valueType,type);

                        AXValue valRefValue = AXJLib.INSTANCE.AXValueGetValue(valueRef);
                        assertEquals(value, valRefValue);
                    } finally {
                        AXJLib.INSTANCE.CFRelease(valueRef);
                    }
                }

                @Test
                @DisplayName("null value")
                void nullValue() throws Exception
                {
                    assertThrows(IllegalArgumentException.class, () -> AXJLib.INSTANCE.AXValueCreate(null));
                }

            }

            @Nested
            @DisplayName("CGPoint")
            class CGPointTests extends CreationTests {
                CGPointTests() {
                    super(AXValue.kAXValueCGPointType, new CGPoint(1.3f,4.7f), new CGSize(1.3f,4.7f));
                }
            }

            @Nested
            @DisplayName("CGSize")
            class CGSizeTests extends CreationTests {
                CGSizeTests() {
                    super(AXValue.kAXValueCGSizeType, new CGSize(1.3f,4.7f), new CGPoint(1.3f,4.7f));
                }
            }

            @Nested
            @DisplayName("CGRect")
            class CGRectTests extends CreationTests {
                CGRectTests() {
                    super(AXValue.kAXValueCGRectType, new CGRect(1f,2f,3f,4f), new CGSize(1.3f,4.7f));
                }
            }

            @Nested
            @DisplayName("CFRange")
            class CFRangeTests extends CreationTests {
                CFRangeTests() {
                    super(AXValue.kAXValueCFRangeType, new CFRange(1,2), new CGSize(1.3f,4.7f));
                }
            }
        }

        @Nested
        class AXValueGetType {
            @Test
            @DisplayName("null reference")
            void nullReference() throws Exception
            {
                long type = AXJLib.INSTANCE.AXValueGetType(0);
                assertEquals(0,type);
            }
        }

        @Nested
        class AXValueGetValue {

            @Test
            @DisplayName("null reference")
            void nullReference() throws Exception
            {
                AXValue valRefValue = AXJLib.INSTANCE.AXValueGetValue(0);
                assertNull(valRefValue);
            }
        }
    }

    @Nested
    class CFMethods {

        @Nested
        @DisplayName("CFRetain, CFGetRetainCount, CFRelease")
        class RetainRelease extends WithName {

            @Test
            @DisplayName("proper usage")
            void properly() throws Exception
            {

                assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(cfNameRef));
                AXJLib.INSTANCE.CFRetain(cfNameRef);
                assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(cfNameRef));
                AXJLib.INSTANCE.CFRelease(cfNameRef);
                assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(cfNameRef));
            }

            @Test
            @DisplayName("CFRetain with 0 ref")
            void retain0() throws Exception
            {
                AXJLib.INSTANCE.CFRetain(0);
            }

            @Test
            @DisplayName("CFGetRetainCount with 0 ref")
            void retainCount0() throws Exception
            {
                AXJLib.INSTANCE.CFGetRetainCount(0);
            }

            @Test
            @DisplayName("CFRelease with 0 ref")
            void release0() throws Exception
            {
                AXJLib.INSTANCE.CFRelease(0);
            }

        }

        @Nested
        @DisplayName("CFStringCreateWithJavaString, StringCreateWithCFStringRef")
        class StringMethods {

            @ParameterizedTest
            @ValueSource(strings = { "expected", "" })
            void stringValue(final String expected) throws Exception
            {
                long ref = 0;
                try {
                    ref = AXJLib.INSTANCE.CFStringCreateWithJavaString(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    String theString = AXJLib.INSTANCE.StringCreateWithCFStringRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected, theString);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @Test
            void nullValue() throws Exception
            {
                long ref = 0;
                try {
                    ref = AXJLib.INSTANCE.CFStringCreateWithJavaString(null);
                    assertEquals(0, ref);

                    String theString = AXJLib.INSTANCE.StringCreateWithCFStringRef(ref);

                    assertEquals(null, theString);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }
        }

        @Nested
        @DisplayName("CFBooleanCreateWithJavaBoolean, BooleanCreateWithCFBoolean")
        class BooleanMethods {

            @ParameterizedTest
            @ValueSource(strings = { "True", "False" })
            void booleanMethods(final String param) throws Exception
            {
                Boolean expectedValue = Boolean.valueOf(param);
                long ref = 0;
                try {
                    ref = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(expectedValue);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    Boolean value = AXJLib.INSTANCE.BooleanCreateWithCFBoolean(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expectedValue,value);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @Test
            void nullValue() throws Exception
            {
                long ref = 0;
                try {
                    ref = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(null);
                    assertEquals(0, ref);

                    Boolean value = AXJLib.INSTANCE.BooleanCreateWithCFBoolean(ref);

                    assertNull(value);
                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

        }

        @Nested
        @DisplayName("CFNumberCreate, NumberCreateWithCFNumberRef")
        class NumberMethods {

            @ParameterizedTest
            @DisplayName("Integer")
            @ValueSource(ints = { Integer.MIN_VALUE, Integer.MIN_VALUE + 1, -1, 0, 1, Integer.MAX_VALUE-1, Integer.MAX_VALUE })
            void intValue(final int value) throws Exception
            {
                long ref = 0;
                try {
                    final Number expected = new Integer(value);
                    ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    Number theNumber = AXJLib.INSTANCE.NumberCreateWithCFNumberRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected.longValue(), theNumber.longValue());

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @DisplayName("Long")
            @ValueSource(longs = { Long.MIN_VALUE, Long.MIN_VALUE + 1, -1, 0, 1, Long.MAX_VALUE-1, Long.MAX_VALUE })
            void longValue(final long value) throws Exception
            {
                long ref = 0;
                try {
                    final Number expected = new Long(value);
                    ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    Number theNumber = AXJLib.INSTANCE.NumberCreateWithCFNumberRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected.intValue(), theNumber.intValue());

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @DisplayName("Byte")
            @ValueSource(bytes = { Byte.MIN_VALUE, Byte.MIN_VALUE + 1, -1, 0, 1, Byte.MAX_VALUE-1, Byte.MAX_VALUE })
            void byteValue(final byte value) throws Exception
            {
                long ref = 0;
                try {
                    final Number expected = new Byte(value);
                    ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    Number theNumber = AXJLib.INSTANCE.NumberCreateWithCFNumberRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected.longValue(), theNumber.longValue());

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @DisplayName("Float")
            @ValueSource(floats = { Float.MIN_VALUE, Float.MIN_VALUE + 0.1f, -0.1f, 0f, 0.1f, Float.MAX_VALUE-.1f, Float.MAX_VALUE })
            void floatValue(final float value) throws Exception
            {
                long ref = 0;
                try {
                    final Number expected = new Float(value);
                    ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    Number theNumber = AXJLib.INSTANCE.NumberCreateWithCFNumberRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected.floatValue(), theNumber.floatValue(),0.00000000001);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @DisplayName("Double")
            @ValueSource(doubles = { Double.MIN_VALUE, Double.MIN_VALUE + 0.1f, -0.1f, 0f, 0.1f, Double.MAX_VALUE-.1f, Double.MAX_VALUE })
            void doubleValue(final double value) throws Exception
            {
                long ref = 0;
                try {
                    final Number expected = new Double(value);
                    ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    Number theNumber = AXJLib.INSTANCE.NumberCreateWithCFNumberRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected.doubleValue(), theNumber.doubleValue(),0.00000000001);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @Test
            void nullValue() throws Exception
            {
                long ref = 0;
                try {
                    ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(null);
                    assertEquals(0, ref);

                    Number theNumber = AXJLib.INSTANCE.NumberCreateWithCFNumberRef(ref);

                    assertEquals(null, theNumber);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }
        }

        @Nested
        @DisplayName("CFURLCreateWithJavaURL, URLCreateWithCFURLRef")
        class URLMethods {

            @Test
            void success() throws Exception
            {
                long ref = 0;
                try {
                    final URL expected = new URL("https://www.qfs.de");
                    ref = AXJLib.INSTANCE.CFURLCreateWithJavaURL(expected);
                    final long refCount = AXJLib.INSTANCE.CFGetRetainCount(ref);
                    assertTrue(refCount > 0);

                    URL theUrl = AXJLib.INSTANCE.URLCreateWithCFURLRef(ref);
                    assertEquals(refCount, AXJLib.INSTANCE.CFGetRetainCount(ref));

                    assertEquals(expected, theUrl);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }

            @Test
            void nullValue() throws Exception
            {
                long ref = 0;
                try {
                    ref = AXJLib.INSTANCE.CFURLCreateWithJavaURL(null);
                    assertEquals(0, ref);

                    URL theURL = AXJLib.INSTANCE.URLCreateWithCFURLRef(ref);

                    assertEquals(null, theURL);

                } finally {
                    AXJLib.INSTANCE.CFRelease(ref);
                }
            }
        }

        @Nested
        @DisplayName("ArrayCopyFromCFArrayRef, CFArrayCreate")
        class ArrayMethods {

            @Test
            @DisplayName("ArrayCopyFromCFArrayRef with 0 reference")
            void nullRef() throws Exception
            {
                final long[] array = AXJLib.INSTANCE.ArrayCopyFromCFArrayRef(0);
                assertEquals(0,array.length);
            }

            @Test
            @DisplayName("CFArrayCreate with null array")
            void nullArray() throws Exception
            {
                final long arrayRef = AXJLib.INSTANCE.CFArrayCreate(null);
                assertEquals(0, arrayRef);
            }

            @Test
            @DisplayName("CFArrayCreate with empty array")
            void emptyArray() throws Exception
            {
                final long arrayRef = AXJLib.INSTANCE.CFArrayCreate(new long[0]);
                assertEquals(0, arrayRef);
            }

            @Test
            @DisplayName("Data in and out")
            void testArrayCreation() throws Exception
            {
                long sRef1 = 0;
                long sRef2 = 0;
                try {
                    long arrayRef = 0;
                    try {
                        sRef1 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Bla 1");
                        sRef2 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Bla 2");

                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(sRef1));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(sRef2));

                        final long[] inArray = new long[] {sRef1, sRef2};

                        arrayRef = AXJLib.INSTANCE.CFArrayCreate(inArray);  // retains members
                        assertNotEquals(0, arrayRef);

                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(arrayRef));
                        assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(sRef1));
                        assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(sRef2));

                        final long[] outArray = AXJLib.INSTANCE.ArrayCopyFromCFArrayRef(arrayRef);
                        try {
                            assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(arrayRef));

                            assertEquals(2, outArray.length);
                            assertEquals(sRef1,outArray[0]);
                            assertEquals(sRef2,outArray[1]);

                            assertEquals(3, AXJLib.INSTANCE.CFGetRetainCount(sRef1));
                            assertEquals(3, AXJLib.INSTANCE.CFGetRetainCount(sRef2));
                        } finally {
                            AXJLib.INSTANCE.CFRelease(outArray[0]);
                            AXJLib.INSTANCE.CFRelease(outArray[1]);
                        }

                    } finally {
                        AXJLib.INSTANCE.CFRelease(arrayRef);
                        arrayRef = 0;

                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(sRef1));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(sRef2));

                    }
                } finally {
                    AXJLib.INSTANCE.CFRelease(sRef1);
                    AXJLib.INSTANCE.CFRelease(sRef2);
                }
            }
        }

        @Nested
        @DisplayName("ArraysCopyFromCFDictionaryRef, CFDictionaryCreate")
        public class DictionaryMethods {

            @Test
            @DisplayName("ArraysCopyFromCFDictionaryRef with 0 reference")
            void nullRef() throws Exception
            {
                final long[][] arrays = AXJLib.INSTANCE.ArraysCopyFromCFDictionaryRef(0);
                assertEquals(2,arrays.length);
                assertEquals(0,arrays[0].length);
                assertEquals(0,arrays[1].length);
            }

            @Test
            @DisplayName("CFDictionaryCreate with null keys and values")
            void nullKeysValues() throws Exception
            {
                final long dictRef = AXJLib.INSTANCE.CFDictionaryCreate(null, null);
                assertEquals(0, dictRef);
            }

            @Test
            @DisplayName("CFDictionaryCreate with empty keys and values")
            void emptyKeysValues() throws Exception
            {
                final long dictRef = AXJLib.INSTANCE.CFDictionaryCreate(new long[0], new long[0]);
                assertEquals(0, dictRef);
            }

            @Test
            @DisplayName("CFDictionaryCreate with null values")
            void emptyValues() throws Exception
            {
                final long dictRef = AXJLib.INSTANCE.CFDictionaryCreate(new long[1], null);
                assertEquals(0, dictRef);
            }

            @Test
            @DisplayName("CFDictionaryCreate with more values than keys")
            void nullKeys() throws Exception
            {
                assertThrows(IllegalArgumentException.class, () -> AXJLib.INSTANCE.CFDictionaryCreate(null, new long[1]));
            }

            @Test
            @DisplayName("Data in and out")
            void testDictCreation() throws Exception
            {
                long keyRef1 = 0;
                long keyRef2 = 0;
                long valRef1 = 0;
                long valRef2 = 0;
                try {
                    long dictRef = 0;
                    try {
                        keyRef1 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Key 1");
                        keyRef2 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Key 2");
                        valRef1 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Value 1");
                        valRef2 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Value 2");

                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(keyRef1));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(keyRef2));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(valRef1));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(valRef2));

                        final long[] keys = new long[] {keyRef1, keyRef2};
                        final long[] values = new long[] {valRef1, valRef2};

                        dictRef = AXJLib.INSTANCE.CFDictionaryCreate(keys, values);  // retains members
                        assertNotEquals(0, dictRef);

                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(dictRef));
                        assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(keyRef1));
                        assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(keyRef2));
                        assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(valRef1));
                        assertEquals(2, AXJLib.INSTANCE.CFGetRetainCount(valRef2));

                        final long[][] outArrays = AXJLib.INSTANCE.ArraysCopyFromCFDictionaryRef(dictRef);

                        try {
                            assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(dictRef));

                            assertEquals(2, outArrays.length);
                            assertEquals(2, outArrays[0].length);
                            assertEquals(2, outArrays[1].length);
                            assertEquals(keyRef1,outArrays[0][0]);
                            assertEquals(keyRef2,outArrays[0][1]);
                            assertEquals(valRef1,outArrays[1][0]);
                            assertEquals(valRef2,outArrays[1][1]);

                            assertEquals(3, AXJLib.INSTANCE.CFGetRetainCount(keyRef1));
                            assertEquals(3, AXJLib.INSTANCE.CFGetRetainCount(keyRef2));
                            assertEquals(3, AXJLib.INSTANCE.CFGetRetainCount(valRef1));
                            assertEquals(3, AXJLib.INSTANCE.CFGetRetainCount(valRef2));
                        } finally {
                            for (int i = 0; i < 2; i++) {
                                for (int j = 0; j < 2; j++) {
                                    if (outArrays.length > i && outArrays[i].length > j && outArrays[i][j] != 0) {
                                        AXJLib.INSTANCE.CFRelease(outArrays[i][j]);
                                    }
                                }
                            }
                        }

                    } finally {
                        AXJLib.INSTANCE.CFRelease(dictRef);
                        dictRef = 0;

                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(keyRef1));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(keyRef2));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(valRef1));
                        assertEquals(1, AXJLib.INSTANCE.CFGetRetainCount(valRef2));

                    }
                } finally {
                    AXJLib.INSTANCE.CFRelease(valRef1);
                    AXJLib.INSTANCE.CFRelease(valRef2);
                    AXJLib.INSTANCE.CFRelease(keyRef1);
                    AXJLib.INSTANCE.CFRelease(keyRef2);
                }
            }
        }
    }

    @Nested
    public class CFCopyDescription {

        @Test
        void nullValue() throws Exception
        {
            long objRef = 0;
            long strRef = AXJLib.INSTANCE.CFCopyDescription(objRef);
            assertEquals(0,strRef);
        }

        @Test
        void CFShow() throws Exception
        {
            long objRef = 0;
            try {
                objRef = createComplexObject();
                long strRef = 0;
                try {
                    strRef = AXJLib.INSTANCE.CFCopyDescription(objRef);

                    assertEquals("(\n" +
                            "    \"Bla 1\",\n" +
                            "    \"Bla 2\"\n" +
                            ")", AXJLib.INSTANCE.StringCreateWithCFStringRef(strRef));
                } finally {
                    AXJLib.INSTANCE.CFRelease(strRef);
                }
            } finally {
                AXJLib.INSTANCE.CFRelease(objRef);
            }
        }
    }

    private long createComplexObject()
    {
        long sRef1 = 0;
        long sRef2 = 0;
        try {
            long arrayRef = 0;
            sRef1 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Bla 1");
            sRef2 = AXJLib.INSTANCE.CFStringCreateWithJavaString("Bla 2");
            final long[] inArray = new long[] {sRef1, sRef2};

            arrayRef = AXJLib.INSTANCE.CFArrayCreate(inArray);  // retains members

            return arrayRef;
        } finally {
            AXJLib.INSTANCE.CFRelease(sRef1);
            AXJLib.INSTANCE.CFRelease(sRef2);
        }
    }

    @Nested
    public class CFGetTypeID {

        @Test
        public void nullValue () throws Exception
        {
            assertEquals(0, AXJLib.INSTANCE.CFGetTypeID(0));
        }

        @Test
        public void CFStringGetTypeID() {
            final long typeId = AXJLib.INSTANCE.CFStringGetTypeID();
            assertNotEquals(0,typeId);
            long strRef = 0;
            try {
                strRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("bla");
                assertNotEquals(0,strRef);
                assertEquals(typeId, AXJLib.INSTANCE.CFGetTypeID(strRef));
            } finally {
                AXJLib.INSTANCE.CFRelease(strRef);
            }
        }

        @Test
        public void CFBooleanGetTypeID() {
            final long typeId = AXJLib.INSTANCE.CFBooleanGetTypeID();
            assertNotEquals(0,typeId);
            long boolRef = 0;
            try {
                boolRef = AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean(true);
                assertNotEquals(0,boolRef);
                assertEquals(typeId, AXJLib.INSTANCE.CFGetTypeID(boolRef));
            } finally {
                AXJLib.INSTANCE.CFRelease(boolRef);
            }
        }

        @Test
        public void CFNumberGetTypeID() {
            final long typeId = AXJLib.INSTANCE.CFNumberGetTypeID();
            assertNotEquals(0,typeId);
            long ref = 0;
            try {
                ref = AXJLib.INSTANCE.CFNumberCreateWithJavaNumber(23);
                assertNotEquals(0,ref);
                assertEquals(typeId, AXJLib.INSTANCE.CFGetTypeID(ref));
            } finally {
                AXJLib.INSTANCE.CFRelease(ref);
            }
        }

        @Test
        public void CFURLGetTypeID() throws Exception {
            final long typeId = AXJLib.INSTANCE.CFURLGetTypeID();
            assertNotEquals(0,typeId);
            long ref = 0;
            try {
                ref = AXJLib.INSTANCE.CFURLCreateWithJavaURL(new URL("http://www.qfs.de"));
                assertNotEquals(0,ref);
                assertEquals(typeId, AXJLib.INSTANCE.CFGetTypeID(ref));
            } finally {
                AXJLib.INSTANCE.CFRelease(ref);
            }
        }

        @Test
        public void CFArrayGetTypeID() {
            final long typeId = AXJLib.INSTANCE.CFArrayGetTypeID();
            assertNotEquals(0,typeId);
            long arrayRef = 0;
            long strRef = 0;
            try {
                strRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("bla");
                arrayRef = AXJLib.INSTANCE.CFArrayCreate(new long[] {strRef});
                assertNotEquals(0,arrayRef);
                assertEquals(typeId, AXJLib.INSTANCE.CFGetTypeID(arrayRef));
            } finally {
                AXJLib.INSTANCE.CFRelease(strRef);
                AXJLib.INSTANCE.CFRelease(arrayRef);
            }
        }

        @Test
        public void CFDictionaryGetTypeID() {
            final long typeId = AXJLib.INSTANCE.CFDictionaryGetTypeID();
            assertNotEquals(0,typeId);
            long dictRef = 0;
            long strRef = 0;
            try {
                strRef = AXJLib.INSTANCE.CFStringCreateWithJavaString("bla");
                dictRef = AXJLib.INSTANCE.CFDictionaryCreate(new long[] {strRef},new long[] {strRef});
                assertNotEquals(0,dictRef);
                assertEquals(typeId, AXJLib.INSTANCE.CFGetTypeID(dictRef));
            } finally {
                AXJLib.INSTANCE.CFRelease(strRef);
                AXJLib.INSTANCE.CFRelease(dictRef);
            }
        }
    }

    @Nested
    class runningApplications {
        @Test
        @DisplayName("returns applications")
        void test() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplications();

            assertNotNull(apps);
            assertTrue(apps.length > 0);
        }

        @Test
        @DisplayName("contains one active application")
        void active() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplications();

            int activeCount = 0;
            for (final NSRunningApplication app: apps) {
                if (app.isActive()) {
                    activeCount++;
                }
            }

            assertEquals(1,activeCount);
        }

        @Test
        @DisplayName("one app owns the menu bar")
        void menuBar() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplications();

            int menuOwnerCount = 0;
            for (final NSRunningApplication app: apps) {
                if (app.ownsMenuBar()) {
                    menuOwnerCount++;
                }
            }

            assertEquals(1,menuOwnerCount);
        }

        @Test
        @DisplayName("Finder is running")
        void checkForFinder() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplications();

            NSRunningApplication finder = null;
            for (NSRunningApplication app: apps) {
                if ("Finder".equals(app.getName())) {
                    finder = app;
                    break;
                }
            }

            assertNotNull(finder);

            final NSRunningApplication app = finder;
            assertAll(() -> assertEquals("com.apple.finder", app.getBundleIdentifier()),
                    () ->assertEquals(new File("/System/Library/CoreServices/Finder.app"), app.getBundleFile()),
                    () ->assertEquals(new File("/System/Library/CoreServices/Finder.app/Contents/MacOS/Finder"), app.getExecutableFile()));
        }

    }

    @Nested
    class runningApplicationWithProcessIdentifier {

        @Test
        @DisplayName("returns Finder")
        void finder() throws Exception
        {
            NSRunningApplication expectedFinder = getFinder();

            int pid = expectedFinder.getProcessIdentifier();

            NSRunningApplication finder = AXJLib.INSTANCE.runningApplicationWithProcessIdentifier(pid);

            assertEquals(expectedFinder, finder);
            assertNotSame(expectedFinder, finder);
        }

        protected NSRunningApplication getFinder()
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplications();

            NSRunningApplication finder = null;
            for (NSRunningApplication app: apps) {
                if ("Finder".equals(app.getName())) {
                    finder = app;
                    break;
                }
            }
            return finder;
        }

        @Test
        @DisplayName("returns null on invalid pid")
        void nullOnInvalid() throws Exception
        {
            NSRunningApplication app = AXJLib.INSTANCE.runningApplicationWithProcessIdentifier(0);

            assertNull(app);
        }
    }

    @Nested
    class runningApplicationsWithBundleIdentifier {
        @Test
        @DisplayName("gets the Finder")
        void finder() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplicationsWithBundleIdentifier("com.apple.finder");

            assertEquals(1,apps.length);
            assertEquals("Finder",apps[0].getName());
        }

        @Test
        @DisplayName("returns empty array on null bundle identifier")
        void nullInput() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplicationsWithBundleIdentifier(null);

            assertEquals(0,apps.length);
        }

        @Test
        @DisplayName("returns empty array on empty bundle identifier")
        void emptyInput() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplicationsWithBundleIdentifier("");

            assertEquals(0,apps.length);
        }

        @Test
        @DisplayName("returns empty array on null bundle identifier")
        void invalidInput() throws Exception
        {
            final NSRunningApplication[] apps = AXJLib.INSTANCE.runningApplicationsWithBundleIdentifier("bla.bla.bla");

            assertEquals(0,apps.length);
        }
    }

    @Nested
    class currentApplication {
        @Test
        @DisplayName("is not null")
        void notNull() throws Exception
        {
            NSRunningApplication app = AXJLib.INSTANCE.currentApplication();
            assertNotNull(app);
        }
    }

    @Nested
    class NSSearchPathForDirectoriesInDomains {
        @Test
        @DisplayName("Returns /Applications dir")
        void localApplications() throws Exception
        {
            long cfPathsRef = AXJLib.INSTANCE.NSSearchPathForDirectoriesInDomains(1, 2, true);
            Object convertedArray = TypeConverter.instance().convertFromCFTypeRef(cfPathsRef);
            assertTrue(convertedArray instanceof List);
            assertEquals("/Applications",((List<?>)convertedArray).get(0));
            AXJLib.INSTANCE.CFRelease(cfPathsRef);
        }

        @Test
        @DisplayName("Returns ~/Applications dir")
        void sharedApplications() throws Exception
        {
            long cfPathsRef = AXJLib.INSTANCE.NSSearchPathForDirectoriesInDomains(1, 1, false);
            Object convertedArray = TypeConverter.instance().convertFromCFTypeRef(cfPathsRef);
            assertTrue(convertedArray instanceof List);
            assertEquals("~/Applications",((List<?>)convertedArray).get(0));
            AXJLib.INSTANCE.CFRelease(cfPathsRef);
        }

        @Test
        @DisplayName("Returns and resolves ~/Applications dir")
        void sharedApplicationsResolved() throws Exception
        {
            long cfPathsRef = AXJLib.INSTANCE.NSSearchPathForDirectoriesInDomains(1, 1, true);
            Object convertedArray = TypeConverter.instance().convertFromCFTypeRef(cfPathsRef);
            assertTrue(convertedArray instanceof List);
            assertEquals(System.getProperty("user.home") + "/Applications",((List<?>)convertedArray).get(0));
            AXJLib.INSTANCE.CFRelease(cfPathsRef);
        }
    }


}
