/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.appkit;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import java.net.URL;

import java.util.Date;

import org.junit.jupiter.api.*;

import de.qfs.lib.axjlib.appkit.NSRunningApplication.ExecutableArchitecture;

public class NSRunningApplicationTest
{
    @Nested
    class Constructor {
        @Test
        @DisplayName("properly initializes")
        void properInit() throws Exception
        {
            NSRunningApplication app = new NSRunningApplication(
                    true,
                    false,
                    "Bla Bla",
                    "de.test.bla",
                    "file://localhost/Applications/Bli%20Bli.app",
                    0x01000007,
                    "file://localhost/Applications/Bli%20Bli.app/Contents/MacOS/schnitzel",
                    1000000,
                    true,
                    123,
                    true,
                    false);

            assertAll(() -> assertTrue(app.isActive()),
                    () -> assertFalse(app.isHidden()),
                    () -> assertEquals("Bla Bla",app.getLocalizedName()),
                    () -> assertEquals("de.test.bla",app.getBundleIdentifier()),
                    () -> assertEquals(new URL("file://localhost/Applications/Bli%20Bli.app"), app.getBundleURL()),
                    () -> assertEquals(new File("/Applications/Bli Bli.app"), app.getBundleFile()),
                    () -> assertEquals("Bli Bli", app.getName()),
                    () -> assertEquals(ExecutableArchitecture.NSBundleExecutableArchitectureX86_64, app.getExecutableArchitecture()),
                    () -> assertEquals(new URL("file://localhost/Applications/Bli%20Bli.app/Contents/MacOS/schnitzel"), app.getExecutableURL()),
                    () -> assertEquals(new File("/Applications/Bli Bli.app/Contents/MacOS/schnitzel"), app.getExecutableFile()),
                    () -> assertEquals(new Date(1000000), app.getLaunchDate()),
                    () -> assertEquals(true, app.isFinishedLaunching()),
                    () -> assertEquals(123, app.getProcessIdentifier()),
                    () -> assertEquals(true, app.ownsMenuBar()),
                    () -> assertEquals(false, app.isTerminated()));
        }

    }
}
