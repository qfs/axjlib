/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.qfs.lib.axjlib.AXError;
import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTest;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AXUIElementHelper;

@ExtendWith(MockedAXJLib.class)
public class AXApplicationTest
{
    long ref = 12312345L;

    AXApplication element;

    @BeforeEach
    void initElement() {
        element = new AXApplication(ref);
    }

    @Nested
    class elementAtPosition {

        @Test
        @DisplayName("returns the element on success")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long objRef = 12874L;
            MockedAXJLib.mockAXUIElement(axjlib, objRef, MockedString.AXMenuBar);

            when(axjlib.AXUIElementCopyElementAtPosition(eq(ref), anyFloat(), anyFloat(), any(long[].class))).thenAnswer(
                    MockedAXJLib.answerSuccessWithRefReturn(objRef));


            AXUIElement result = element.elementAtPosition(10,10);

            assertEquals(objRef,AXUIElementHelper.getReference(result));
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyElementAtPosition(eq(ref), anyFloat(), anyFloat(), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.elementAtPosition(10,10));
            assertEquals(error,ex.getError());
        }
    }
}
