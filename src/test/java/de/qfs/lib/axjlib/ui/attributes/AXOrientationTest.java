/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class AXOrientationTest
{
    @Spy AXOrientation orientation;

    @BeforeEach
    void setupSut() {
        MockitoAnnotations.initMocks(this);
    }

    @Nested
    class hasHorizontalOrientation {
        @Test
        @DisplayName("returns true when horizontal")
        void trueCase() throws Exception
        {
            when(orientation.getAttributeValueOrNull("AXOrientation")).thenReturn("AXHorizontalOrientation");
            assertTrue(orientation.hasHorizontalOrientation());
        }

        @Test
        @DisplayName("returns false when not horizontal")
        void falseCase() throws Exception
        {
            when(orientation.getAttributeValueOrNull("AXOrientation")).thenReturn("AXVerticalOrientation");
            assertFalse(orientation.hasHorizontalOrientation());
        }
    }

    @Nested
    class hasVerticalOrientation {
        @Test
        @DisplayName("returns true when vertical")
        void trueCase() throws Exception
        {
            when(orientation.getAttributeValueOrNull("AXOrientation")).thenReturn("AXVerticalOrientation");
            assertTrue(orientation.hasVerticalOrientation());
        }

        @Test
        @DisplayName("returns false when not vertical")
        void falseCase() throws Exception
        {
            when(orientation.getAttributeValueOrNull("AXOrientation")).thenReturn("AXUnknownOrientation");
            assertFalse(orientation.hasVerticalOrientation());
        }
    }

    @Nested
    class hasUnkownOrientation {
        @Test
        @DisplayName("returns true when unknown")
        void trueCase() throws Exception
        {
            when(orientation.getAttributeValueOrNull("AXOrientation")).thenReturn("AXUnknownOrientation");
            assertTrue(orientation.hasUnkownOrientation());
        }

        @Test
        @DisplayName("returns false when not horizontal")
        void falseCase() throws Exception
        {
            when(orientation.getAttributeValueOrNull("AXOrientation")).thenReturn("AXVerticalOrientation");
            assertFalse(orientation.hasUnkownOrientation());
        }
    }
}
