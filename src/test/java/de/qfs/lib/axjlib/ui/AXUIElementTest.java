/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.EnumSource.Mode;
import org.junit.jupiter.params.provider.ValueSource;

import de.qfs.lib.axjlib.AXError;
import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTest;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTestWithFalseUponMissingValue;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTestWithNullReturnUponMissingAttribute;
import de.qfs.lib.axjlib.MockedAXJLib.ParameterizedAXErrorTestWithoutMissingAttribute;

@ExtendWith(MockedAXJLib.class)
public class AXUIElementTest
{

    long ref = 12312345L;

    AXUIElement element;

    @BeforeEach
    void initElement() {
        element = new AXUIElement(ref);
    }

    @Nested
    class referenceIsAXUIElement {

        @Test
        @DisplayName("when it is a AXUIElement")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long objRef = 1212127L;
            MockedAXJLib.mockAXUIElement(axjlib, objRef, MockedString.AXMenuBar);
            assertTrue(AXUIElement.referenceIsAXUIElement(objRef));
        }

        @Test
        @DisplayName("when it is no AXUIElement")
        void failureCase(final AXJLib axjlib) throws Exception
        {
            long objRef = 1212127L;
            MockedAXJLib.mockString(axjlib, objRef, "bla");
            assertFalse(AXUIElement.referenceIsAXUIElement(objRef));
        }
    }

    @Test
    @DisplayName("Constructor fails with 0 reference")
    void no0Reference() throws Exception
    {
        assertThrows(IllegalArgumentException.class, () -> new AXUIElement(0));
    }

    @Test
    @DisplayName("Retains CF object on construction")
    void retains(final AXJLib axjlib) throws Exception
    {
        verify(axjlib).CFRetain(ref);
    }

    @Test
    @DisplayName("releases CF object on finalize")
    void relases(final AXJLib axjlib) throws Exception
    {
        element = null;
        // try to force finalize
        for (int i = 0; i < 10; i++) {
            Thread.sleep(10);
            System.gc();
        }

        verify(axjlib).CFRelease(ref);
    }

    @Test
    @DisplayName("toString contains reference")
    void referenceInToString() throws Exception
    {
        final String hexRef = "0x" + Long.toHexString(ref);
        assertTrue(element.toString().contains(hexRef));
    }

    @Nested
    class getObjectDescription {

        @Test
        @DisplayName("gets the object description")
        void getsDescription(final AXJLib axjlib) throws Exception
        {
            when(axjlib.CFCopyDescription(ref)).thenReturn(MockedString.Unknown.getCfRef());

            assertEquals("Unknown",element.getObjectDescription());
        }

    }

    @Nested
    class getPid {

        @Test
        @DisplayName("returns the pid of the element")
        void returnsPid(final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementGetPid(eq(ref), any(int[].class))).thenAnswer(
                    MockedAXJLib.answerSuccessWithRefReturn(4711));

            int pid = element.getPid();
            assertEquals(4711, pid);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementGetPid(eq(ref), any(int[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getPid());
            assertEquals(error,ex.getError());
        }

    }

    @Nested
    class getAttributeNames {

        @Test
        @DisplayName("returns list of names")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long arrayRef = 815815L;
            MockedAXJLib.mockArray(axjlib, arrayRef, new long[] {MockedString.AXRole.getCfRef(), MockedString.AXSubrole.getCfRef()});
            when(axjlib.AXUIElementCopyAttributeNames(eq(ref), any(long[].class))).thenAnswer(
                    MockedAXJLib.answerSuccessWithRefReturn(arrayRef));

            final List<String> names = element.getAttributeNames();

            assertEquals(Arrays.asList("AXRole","AXSubrole"), names);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyAttributeNames(eq(ref), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getAttributeNames());
            assertEquals(error,ex.getError());
        }

    }

    @Nested
    class getAttributeValue {

        @Test
        @DisplayName("returns the attribute value")
        void successCase(final AXJLib axjlib) throws Exception
        {
            MockedAXJLib.mockAttributeValue(axjlib, ref, MockedString.AXRole, MockedString.AXWindow.getCfRef());

            Object value = element.getAttributeValue("AXRole");

            assertEquals("AXWindow", value);
        }


        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyAttributeValue(eq(ref), any(long.class), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getAttributeValue("AXSubRole"));
            assertEquals(error,ex.getError());
        }

    }

    @Nested
    class getAttributeValueOrNull {

        @Test
        @DisplayName("returns the attribute value, if available")
        void successCase(final AXJLib axjlib) throws Exception
        {
            MockedAXJLib.mockAttributeValue(axjlib, ref, MockedString.AXRole, MockedString.AXWindow.getCfRef());

            Object value = element.getAttributeValueOrNull("AXRole");

            assertEquals("AXWindow", value);
        }

        @ParameterizedAXErrorTestWithNullReturnUponMissingAttribute
        void nullCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyAttributeValue(eq(ref), any(long.class), any(long[].class))).thenReturn(error.getIntValue());

            Object value = element.getAttributeValueOrNull("AXRole");

            assertNull(value);
        }

        @ParameterizedAXErrorTestWithoutMissingAttribute
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyAttributeValue(eq(ref), any(long.class), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getAttributeValue("AXSubRole"));
            assertEquals(error,ex.getError());
        }

    }

    @Nested
    class isAttributeSettable {
        @ParameterizedTest
        @DisplayName("works for value")
        @ValueSource(strings={"true", "false"})
        void successCase(final String valueString, final AXJLib axjlib) throws Exception
        {
            boolean value = Boolean.valueOf(valueString);
            when(axjlib.AXUIElementIsAttributeSettable(eq(ref), anyLong(), any(boolean[].class))).then(
                    MockedAXJLib.answerSuccessWithRefReturn(value)
            );

            assertEquals(value,element.isAttributeSettable("AXRole"));
        }

        @ParameterizedAXErrorTestWithFalseUponMissingValue
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementIsAttributeSettable(eq(ref), anyLong(), any(boolean[].class))).thenReturn(error.getIntValue());

            boolean value = element.isAttributeSettable("AXSubRole");
            assertFalse(value);
        }
    }

    @Nested
    class setAttribute {

        @Test
        @DisplayName("sets the attribute")
        void successCase(final AXJLib axjlib) throws Exception
        {
            element.setAttribute("AXRole", "AXWindow");
            verify(axjlib).AXUIElementSetAttributeValue(ref,MockedString.AXRole.getCfRef(), MockedString.AXWindow.getCfRef());
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementSetAttributeValue(eq(ref), any(long.class), any(long.class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.setAttribute("AXRole", "AXWindow"));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class safeSetAttribute {

        @Test
        @DisplayName("sets the attribute")
        void successCase(final AXJLib axjlib) throws Exception
        {
            boolean success = element.safeSetAttribute("AXRole", "AXWindow");
            assertTrue(success);
            verify(axjlib).AXUIElementSetAttributeValue(ref,MockedString.AXRole.getCfRef(), MockedString.AXWindow.getCfRef());
        }


        @ParameterizedAXErrorTestWithFalseUponMissingValue
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementSetAttributeValue(eq(ref), any(long.class), any(long.class))).thenReturn(error.getIntValue());

            boolean success = element.safeSetAttribute("AXRole", "AXWindow");
            assertFalse(success);

        }
    }

    @Nested
    class getParameterizedAttributeNames {
        @Test
        @DisplayName("returns list of names")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long arrayRef = 815815L;
            MockedAXJLib.mockArray(axjlib, arrayRef, new long[] {MockedString.AXRole.getCfRef(), MockedString.AXSubrole.getCfRef()});
            when(axjlib.AXUIElementCopyParameterizedAttributeNames(eq(ref), any(long[].class))).thenAnswer(
                    MockedAXJLib.answerSuccessWithRefReturn(arrayRef));

            final List<String> names = element.getParameterizedAttributeNames();

            assertEquals(Arrays.asList("AXRole","AXSubrole"), names);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyParameterizedAttributeNames(eq(ref), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getParameterizedAttributeNames());
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class getActionNames {

        @Test
        @DisplayName("returns list of names")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long arrayRef = 815815L;
            MockedAXJLib.mockArray(axjlib, arrayRef, new long[] {MockedString.AXRaise.getCfRef(), MockedString.AXCancel.getCfRef()});
            when(axjlib.AXUIElementCopyActionNames(eq(ref), any(long[].class))).thenAnswer(
                    MockedAXJLib.answerSuccessWithRefReturn(arrayRef));

            final List<String> names = element.getActionNames();

            assertEquals(Arrays.asList("AXRaise","AXCancel"), names);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyActionNames(eq(ref), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getActionNames());
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class getActionDescription {

        @Test
        @DisplayName("returns the action Description")
        void successCase(final AXJLib axjlib) throws Exception
        {
            long descRef = 12874L;
            final String description = "A suitable description";

            MockedAXJLib.mockString(axjlib, descRef, description);
            when(axjlib.AXUIElementCopyActionDescription(eq(ref), eq(MockedString.AXRaise.getCfRef()), any(long[].class))).thenAnswer(
                    MockedAXJLib.answerSuccessWithRefReturn(descRef));


            String value = element.getActionDescription("AXRaise");

            assertEquals(description, value);
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementCopyActionDescription(eq(ref), any(long.class), any(long[].class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.getActionDescription("AXRaise"));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class performAction {
        @Test
        @DisplayName("performs the action")
        void successCase(final AXJLib axjlib) throws Exception
        {
            element.performAction("AXRaise");
            verify(axjlib).AXUIElementPerformAction(ref,MockedString.AXRaise.getCfRef());
        }

        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementPerformAction(eq(ref), any(long.class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.performAction("AXRole"));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class safePerformAction {

        @Test
        @DisplayName("performs the action")
        void successCase(final AXJLib axjlib) throws Exception
        {
            element.safePerformAction("AXRaise");
            verify(axjlib).AXUIElementPerformAction(ref,MockedString.AXRaise.getCfRef());
        }

        @Test
        @DisplayName("ignores kAXErrorCannotComplete")
        void ignoreCase(final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementPerformAction(eq(ref), any(long.class))).thenReturn(AXError.kAXErrorCannotComplete.getIntValue());

            element.safePerformAction("AXRaise");
        }

        @ParameterizedTest(name="upon {0} error")
        @DisplayName("throws Exception")
        @EnumSource(value=AXError.class, names= {"kAXErrorSuccess","kAXErrorCannotComplete"}, mode=Mode.EXCLUDE)
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementPerformAction(eq(ref), any(long.class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.safePerformAction("AXRole"));
            assertEquals(error,ex.getError());
        }
    }

    @Nested
    class setMessagingTimeout {

        @Test
        @DisplayName("sets the attribute in seconds")
        void successCase(final AXJLib axjlib) throws Exception
        {
            element.setMessagingTimeout(1560);
            verify(axjlib).AXUIElementSetMessagingTimeout(ref,1.56f);
        }


        @ParameterizedAXErrorTest
        void errorCase(final AXError error, final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXUIElementSetMessagingTimeout(eq(ref), any(float.class))).thenReturn(error.getIntValue());

            AXException ex = assertThrows(AXException.class, () -> element.setMessagingTimeout(0));
            assertEquals(error,ex.getError());
        }
    }

}
