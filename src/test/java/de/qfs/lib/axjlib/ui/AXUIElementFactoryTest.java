/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;

@ExtendWith(MockedAXJLib.class)
public class AXUIElementFactoryTest
{

    @Nested
    class getElement {
        @ParameterizedTest
        @DisplayName("works for")
        @MethodSource("de.qfs.lib.axjlib.ui.AXUIElementHelper#getElementClasses")
        void worksFor(final Class<?> elementClass, final AXJLib axjlib){

            final long ref = 1234567;
            final String role = AXUIElementHelper.getRole(elementClass);
            final String subrole = AXUIElementHelper.getSubrole(elementClass);

            MockedAXJLib.mockAXUIElement(axjlib, ref, MockedString.valueOfOrNull(role), MockedString.valueOfOrNull(subrole));

            final AXUIElement element = AXUIElementFactory.instance().getElement(ref);

            assertTrue(elementClass.isInstance(element));
        }

        @Test
        @DisplayName("returns general AXUIElement for unkown types")
        void unkown(final AXJLib axjlib) throws Exception
        {
            final long ref = 11112222;

            MockedAXJLib.mockAXUIElement(axjlib, ref, MockedString.Unknown);

            final AXUIElement element = AXUIElementFactory.instance().getElement(ref);

            assertTrue(element instanceof AXUIElement);
        }


    }
}
