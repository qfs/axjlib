/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import java.lang.reflect.Method;

import java.net.URI;
import java.net.URISyntaxException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.ui.AXUIElementHelper;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

@ExtendWith(MockedAXJLib.class)
public class AttributesTest
{
    @ParameterizedTest
    @MethodSource("getAttributeClassFiles")
    @DisplayName("getter/setter call AttributeAccessor with the correct attribute name")
    void getAndSet(final String fileName, final AXJLib axjlib) throws Exception {
        final String attributeName = fileName.substring(0, fileName.indexOf('.'));
        final Class<?> attributeClass = Class.forName(this.getClass().getPackage().getName() + "." + attributeName);

        final AttributeAccessor mock = (AttributeAccessor) Mockito.mock(attributeClass);

        final Method[] attributeMethods = attributeClass.getDeclaredMethods();

        boolean hasSetter = false;

        Method getterMethod = null;
        Method setterMethod = null;
        for (final Method method: attributeMethods) {
            final String methodName = method.getName();
            final String simpleAttributeName = attributeName.substring(2);
            if (methodName.equals("get" + simpleAttributeName)
                    || methodName.equals("is" + simpleAttributeName)
                    || methodName.equals("has" + simpleAttributeName)
                    || methodName.equals("does" + simpleAttributeName)) {
                getterMethod = method;
            }
            if (methodName.equals("set" + simpleAttributeName)) {
                hasSetter = true;
                setterMethod = method;
            }
        }
        assertNotNull(getterMethod);
        if (hasSetter) {
            assertNotNull(setterMethod);
        }

        final List<Executable> verifiers = new LinkedList<>();

        getterMethod.invoke(mock); // call mock once to make next when working
        when(null).thenCallRealMethod(); // This should not be mocked

        // now call it again to test
        getterMethod.invoke(mock);

        verifiers.add(() -> verify(mock).getAttributeValueOrNull(attributeName));

        if (hasSetter) {
            assertEquals(1, setterMethod.getParameterCount());

            Class<?> getterResultType = getterMethod.getReturnType();
            Class<?> setterAttributeType = setterMethod.getParameterTypes()[0];

            assertEquals(getterResultType, setterAttributeType);

            final Object mockedParameter = mockType(setterAttributeType);

            setterMethod.invoke(mock, mockedParameter); // call mock once to make next when working
            when(null).thenCallRealMethod(); // This should not be mocked

            // now call it again to test
            setterMethod.invoke(mock, mockedParameter);

            verifiers.add(() -> verify(mock).safeSetAttribute(attributeName, mockedParameter));
        }

        assertAll(verifiers);
    }

    public Object mockType(Class<?> type)
    {
        // final classes cannot be mocked by mockito, so we provide some dummy here
        if (type == Boolean.class) return Boolean.TRUE;
        if (type == String.class) return "Mocked String";
        if (type == Long.class) return new Long(1234512345);
        if (type == Integer.class) return new Integer(1234512);

        return Mockito.mock(type);
    }

    public static Stream<String> getAttributeClassFiles() throws URISyntaxException, IOException
    {
        final String pkg = AttributesTest.class.getPackage().getName();
        final URI codeSource = AXUIElementHelper.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        final Path elementsDir = Paths.get(codeSource).getParent().resolve("main/" + pkg.replace('.', '/'));
        return Files.list(elementsDir).filter((path)->! path.toString().contains("$")).map((path) -> path.getFileName().toString());
    }
}
