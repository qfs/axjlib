/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXSections.AXSection;

public class AXSectionsTest
{
    @Nested
    @DisplayName("AXSection")
    class AXSectionTest {

        AXSection section;
        AXSection emptySection = new AXSection(Collections.emptyMap());
        @Mock AXUIElement object;

        @BeforeEach
        void initSection() {
            MockitoAnnotations.initMocks(this);
            Map<String, Object> map = new HashMap<>();
            map.put("SectionDescription","content");
            map.put("SectionObject", object);
            map.put("SectionUniqueID","AXContent");

            section = new AXSection(map);
        }

        @Nested
        class Constructor {
            @Test
            @DisplayName("constructs with data")
            void data() throws Exception
            {
                // already constructed in @BeforeEach...
                assertAll(
                        () -> assertEquals("content",section.get("SectionDescription")),
                        () -> assertEquals(object,section.get("SectionObject")),
                        () -> assertEquals("AXContent",section.get("SectionUniqueID"))
                );
            }

            @Test
            @DisplayName("constructs empty with null")
            void withNull() throws Exception
            {
                section = new AXSection(null);
            }
        }

        @Nested
        @DisplayName("getDescription")
        class getDescription {
            @Test
            @DisplayName("returns String when available")
            void available() throws Exception
            {
                assertEquals("content",section.getDescription());
            }

            @Test
            @DisplayName("returns null when not available")
            void nullCase() throws Exception
            {
                assertNull(emptySection.getDescription());
            }
        }

        @Nested
        @DisplayName("getObject")
        class getObject {
            @Test
            @DisplayName("returns AXUIElement when available")
            void available() throws Exception
            {
                assertEquals(object,section.getObject());
            }

            @Test
            @DisplayName("returns null when not available")
            void nullCase() throws Exception
            {
                assertNull(emptySection.getObject());
            }
        }

        @Nested
        @DisplayName("getUniqueID")
        class getUniqueID {
            @Test
            @DisplayName("returns String when available")
            void available() throws Exception
            {
                assertEquals("AXContent",section.getUniqueID());
            }

            @Test
            @DisplayName("returns null when not available")
            void nullCase() throws Exception
            {
                assertNull(emptySection.getUniqueID());
            }
        }

    }
}
