/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParser;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.CsvParserSettings;

import de.qfs.lib.axjlib.StringListConverter.StringList;

public class AXUIElementImplementationTest
{

    private static final String DESCRIPTION_FILE = "AXUIElements.csv";

    @ParameterizedTest(name="{0}-{1}")
    @CsvFileSource(resources=DESCRIPTION_FILE)
    void checkIntegrity(final String role, final String subrole, @StringList final List<String> attributes, @StringList final List<String> actions) throws Exception {

        final String basePackage = this.getClass().getPackage().getName();
        final String fullClassName = getFullClassName(role, subrole);

        final Class<?> elementClass = Class.forName(fullClassName);

        final List<Executable> assertions = new LinkedList<>();
        assertions.add(() -> assertEquals(role, AXUIElementHelper.getRole(elementClass)));
        assertions.add(() -> assertEquals(subrole, AXUIElementHelper.getSubrole(elementClass)));
        assertions.addAll(assertInterfaces(basePackage, elementClass, attributes, "attributes"));
        assertions.addAll(assertInterfaces(basePackage, elementClass, actions, "actions"));
        assertAll(assertions);

    }

    private String getFullClassName(final String role, final String subrole)
    {
        final String basePackage = this.getClass().getPackage().getName();
        final String className = (subrole != null) ? subrole : role;
        final String fullClassName = "AXUIElement".equals(className) ? basePackage + "." + className : basePackage + ".elements." + className;
        return fullClassName;
    }

    private List<Executable> assertInterfaces(final String basePackage, final Class<?> elementClass, final List<String> interfaces,
                                 final String subPkg)
    {
        final List<Executable> assertions = new LinkedList<>();

        if (interfaces != null) {
            for (String interfc: interfaces) {
                if ("".equals(interfc)) continue;

                final boolean isSettable = interfc.endsWith("!");
                if (isSettable) interfc = interfc.substring(0,interfc.length()-1);

                final String fullInterfaceName = basePackage + "."+subPkg+"." + interfc;
                assertions.add(
                 () -> { try {
                        final Class<?> attributeClass = Class.forName(fullInterfaceName);
                        assertTrue(attributeClass.isAssignableFrom(elementClass), elementClass.getName() + " does not implement " + fullInterfaceName);
                    } catch (ClassNotFoundException ex) {
                        fail(fullInterfaceName + " does not exists");
                    }
                });
            }
        }
        return assertions;
    }

    @ParameterizedTest
    @MethodSource("de.qfs.lib.axjlib.ui.AXUIElementHelper#getElementClasses")
    @DisplayName("A description exists for")
    void doubleCheck(final Class<?> elementClass) throws Exception
    {
        final String elementClassName = elementClass.getName();

        final CsvParser csvParser = new CsvParser(new CsvParserSettings());
        csvParser.beginParsing(this.getClass().getResourceAsStream(DESCRIPTION_FILE), "UTF-8");
        final List<String[]> content = csvParser.parseAll();
        for (String[] line: content) {
            if (elementClassName.equals(getFullClassName(line[0], line[1]))) {
                // fine :)
                return;
            }
        }
        fail("No description found in " + DESCRIPTION_FILE + " for " + elementClassName);
    }


}
