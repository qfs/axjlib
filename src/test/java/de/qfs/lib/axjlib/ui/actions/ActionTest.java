/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.actions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import java.lang.reflect.Method;

import java.net.URI;
import java.net.URISyntaxException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.MockedAXJLib;
import de.qfs.lib.axjlib.ui.AXUIElementHelper;
import de.qfs.lib.axjlib.ui.ActionPerformer;

@ExtendWith(MockedAXJLib.class)
public class ActionTest
{
    @ParameterizedTest
    @MethodSource("getActionClassFiles")
    @DisplayName("Perform method calls safePerformAction with the correct action name")
    void perform(final String fileName, final AXJLib axjlib) throws Exception {
        final String actionName = fileName.substring(0, fileName.indexOf('.'));

        final Class<?> actionClass = Class.forName(this.getClass().getPackage().getName() + "." + actionName);

        final ActionPerformer mock = (ActionPerformer) Mockito.mock(actionClass);

        final Method[] actionMethods = actionClass.getDeclaredMethods();

        // Action interfaces should have exactly one declared method
        assertEquals(1,actionMethods.length);

        final Method actionMethod = actionMethods[0];

        actionMethod.invoke(mock); // call mock once to make next when working
        when(null).thenCallRealMethod(); // This should not be mocked

        // now call it again
        actionMethod.invoke(mock);

        verify(mock).safePerformAction(actionName);

    }

    public static Stream<String> getActionClassFiles() throws URISyntaxException, IOException
    {
        final String pkg = ActionTest.class.getPackage().getName();
        final URI codeSource = AXUIElementHelper.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        final Path elementsDir = Paths.get(codeSource).getParent().resolve("main/" + pkg.replace('.', '/'));
        return Files.list(elementsDir).map((path) -> path.getFileName().toString());
    }
}
