/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import de.qfs.lib.axjlib.ui.attributes.AXMenuItemCmdModifiers.KeyModifier;


public class AXMenuItemCmdModifiersTest
{
    @Nested
    @DisplayName("KeyModifier")
    class KeyModifierClass
    {
        @ParameterizedTest(name="for mask {0}")
        @MethodSource("de.qfs.lib.axjlib.ui.attributes.AXMenuItemCmdModifiersTest#maskProvider")
        void getEnumSet(final Long mask)
        {
            final EnumSet<KeyModifier> enumSet = KeyModifier.getEnumSet(mask);

            assertNotNull(enumSet);
            int sum = 0;
            for (KeyModifier modifier: enumSet) {
                sum += 1 << modifier.ordinal();
            }
            assertEquals((long)mask, sum);
        }

    }

    static List<Long> maskProvider() {
        final List<Long> masks = new LinkedList<>();
        for (long i = 0;  i < (1 << KeyModifier.values().length); i++) {
            masks.add(i);
        }
        return masks;
    }

    @Nested
    class getMenuItemCmdModifiers {

        @Spy AXMenuItemCmdModifiers attribute;

        @BeforeEach
        void initMocks() {
            MockitoAnnotations.initMocks(this);
        }

        @Test
        @DisplayName("returns correct enum set")
        void success() throws Exception
        {
            when(attribute.getAttributeValueOrNull("AXMenuItemCmdModifiers")).thenReturn(6L);

            final EnumSet<KeyModifier> set = attribute.getMenuItemCmdModifiers();

            assertEquals(EnumSet.of(KeyModifier.OPTION, KeyModifier.CONTROL), set);
        }

        @Test
        @DisplayName("returns empty enum set on 0")
        void success0() throws Exception
        {
            when(attribute.getAttributeValueOrNull("AXMenuItemCmdModifiers")).thenReturn(0L);

            final EnumSet<KeyModifier> set = attribute.getMenuItemCmdModifiers();

            assertEquals(EnumSet.noneOf(KeyModifier.class), set);
        }

        @Test
        @DisplayName("returns null on null")
        void testName() throws Exception
        {
            when(attribute.getAttributeValueOrNull("AXMenuItemCmdModifiers")).thenReturn(null);

            final EnumSet<KeyModifier> set = attribute.getMenuItemCmdModifiers();

            assertNull(set);

        }

    }
}
