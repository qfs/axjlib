/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static de.qfs.lib.axjlib.MockedAXJLib.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.qfs.lib.axjlib.MockedAXJLib.MockedBoolean;
import de.qfs.lib.axjlib.MockedAXJLib.MockedConstant;
import de.qfs.lib.axjlib.MockedAXJLib.MockedString;
import de.qfs.lib.axjlib.ui.AXUIElementHelper;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.elements.AXApplication;
import de.qfs.lib.axjlib.ui.elements.AXSystemWide;

@ExtendWith(MockedAXJLib.class)
public class AXAPITest
{
    @Nested
    @DisplayName("isProcessTrusted")
    public class IsProcessTrustedMethod {

        @Test
        @DisplayName("Returns true when the API returns true")
        public void trusted(final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXIsProcessTrustedWithOptions(anyLong())).thenReturn(true);
            assertTrue(AXAPI.isProcessTrusted(false));
        }

        @Test
        @DisplayName("Returns false when the API returns false")
        public void notTrusted(final AXJLib axjlib) throws Exception
        {
            when(axjlib.AXIsProcessTrustedWithOptions(anyLong())).thenReturn(false);
            assertFalse(AXAPI.isProcessTrusted(false));
        }

        @Nested
        @DisplayName("Properly sets up the options")
        public class InformUser {

            @BeforeEach
            public void setupMock(final AXJLib axjlib) {
                when(axjlib.CFStringCreateWithJavaString(AXAPI.kAXTrustedCheckOptionPrompt)).thenReturn(MockedConstant.kAXTrustedCheckOptionPrompt.getCfRef());
                when(axjlib.CFDictionaryCreate(aryEq(new long[] {MockedConstant.kAXTrustedCheckOptionPrompt.getCfRef()}),
                                               aryEq(new long[] {MockedBoolean.True.getCfRef()}))).thenReturn(4711L);
                when(axjlib.CFDictionaryCreate(aryEq(new long[] {MockedConstant.kAXTrustedCheckOptionPrompt.getCfRef()}),
                                               aryEq(new long[] {MockedBoolean.False.getCfRef()}))).thenReturn(4712L);

                when(axjlib.AXIsProcessTrustedWithOptions(4711L)).thenReturn(false);
                when(axjlib.AXIsProcessTrustedWithOptions(4712L)).thenReturn(false);
            }

            @Test
            @DisplayName("to inform user")
            public void informUser(final AXJLib axjlib) throws Exception
            {
                assertFalse(AXAPI.isProcessTrusted(true));
                verify(axjlib).AXIsProcessTrustedWithOptions(4711L);
            }

            @Test
            @DisplayName("not to inform user")
            public void notInformUser(final AXJLib axjlib) throws Exception
            {
                assertFalse(AXAPI.isProcessTrusted(false));
                verify(axjlib).AXIsProcessTrustedWithOptions(4712L);
            }
        }

    }

    @Nested
    @DisplayName("getSystemWideAccessibilityObject")
    public class GetSystemWideAccessibilityObjectMethod {

        @Test
        @DisplayName("gets the object from the API")
        void getsObject() throws Exception
        {
            final AXSystemWide object = AXAPI.getSystemWideAccessibilityObject();
            assertEquals(MockedConstant.AXUIElementCreateSystemWide.getCfRef(), AXUIElementHelper.getReference(object));
        }

    }

    @Nested
    @DisplayName("elementAtPosition")
    public class GetElement {

        @Test
        @DisplayName("gets an element at the system wide position")
        void getElement(final AXJLib axjlib) throws Exception
        {
            final long elementRef = 11L;
            mockAXUIElement(axjlib, elementRef, MockedString.Unknown);
            mockElementAtPosition(axjlib, MockedConstant.AXUIElementCreateSystemWide.getCfRef(), 100, 100, elementRef);

            AXUIElement object = AXAPI.elementAtPosition(100, 100);

            assertEquals(elementRef, AXUIElementHelper.getReference(object));
        }

    }

    @Nested
    @DisplayName("getApplicationAccessibilityObject")
    public class GetApplicationAccessibilityObjectMethod {

        @Test
        @DisplayName("gets the correct object from the API")
        void getsObject(final AXJLib axjlib) throws Exception
        {
            final int pid = 1234;
            when(axjlib.AXUIElementCreateApplication(pid)).thenReturn(10L);
            mockAXUIElement(axjlib, 10L, MockedString.AXApplication);
            final AXApplication object = AXAPI.getApplicationAccessibilityObject(pid);
            assertEquals(10L, AXUIElementHelper.getReference(object));
        }

    }
}
