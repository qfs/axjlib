/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
/**
 *
 */
package de.qfs.lib.axjlib;

import java.net.URL;

import de.qfs.lib.axjlib.appkit.NSRunningApplication;
import de.qfs.lib.axjlib.utils.OS;
import de.qfs.lib.axjlib.values.AXValue;

import lombok.SneakyThrows;

/**
 * Native library to interact with the AX... methods of the HIServices from Java
 * @author Pascal Bihler
 *
 */
public class AXJLib
{

    static {
        OS.loadLibrary("axjlib");
    }

    public /* final */ static AXJLib INSTANCE = new AXJLib();

    protected AXJLib() {
        // This is a singleton class
    }

    /**
     * Returns whether the current process is a trusted accessibility client.
     * @param optionsRef A CFDictionary of options, or 0 to specify no options. The following options are available:
     *
     * KEY: kAXTrustedCheckOptionPrompt
     * VALUE: A CFBooleanRef indicating whether the user will be informed if the current process is untrusted. This could be used, for example, on application startup to always warn a user if accessibility is not enabled for the current process. Prompting occurs asynchronously and does not affect the return value.
     *
     * @return Returns TRUE if the current process is a trusted accessibility client, FALSE if it is not.
     */
    public native boolean AXIsProcessTrustedWithOptions(long optionsRef);

    /**
     * @return a CFStringRef for the value of kAXTrustedCheckOptionPrompt
     */
    public native long kAXTrustedCheckOptionPrompt();

    /**
     * @return whether the current process is a trusted accessibility client.
     */
    public native boolean AXIsProcessTrusted();

    /**
     * Returns the unique type identifier for the AXUIElementRef type.
     *
     * @return Returns a CFTypeID representing the AXUIElementRef type.
     */
    public native long AXUIElementGetTypeID();

    /**
     * Returns a list of all the attributes supported by the specified accessibility object.
     *
     * @param element The AXUIElementRef representing the accessibility object.
     * @param arrayRef On return, a CFArrayRef containing the accessibility object's attribute names.
     */
    public native int AXUIElementCopyAttributeNames (long element, long[] arrayRef);


    /**
     * Returns the value of an accessibility object's attribute.
     *
     * @param element The AXUIElementRef representing the accessibility object.
     * @param attribute The attribute name.
     * @param valueRef On return, the value associated with the specified attribute.
     */
    public native int AXUIElementCopyAttributeValue (long element, long attribute, long[] valueRef);

    /**
     *  Returns whether the specified accessibility object's attribute can be modified.
     *  <br>
     *  If you receive a <code>kAXErrorCannotComplete</code> error from this function, you might want to repeat the request or change the timeout value.
     *
     *  @param element The AXUIElementRef representing the accessibility object.
     *  @param attribute The attribute name.
     *  @param booleanRef On return, a boolean value indicating whether the attribute is setable.
     *
     *  @return If unsuccessful, <code>AXUIElementIsAttributeSettable</code> may return one of the following error codes, among others:
    <dl><dt><code>kAXErrorCannotComplete</code></dt><dd>The function cannot complete because messaging has failed in some way (often due to a timeout).</dd>
    <dt><code>kAXErrorIllegalArgument</code></dt><dd>One or more of the arguments is an illegal value.</dd>
    <dt><code>kAXErrorAttributeUnsupported</code></dt><dd>The specified AXUIElementRef does not support the specified attribute.</dd>
    <dt><code>kAXErrorNoValue</code></dt><dd>The specified attribute does not have a value.</dd>
    <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd>
    <dt><code>kAXErrorNotImplemented</code></dt><dd>The process does not fully support the accessibility API.</dd></dl>
     */
    public native int AXUIElementIsAttributeSettable (long element, long attribute, boolean[] booleanRef);

    /**
     * Sets the accessibility object's attribute to the specified value.
     * <br>
     *  You can send and receive many different CFTypeRefs using the accessibility API.
     *  These include all CFPropertyListRef types, AXUIElementRef, AXValueRef, AXTextMarkerRef, AXTextMarkerRangeRef,
     *  CFNullRef, CFAttributedStringRef, and CRURLRef.
     *  @param element The AXUIElementRef representing the accessibility object.
     *  @param attribute The attribute name.
     *  @param value The new value for the attribute.
     *
     *  @return If unsuccessful, <code>AXUIElementSetAttributeValue</code> may return one of the following error codes, among others:
    <dl><dt><code>kAXErrorIllegalArgument</code></dt><dd>The value is not recognized by the accessible application or one of the other arguments is an illegal value.</dd>
    <dt><code>kAXErrorAttributeUnsupported</code></dt><dd>The specified AXUIElementRef does not support the specified attribute.</dd>
    <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd>
    <dt><code>kAXErrorCannotComplete</code></dt><dd>The function cannot complete because messaging has failed in some way.</dd>
    <dt><code>kAXErrorNotImplemented</code></dt><dd>The process does not fully support the accessibility API.</dd></dl>
     */
    public native int AXUIElementSetAttributeValue (long element, long attribute, long value);


    /**
     * Returns a list of all the parameterized attributes supported by the specified accessibility object.
     *
     * @param element The AXUIElementRef representing the accessibility object.
     * @param arrayRef On return, an array containing the accessibility object's parameterized attribute names.
     */
    public native int AXUIElementCopyParameterizedAttributeNames (long element, long[] arrayRef);

    /**
     * Returns a list of all the actions the specified accessibility object can perform.
     *
     * @param element The AXUIElementRef representing the accessibility object.
     * @param namesRef On return, an array of actions the accessibility object can perform (empty if the accessibility object supports no actions).
     *
     * @return If unsuccessful, <code>AXUIElementCopyActionNames</code> may return one of the following error codes, among others:
    <dl><dt><code>kAXErrorIllegalArgument</code></dt><dd>One or both of the arguments is an illegal value.</dd>
    <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd>
    <dt><code>kAXErrorFailure</code></dt><dd>There was some sort of system memory failure.</dd>
    <dt><code>kAXErrorCannotComplete</code></dt><dd>The function cannot complete because messaging has failed in some way.</dd>
    <dt><code>kAXErrorNotImplemented</code></dt><dd>The process does not fully support the accessibility API.</dd></dl>
     */
    public native int AXUIElementCopyActionNames (long element, long[] namesRef);

    /**
     * Returns a localized description of the specified accessibility object's action.
     *
     * @param element The AXUIElementRef representing the accessibility object.
     * @param action The action to be described.
     * @param descriptionRef On return, a string containing the description of the action.
     *
     * @return If unsuccessful, <code>AXUIElementCopyActionDescription</code> may return one of the following error codes, among others:
    <dl><dt><code>kAXErrorActionUnsupported</code></dt><dd>The specified AXUIElementRef does not support the specified action (you will also receive this error if you pass in the system-wide accessibility object).</dd>
    <dt><code>kAXErrorIllegalArgument</code></dt><dd>One or more of the arguments is an illegal value.</dd>
    <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd>
    <dt><code>kAXErrorCannotComplete</code></dt><dd>The function cannot complete because messaging has failed in some way.</dd>
    <dt><code>kAXErrorNotImplemented</code></dt><dd>The process does not fully support the accessibility API.</dd></dl>
     */
    public native int AXUIElementCopyActionDescription (long element, long action, long[] descriptionRef);

    /**
     * Requests that the specified accessibility object perform the specified action.
     *
     * It is possible to receive the <code>kAXErrorCannotComplete</code> error code from this function because accessible applications often need to
     * perform some sort of modal processing inside their action callbacks and they may not return within the timeout value set by the accessibility API.
     * This does not necessarily mean that the function has failed, however. If appropriate, your assistive application
     * can try to call this function again. Also, you may be able to increase the timeout value.
     *
     * @param element The AXUIElementRef representing the accessibility object.
     * @param action The action to be performed.
     *
     * @return If unsuccessful, <code>AXUIElementPerformAction</code> may return one of the following error codes, among others:
    <dl><dt><code>kAXErrorActionUnsupported</code></dt><dd>The specified AXUIElementRef does not support the specified action (you will also receive this error if you pass in the system-wide accessibility object).</dd>
    <dt><code>kAXErrorIllegalArgument</code></dt><dd>One or more of the arguments is an illegal value.</dd>
    <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd>
    <dt><code>kAXErrorCannotComplete</code></dt><dd>The function cannot complete because messaging has failed in some way or the application has not yet responded.</dd>
    <dt><code>kAXErrorNotImplemented</code></dt><dd>The process does not fully support the accessibility API.</dd></dl>
     */
    public native int AXUIElementPerformAction (long element, long action);

    /**
     * Returns the accessibility object at the specified position in top-left relative screen coordinates.
     * <br>
     * This function does hit-testing based on window z-order (that is, layering). If one window is on top of another window, the returned accessibility object comes from whichever window is topmost at the specified
     * location. Note that if the system-wide accessibility object is passed in the <code>application</code> parameter, the position test is not restricted to a
     * particular application.
     *
     * @param application The AXUIElementRef representing the application that contains the screen coordinates (or the system-wide accessibility object).
     * @param x The horizontal position.
     * @param y The vertical position.
     * @param element On return, the accessibility object at the position specified by x and y.
     *
     * @return If unsuccessful, <code>AXUIElementCopyElementAtPosition</code> may return one of the following error codes, among others:
 <dl><dt><code>kAXErrorNoValue</code></dt><dd>There is no accessibility object at the specified position.</dd>
 <dt><code>kAXErrorIllegalArgument</code></dt><dd>One or more of the arguments is an illegal value.</dd>
 <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd>
 <dt><code>kAXErrorCannotComplete</code></dt><dd>The function cannot complete because messaging has failed in some way.</dd>
 <dt><code>kAXErrorNotImplemented</code></dt><dd>The process does not fully support the accessibility API.</dd></dl>
     */
    public native int AXUIElementCopyElementAtPosition (long application, float x, float y, long[] element);


    /**
     * Creates and returns the top-level accessibility object for the application with the specified process ID.
     *
     * @param pid The process ID of an application.
     * @return The AXUIElementRef representing the top-level accessibility object for the application with the specified process ID.
     */
    public native long AXUIElementCreateApplication (int pid);

    /**
     * Returns an accessibility object that provides access to system attributes.
     * <br>
     * This is useful for things like finding the focused accessibility object regardless of which application is currently active.
     * @return The AXUIElementRef representing the system-wide accessibility object.
     */
    public native long AXUIElementCreateSystemWide ();

    /**
     * Returns the process ID associated with the specified accessibility object.
     * @param element The AXUIElementRef representing an accessibility object.
     * @param p_pid On return, the process ID associated with the specified accessibility object.
     *
     * @return If unsuccessful, <code>AXUIElementGetPid</code> may return one of the following error codes, among others:
    <dl><dt><code>kAXErrorIllegalArgument</code></dt><dd>One or more of the arguments is an illegal value.</dd>
    <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd></dl>
     */
    public native int AXUIElementGetPid (long element, int[] p_pid);

    /**
     * Sets the timeout value used in the accessibility API.
     * <br>
     * Pass the system-wide accessibility object if you want to set the timeout globally for this process.
     * Setting the timeout on another accessibility object sets it only for that object,
     * not for other accessibility objects that are equal to it.
     * <br>
     * Setting <code>timeoutInSeconds</code> to 0 for the system-wide accessibility object resets the global
     * timeout to its default value. Setting <code>timeoutInSeconds</code>
     * to 0 for any other accessibility object makes that element use the current global timeout value.
     * @param element The AXUIElementRef representing an accessibility object.
     * @param timeoutInSeconds The number of seconds for the new timeout value.
     *
     * @return If unsuccessful, <code>AXUIElementSetMessagingTimeout</code> may return one of the following error codes, among others:
 <dl><dt><code>kAXErrorIllegalArgument</code></dt><dd>One or more of the arguments is an illegal value (timeout values must be positive).</dd>
 <dt><code>kAXErrorInvalidUIElement</code></dt><dd>The AXUIElementRef is invalid.</dd></dl>
     */
    public native int AXUIElementSetMessagingTimeout (long element, float timeoutInSeconds);


    /*
     * AXValue methods
     */

    public native long AXValueGetTypeID();


    /**
     * Encodes a AXValue into a CFTypeRef.
     *
     */
    public native long AXValueCreate (AXValue value);


    /**
     * Returns the structure type encoded in value. If the type is not recognized, it returns kAXValueIllegalType.
     **/
    public native long AXValueGetType(long valueRef);

    /**
     * Decodes the structure stored in value and returns it as java object.
     */
    public native AXValue AXValueGetValue(long valueRef);


    /*
     * Helper methods to create / interact with macOS datatypes
     *
     */

    /**
     * Retains a Core Foundation object.
     *
     * @param cfRef A CFType object to retain.
     *
     */
    public native void CFRetain(long cfRef);

    /**
     * Releases a Core Foundation object.
     *
     * @param cfRef A CFType object to release.
     *
     */
    public native void CFRelease(long cfRef);

    /**
     * Returns the reference count of a Core Foundation object.
     * @param objRef The CFType object to examine.
     * @return A number representing the reference count of cfRef.
     */
    public native long CFGetRetainCount(long objRef);

    /**
     * Creates an immutable CFString from a Java string.
     *
     * @param string The Java string to be used to create the CFString object.
     * @return the retained CFStringRef
     */
    public native long CFStringCreateWithJavaString(String string);

    /**
     * Creates an immutable Java String from a CFString string. Does not release the CF String.
     *
     * @param cfStringRef The CFString to be used to create the String object.
     * @return the Java string with the content of the referenced CFString
     */
    public native String StringCreateWithCFStringRef(long cfStringRef);

    /**
     * Creates a CFBooleanRef from a Java boolean.
     *
     * @param bool The Java boolean defining the value of the CFBooleanRef.
     * @return the retained CFBooleanRef
     */
    public long CFBooleanCreateWithJavaBoolean(Boolean bool) {
        if (bool == null) return 0;
        return CFBooleanCreate(bool.booleanValue());
    }
    private native long CFBooleanCreate(boolean bool);


    /**
     * Returns the value of a CFBoolean object as a standard Java boolean.
     *
     * @param cfBooleanRef The boolean to examine.
     * @return the value of the referenced CFBoolean
     */
    public Boolean BooleanCreateWithCFBoolean(long cfBooleanRef) {
        if (cfBooleanRef == 0) return null;
        return Boolean.valueOf(CFBooleanGetValue(cfBooleanRef));
    }
    private native boolean CFBooleanGetValue(long cfBooleanRef);

    /**
     * Creates a CFNumberRef from a Java Number.
     *
     * @param number The Java Number defining the value of the CFNumberRef.
     * @return the retained CFNumberRef
     */
    public native long CFNumberCreateWithJavaNumber(Number number);

    /**
     * Returns the value of a CFNumber object as a standard Java Number.
     *
     * @param cfNumberRef The number to convert.
     * @return the value of the referenced CFNumberRef
     */
    public native Number NumberCreateWithCFNumberRef(long cfNumberRef);

    /**
     * Creates a CFURLRef from a Java URL.
     *
     * @param url The Java URL defining the value of the CFURLRef.
     * @return the retained CFURLRef
     */
    public native long CFURLCreateWithJavaURL(URL url);

    /**
     * Returns the value of a CFURL object as a standard Java URL.
     *
     * @param cfUrlRef The url to convert.
     * @return the value of the referenced CFURLRef
     */
    @SneakyThrows
    public URL URLCreateWithCFURLRef(long cfUrlRef) {
        String urlString = StringCreateWithCFURLRef(cfUrlRef);
        if (urlString == null) return null;
        return new URL(urlString);
    }
    private native String StringCreateWithCFURLRef(long cfUrlRef);

    /**
     * Creates a new immutable array with the given values.
     *
     * Values must be CFTypeRefs.
     *
     * @return the reference to the created array (This value may be 0 if values.length==0), or null.
     */
    public native long CFArrayCreate(long[] values);

    /**
     * Creates a new java array with the retained content of a CFArray
     *
     * @param cfArrayRef The CFArrayRef to be used to create the java array.
     * @return a java array with the references in the references CFArray
     */
    public native long[] ArrayCopyFromCFArrayRef(long cfArrayRef);

    /**
     * Creates an immutable dictionary containing the specified key-value pairs.
     *
     * Keys must be CFString references, and values must be CFTypeRefs
     *
     * @return the reference to the created dictionary (This value may be 0 if keys.length==values.length==0), or null.
     */
    public native long CFDictionaryCreate(long[] keys, long[] values);

    /**
     * Creates a new 2D java array with two long[] arrays: the first contains CFStringRefs of the retained dictionary keys,
     * the second contains CFTypeRefs of the retained dictionary values
     *
     * @param cfDictionaryRef The CFDictionaryRef to be used to create the java arrays.
     * @return a 2d array with the content refs from the references CFDictionary
     */
    public native long[][] ArraysCopyFromCFDictionaryRef(long cfDictionaryRef);

    /**
     * Prints a description of a Core Foundation object to stderr.
     * <br>
     * This function is useful as a debugging aid for Core Foundation objects. Because these objects are based on opaque types, it is difficult to examine their contents directly. However, the opaque types implement description function callbacks that return descriptions of their objects. This function invokes these callbacks.
     * @param objRef A Core Foundation object derived from CFType. If obj is not a Core Foundation object, an assertion is raised.
     *
     */
    public native void CFShow(long objRef);

    /**
     * Prints the attributes of a string during debugging.
     * <br>
     * Use this function to learn about specific attributes of a CFString object during debugging. These attributes include the following:
     * <ul>
     * <li> Length (in Unicode characters)
     * <li> Whether originally it was an 8-bit string and, if so, whether it was a C (HasNullByte) or Pascal (HasLengthByte) string
     * <li> Whether it is a mutable or an immutable object
     * <li> The allocator used to create it
     * <li> The memory address of the character contents and whether those contents are in-line
     * <li> The information provided by this function is for debugging purposes only. The values of any of these attributes might change between different releases and on different platforms. Note in particular that this function does not show the contents of the string. If you want to display the contents of the string, use CFShow.
     * @param stringRef The string whose attributes you want to print.
     */
    public native void CFShowStr(long stringRef);

    /**
     * Returns a textual description of a Core Foundation object.
     *
     * @param objRef The CFType object (a generic reference of type CFTypeRef) from which to derive a description.
     * @return A CFString reference that contains a description of cf.
     */
    public native long CFCopyDescription(long objRef);

    /**
     * Returns the unique identifier of an opaque type to which a Core Foundation object belongs.
     * <br>
     * This function returns a value that uniquely identifies the opaque type of any Core Foundation object. You can compare this value with the known CFTypeID identifier obtained with a “GetTypeID” function specific to a type, for example CFDateGetTypeID. These values might change from release to release or platform to platform.
     *
     * @param objRef The CFType object to examine.
     * @return A value of type CFTypeID that identifies the opaque type of cf.
     */
    public native long CFGetTypeID(long objRef);

    /**
     * Returns the type identifier for the CFString opaque type.
     * <br>
     * CFMutableString objects have the same type identifier as CFString objects.
     */
    public native long CFStringGetTypeID();

    /**
     * Returns the type identifier for the CFBoolean opaque type.
     */
    public native long CFBooleanGetTypeID();

    /**
     * Returns the type identifier for the CFNumber opaque type.
     */
    public native long CFNumberGetTypeID();

    /**
     * Returns the type identifier for the CFURL opaque type.
     */
    public native long CFURLGetTypeID();

    /**
     * Returns the type identifier for the CFArray opaque type.
     * <br>
     * CFMutableArray objects have the same type identifier as CFArray objects.
     */
    public native long CFArrayGetTypeID();

    /**
     * Returns the type identifier for the CFDictionary opaque type.
     * <br>
     * CFMutableDictionary objects have the same type identifier as CFDictionary objects.
     */
    public native long CFDictionaryGetTypeID();


    /*
     * Access to Information about running applications
     */

    /**
     * Returns an array of running applications, as seen by the Cocoa Core framework
     */
    public native NSRunningApplication[] runningApplications();

    /**
     * Returns the running application with the given process identifier, or null if no application has that pid
     */
    public native NSRunningApplication runningApplicationWithProcessIdentifier(int processIdentifier);

    /**
     * Returns an array of currently running applications with the specified bundle identifier.
     */
    public native NSRunningApplication[] runningApplicationsWithBundleIdentifier(String bundleIdentifier);

    /**
     * Returns an NSRunningApplication representing this application.
     */
    public native NSRunningApplication currentApplication();


    /*
     * Access to Information about file system
     */

    /**
     * Creates a *retained* list of directory search paths.
     * <br>
     * Creates a list of path strings for the specified directories in the specified domains.
     * The list is in the order in which you should search the directories.
     *
     * @param directory An NSSearchPathDirectory constant
     * @param domainMask AN NSSearchPathDomainMask constant
     * @param expandTilde If expandTilde is YES, tildes are expanded
     *
     * @return A retained CFArrayRef
     */
    public native long NSSearchPathForDirectoriesInDomains(int directory, int domainMask, boolean expandTilde);

}
