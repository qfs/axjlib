/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * Helper class to load library from file system or jar
 *
 * @author Pascal Bihler
 *
 */
public class OS
{

    /**
     * True on a macoS system, false otherwise
     */
    public final static boolean IS_MAC_OS_X =
        System.getProperty("os.name").toUpperCase().indexOf("MAC") >= 0;


    /**
     * Loads a library from the current dir, if exists, or copies it from the jar to a tempdir and loads it from there
     *
     * @param libName the library to load
     */
    public static void loadLibrary(final String libName)
    {
        if (IS_MAC_OS_X) {
            try {
                // from filesystem first
                load(libName);
            } catch (final UnsatisfiedLinkError e) {
                try {
                    extractAndLoad(libName);
                } catch (final IOException ioEx) {
                    System.err.println(ioEx);
                }
            }
        } else {
            System.out.printf("Skipping loading %s on non-macOS system: %s", libName, System.getProperty("os.name"));
        }
    }

    private static void load(final String libName)
    {
        if (libName.indexOf(File.separator) > -1) {
            System.load (libName);
        } else {
            System.loadLibrary (libName);
        }
    }


    private static void extractAndLoad(final String libName) throws IOException
    {
        final String fullName = "lib" + libName  + ".dylib";

        final String tempDir = System.getProperty("java.io.tmpdir");
        final File tempLibDir = new File(tempDir, libName + "-" + System.nanoTime());

        if (!tempLibDir.mkdir()) {
            throw new IOException("Could not create temp dir " + tempLibDir.getName());
        }

        final File libFile = new File(tempLibDir, fullName);
        try (InputStream is = OS.class.getResourceAsStream("/" + fullName)) {
            Files.copy(is, libFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (final IOException e) {
            libFile.delete();
            tempLibDir.delete();
            throw e;
        } catch (final NullPointerException e) {
            libFile.delete();
            tempLibDir.delete();
            throw new IllegalArgumentException("Lib " + fullName + "could not be found.");
        }
        try {
            load(libFile.getAbsolutePath());
        } finally {
            if (libFile.delete()) { // Try to delete immediately (if not locked by the system)
                if (!tempLibDir.delete()) {
                    tempLibDir.deleteOnExit();
                }
            } else {
                libFile.deleteOnExit();
                tempLibDir.deleteOnExit();
            }
        }
    }

}
