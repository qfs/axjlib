/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.utils;

import java.net.URL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.ui.AXUIElementFactory;
import de.qfs.lib.axjlib.values.AXValue;
import de.qfs.lib.axjlib.values.AXValueFactory;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Helper to convert between different CFTypes and java
 * @author Pascal Bihler
 *
 */
public class TypeConverter
{
    public /* final */ static long AXUIElementTypeID;
    public /* final */ static long AXValueTypeID;
    public /* final */ static long CFStringTypeID;
    public /* final */ static long CFBooleanTypeID;
    public /* final */ static long CFNumberTypeID;
    public /* final */ static long CFURLTypeID;
    public /* final */ static long CFArrayTypeID;
    public /* final */ static long CFDictionaryTypeID;

    static {
        initTypeConstants();
    }

    static TypeConverter instance = new TypeConverter();

    public static TypeConverter instance() {
        return instance;
    }

    static void initTypeConstants() {
        AXUIElementTypeID = AXJLib.INSTANCE.AXUIElementGetTypeID();
        AXValueTypeID = AXJLib.INSTANCE.AXValueGetTypeID();
        CFStringTypeID = AXJLib.INSTANCE.CFStringGetTypeID();
        CFBooleanTypeID = AXJLib.INSTANCE.CFBooleanGetTypeID();
        CFNumberTypeID = AXJLib.INSTANCE.CFNumberGetTypeID();
        CFURLTypeID = AXJLib.INSTANCE.CFURLGetTypeID();
        CFArrayTypeID = AXJLib.INSTANCE.CFArrayGetTypeID();
        CFDictionaryTypeID = AXJLib.INSTANCE.CFDictionaryGetTypeID();
    }

    protected TypeConverter() {/*Singleton*/}

    public boolean referenceIsOfType(final long reference, final long expectedTypeId) {
        final long typeId = AXJLib.INSTANCE.CFGetTypeID(reference);
        return typeId == expectedTypeId;
    }

    public Object convertFromCFTypeRef(final long reference)
    {
        if (reference == 0) return null;

        final long typeId = AXJLib.INSTANCE.CFGetTypeID(reference);
        if (typeId == CFArrayTypeID) {
            return getListFromCfArray(reference);
        } else if (typeId == CFDictionaryTypeID) {
            return getMapFromCfDictionary(reference);
        } else if (typeId == CFStringTypeID) {
            return AXJLib.INSTANCE.StringCreateWithCFStringRef(reference);
        } else if (typeId == CFBooleanTypeID) {
            return AXJLib.INSTANCE.BooleanCreateWithCFBoolean(reference);
        } else if (typeId == CFNumberTypeID) {
            return AXJLib.INSTANCE.NumberCreateWithCFNumberRef(reference);
        } else if (typeId == CFURLTypeID) {
            return AXJLib.INSTANCE.URLCreateWithCFURLRef(reference);
        } else if (typeId == AXValueTypeID) {
            return AXValueFactory.instance().getValue(reference);
        } else if (typeId == AXUIElementTypeID) {
            return AXUIElementFactory.instance().getElement(reference);
        }

        return new UnknownType(reference, typeId);
    }

    /**
     * Converts an object (as far as implemented) to a retained CFType and returns it reference
     */
    public long createCFTypeRef(final Object object)
    {
        if (object == null) return 0;

        if (object instanceof String) {
            return AXJLib.INSTANCE.CFStringCreateWithJavaString((String) object);
        } else if (object instanceof Boolean) {
            return AXJLib.INSTANCE.CFBooleanCreateWithJavaBoolean((Boolean) object);
        } else if (object instanceof Number) {
            return AXJLib.INSTANCE.CFNumberCreateWithJavaNumber((Number) object);
        } else if (object instanceof URL) {
            return AXJLib.INSTANCE.CFURLCreateWithJavaURL((URL) object);
        } else if (object instanceof AXValue) {
            return ((AXValue) object).createAXValueRef();
        } else if (object instanceof Collection) {
            final int size = ((Collection<?>) object).size();
            final long[] references = new long[size];
            int i = 0;
            for(final Object member: (Collection<?>) object) {
                references[i++] = createCFTypeRef(member);
            }
            try {
                return AXJLib.INSTANCE.CFArrayCreate(references);
            } finally {
                // The CFArray retains the content itself, so we have to release the objects we created
                for (i = 0; i < size; i++) {
                    AXJLib.INSTANCE.CFRelease(references[i]);
                }
            }
        } else if (object instanceof Map) {
            final int size = ((Map<?,?>) object).size();
            final long[] keys = new long[size];
            final long[] values = new long[size];
            int i = 0;
            for(final Entry<?,?> member: ((Map<?,?>) object).entrySet()) {
                keys[i] = createCFTypeRef(String.valueOf(member.getKey()));
                values[i++] = createCFTypeRef(member.getValue());
            }
            try {
                return AXJLib.INSTANCE.CFDictionaryCreate(keys, values);
            } finally {
                // The CFDictionary retains the content itself, so we have to release the objects we created
                for (i = 0; i < size; i++) {
                    AXJLib.INSTANCE.CFRelease(keys[i]);
                    AXJLib.INSTANCE.CFRelease(values[i]);
                }
            }
        }

        throw new IllegalArgumentException("Cannot create cfTypeRef for " + object.getClass());
    }

    private List<Object> getListFromCfArray(final long cfArray)
    {
        List<Object> list = new ArrayList<>();
        if (cfArray == 0) return list; // empty :(

        final long[] arrayContent = AXJLib.INSTANCE.ArrayCopyFromCFArrayRef(cfArray);
        for (final long reference: arrayContent) {
            try {
                final Object object = convertFromCFTypeRef(reference);
                list.add(object);
            } finally {
                AXJLib.INSTANCE.CFRelease(reference);
            }
        }

        return list;
    }

    private Map<String,Object> getMapFromCfDictionary(final long cfDict)
    {
        Map<String,Object> map = new HashMap<>();
        if (cfDict == 0) return map; // empty :(

        final long[][] contentArrays = AXJLib.INSTANCE.ArraysCopyFromCFDictionaryRef(cfDict);
        final long[] keys = contentArrays[0];
        final long[] values = contentArrays[1];
        final int size = Math.min(keys.length, values.length);

        for (int i = 0; i < size; i++) {
            long keyRef = keys[i];
            long valRef = values[i];
            Object value = null;

            try {
                value = convertFromCFTypeRef(valRef);
            } finally {
                AXJLib.INSTANCE.CFRelease(valRef);
            }

            try {
                final String key = AXJLib.INSTANCE.StringCreateWithCFStringRef(keyRef);
                map.put(key, value);
            } finally {
                AXJLib.INSTANCE.CFRelease(keyRef);
            }
        }

        return map;
    }

    @EqualsAndHashCode
    public static class UnknownType {
        @Getter final long reference;
        @Getter final long typeId;
        final AXJLib axjlib;

        UnknownType(final long reference, final long typeId) {
            if (reference == 0) throw new IllegalArgumentException("Reference must not be 0");
            this.reference = reference;
            this.typeId = typeId;
            axjlib = AXJLib.INSTANCE;
            axjlib.CFRetain(reference);
        }

        @Override
        protected void finalize() throws Throwable
        {
            axjlib.CFRelease(reference);
            super.finalize();
        }

        @Override
        public String toString()
        {
            return "UnknownType [reference=" + reference + ", typeId=" + typeId + "] " + AXUtils.getDescription(reference);
        }

    }
}
