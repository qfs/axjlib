/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;
import java.util.function.ToIntFunction;

import de.qfs.lib.axjlib.AXError;
import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;

public class AXUtils
{
    final static ConcurrentHashMap<String, Long> cfStringCache = new CFHashMap<String>();

    /**
     * Returns the description of the referenced cf object
     * @param reference the CFTypeRef
     */
    public static String getDescription(final long reference) {
        try {
            final Object description = callMethodReturningCFTypeRef(() -> AXJLib.INSTANCE.CFCopyDescription(reference));
            if (description == null) return null;
            if (description instanceof String) return (String)description;
            return "";
        } catch (final Exception ex) {
            ex.printStackTrace();
            return "getDescription excepted: " + ex.getMessage();
        }
    }


    /**
     * Runs a given method and converts the resulting CFTypeRef to a java object.
     *
     * @param methodCall A Supplier for the CFTypeRef code, i.e. the AX-Method call
     */
    public static Object callMethodReturningCFTypeRef(final LongSupplier methodCall) {

        long reference = 0;
        try {
            reference = methodCall.getAsLong();
            return TypeConverter.instance().convertFromCFTypeRef(reference);
        } finally {
            AXJLib.INSTANCE.CFRelease(reference);
        }
    }

    /**
     * Runs a given method (without return parameter) and interprets the result as AXError code.
     *
     * @param methodCall A Supplier for the AXError code, i.e. the AX-Method call
     * @throws AXException if the result differs from {@link AXError#kAXErrorSuccess} (0)
     */
    public static void callAXMethod(final String action, final IntSupplier methodCall) {
        final int result = methodCall.getAsInt();
        if (result != 0) throw new AXException(result, action);
    }


    /**
     * Runs a given method (with return parameter, given as long[] array as reference storage)
     * and interprets the result as AXError code.
     *
     * @param methodCall A Supplier for the AXError code, i.e. the AX-Method call
     * @return The result converted to a java object
     *
     * @throws AXException if the result differs from {@link AXError#kAXErrorSuccess} (0)
     */
    public static Object callAXMethodWithResult(final String action, final ToIntFunction<long[]> methodCall) {
        final long[] cfArray = new long[1];
        try {
            final int result = methodCall.applyAsInt(cfArray);
            if (result != 0) throw new AXException(result, action);

            return TypeConverter.instance().convertFromCFTypeRef(cfArray[0]);
        } finally {
            AXJLib.INSTANCE.CFRelease(cfArray[0]);
        }
    }

    /**
     * Runs a given method (with boolean return parameter, given as boolean[] array as reference storage)
     * and interprets the result as AXError code.
     *
     * @param methodCall A Supplier for the AXError code, i.e. the AX-Method call
     * @return The result converted to a java object
     *
     * @throws AXException if the result differs from {@link AXError#kAXErrorSuccess} (0)
     */
    public static boolean callAXMethodWithBooleanResult(final String action, final ToIntFunction<boolean[]> methodCall) {
        final boolean[] array = new boolean[1];
        final int result = methodCall.applyAsInt(array);
        if (result != 0) throw new AXException(result, action);
        return array[0];
    }

    /**
     * This method converts a java String to a CFStringRef and caches
     * this reference, so upon the next call with the same string
     * the same reference is returned. The cached String is retained
     * and released upon cache clear.
     *
     * @param string the string to convert
     * @return the cf string ref
     */
    public static long getCachedCFString(final String string) {
        if (string == null) return 0;

        Long reference = cfStringCache.get(string);
        if (reference == null) {
            reference = AXJLib.INSTANCE.CFStringCreateWithJavaString(string);
            if (reference != 0) {
                final Long prevReference = cfStringCache.putIfAbsent(string, reference);
                if (prevReference != null) {
                    AXJLib.INSTANCE.CFRelease(reference); // reference not used anymore
                    return prevReference;
                }
            }
        }
        return reference;
    }


    /**
     * A concurrent hash map for caching CFTypeRef.
     *
     * Despite the name, removing objects/clearing the cache is currently not thread save.
     *
     * If a CFHashMap is freed during runtime, call clear() first to assure proper content release
     * (Finalizer does not work properly during shutdwon with native lib)
     *
     * @param <T> The key type
     */
    protected static class CFHashMap<T>
            extends ConcurrentHashMap<T, Long>
    {
        private static final long serialVersionUID = -4269363210956816901L;

        @Override
        public Long remove(Object key)
        {
            Long reference = super.remove(key);
            if (reference != null && reference != 0) {
                AXJLib.INSTANCE.CFRelease(reference);
            }
            return reference;
        }

        @Override
        public void clear()
        {
            final Iterator<Entry<T, Long>> entryIterator = entrySet().iterator();
            Entry<T, Long> next;
            try {
                while (true) {
                    next = entryIterator.next();
                    if (next != null) {
                        Long reference = next.getValue();
                        AXJLib.INSTANCE.CFRelease(reference);
                    }
                    entryIterator.remove();
                }
            } catch (final NoSuchElementException ex) {
                // done
            }
        }

        @Override
        public boolean remove(Object key, Object value)
        {
            boolean success = super.remove(key, value);
            if (success && value != null && (Long) value != 0) {
                AXJLib.INSTANCE.CFRelease((Long) value);
            }
            return success;
        }

        @Override
        public boolean replace(T key, Long oldValue, Long newValue)
        {
            boolean success = super.replace(key, oldValue, newValue);
            if (success && oldValue != null && (Long) oldValue != 0) {
                AXJLib.INSTANCE.CFRelease((Long) oldValue);
            }
            return success;
        }

        @Override
        public Long replace(T key, Long value)
        {
            Long reference = super.replace(key, value);
            if (reference != null && reference != 0) {
                AXJLib.INSTANCE.CFRelease(reference);
            }
            return reference;
        }

    }


}
