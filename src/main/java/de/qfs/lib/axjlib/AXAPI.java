/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AXUIElementFactory;
import de.qfs.lib.axjlib.ui.elements.AXApplication;
import de.qfs.lib.axjlib.ui.elements.AXSystemWide;
import de.qfs.lib.axjlib.utils.TypeConverter;

/**
 * Static helper to provide a nicer java API to the core AXJLib functions
 *
 * @author Pascal Bihler
 *
 */
public class AXAPI
{
    final static String kAXTrustedCheckOptionPrompt = "AXTrustedCheckOptionPrompt";

    final static AtomicReference<AXSystemWide> systemWideAccessibilityObjectReference = new AtomicReference<>();

    /**
     * Returns whether the current process is a trusted accessibility client.
     * @param informUser indicating whether the user will be informed if the current process is untrusted.
     * This could be used, for example, on application startup to always warn a user if accessibility is not enabled for the current process.
     * Prompting occurs asynchronously and does not affect the return value.
     *
     * @return true if the current process is a trusted accessibility client, false if it is not.
     */
    public static boolean isProcessTrusted(boolean informUser) {
        final Map<String,Boolean> options = new HashMap<>();
        options.put(kAXTrustedCheckOptionPrompt, informUser);
        long optionsRef = 0;
        try {
            optionsRef = TypeConverter.instance().createCFTypeRef(options);
            return AXJLib.INSTANCE.AXIsProcessTrustedWithOptions(optionsRef);
        } finally {
            AXJLib.INSTANCE.CFRelease(optionsRef);
        }
    }

    /**
     * Returns the accessibility object at the specified position in top-left relative screen coordinates,
     * unrestricted to the application
     *
     * @param x The horizontal position.
     * @param y The vertical position.
     * @return the accessibility object at the position specified by x and y.
     *
     * @throws AXException on error
     */
    public static AXUIElement elementAtPosition (final float x, final float y) {
        final AXSystemWide systemWide = getSystemWideAccessibilityObject();
        return systemWide.elementAtPosition(x, y);
    }

    /**
     * Creates and returns the top-level accessibility object for the application with the specified process ID.
     *
     * @param pid The process ID of an application.
     * @return The AXUIElement representing the top-level accessibility object for the application with the specified process ID.
    */
    public static AXApplication getApplicationAccessibilityObject (final int pid) {
        long reference = 0;
        try {
            reference = AXJLib.INSTANCE.AXUIElementCreateApplication(pid);
            return (AXApplication) AXUIElementFactory.instance().getElement(reference);
        } finally {
            AXJLib.INSTANCE.CFRelease(reference);
        }
    }

    /**
     * Returns an accessibility object that provides access to system attributes.
     * <br>
     * This is useful for things like finding the focused accessibility object regardless of which application is currently active.
     * @return The AXUIElement representing the system-wide accessibility object.
     */
    public static AXSystemWide getSystemWideAccessibilityObject () {
        AXSystemWide systemWide = systemWideAccessibilityObjectReference.get();
        if (systemWide == null) {
            final long reference = AXJLib.INSTANCE.AXUIElementCreateSystemWide();
            if (reference != 0) {
                try {
                    systemWide = (AXSystemWide) AXUIElementFactory.instance().getElement(reference);
                    final boolean update = systemWideAccessibilityObjectReference.compareAndSet(null, systemWide);
                    if (! update) {
                        return systemWideAccessibilityObjectReference.get();
                    }
                } finally {
                    AXJLib.INSTANCE.CFRelease(reference);
                }

                // call AXAPI.elementAtPosition once to be able to use getFocusedApplication etc.
                // Workaround for https://forums.developer.apple.com/thread/96423

                try {
                    AXAPI.elementAtPosition(0, 0);
                } catch (final AXException ex) {
                    // API might be disabled, ignore;
                }
            }
        }
        return systemWide;
    }

    public static void setGlobalMessagingTimeout(int timeoutInMs) {
        final AXSystemWide systemWide = getSystemWideAccessibilityObject();
        systemWide.setMessagingTimeout(timeoutInMs);
    }

    /**
     * The main method can be called to check from external if a process using this lib is trusted.
     */
    public static void main(final String[] args)
    {
        if (isProcessTrusted(false)) {
            System.out.println("Current process is trusted.");
            System.exit(0);
        } else {
            System.out.println("Current process is not trusted.");
            System.exit(1);
        }
    }

}
