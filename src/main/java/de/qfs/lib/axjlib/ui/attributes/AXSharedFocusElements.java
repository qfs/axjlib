package de.qfs.lib.axjlib.ui.attributes;

import java.util.List;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXSharedFocusElements extends AttributeAccessor
{
    @SuppressWarnings("unchecked")
    default public List<? extends AXUIElement> getSharedFocusElements() {
        return (List<? extends AXUIElement>) getAttributeValueOrNull("AXSharedFocusElements");
    }
}
