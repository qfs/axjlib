/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXAuditIssues;
import de.qfs.lib.axjlib.ui.attributes.AXEnhancedUserInterface;
import de.qfs.lib.axjlib.ui.attributes.AXExtrasMenuBar;
import de.qfs.lib.axjlib.ui.attributes.AXFocusedUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXFocusedWindow;
import de.qfs.lib.axjlib.ui.attributes.AXFrontmost;
import de.qfs.lib.axjlib.ui.attributes.AXFunctionRowTopLevelElements;
import de.qfs.lib.axjlib.ui.attributes.AXHidden;
import de.qfs.lib.axjlib.ui.attributes.AXMainWindow;
import de.qfs.lib.axjlib.ui.attributes.AXWindows;
import de.qfs.lib.axjlib.utils.AXUtils;

/**
 * An AXUIElement that represents an application
 */
public class AXApplication extends AXUIElement
    implements AXFocusedUIElement, AXFunctionRowTopLevelElements, AXFrontmost, AXExtrasMenuBar, AXMainWindow,
               AXFocusedWindow, AXEnhancedUserInterface, AXHidden, de.qfs.lib.axjlib.ui.attributes.AXMenuBar,
               AXWindows, AXAuditIssues {

    public static final String ROLE = "AXApplication";

    public AXApplication(final long reference) {
        super(reference);
    }

    /**
     * Returns the accessibility object at the specified position in top-left relative screen coordinates.
     * <br>
     * This function does hit-testing based on window z-order (that is, layering). If one window is on top of another window, the returned accessibility object comes from whichever window is topmost at the specified
     * location. Note that if the system-wide accessibility object is passed in the <code>application</code> parameter, the position test is not restricted to a
     * particular application.
     *
     * @param x The horizontal position.
     * @param y The vertical position.
     * @return the accessibility object at the position specified by x and y.
     *
     * @throws AXException on error
     */
    public AXUIElement elementAtPosition (final float x, final float y) {
        return (AXUIElement) AXUtils.callAXMethodWithResult("elementAtPosition(" + x + "," + y + ")",
                (elementRef) -> AXJLib.INSTANCE.AXUIElementCopyElementAtPosition(this.reference, x, y, elementRef)
               );
    }

}