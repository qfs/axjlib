/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXOrientation extends AttributeAccessor
{
    /**
     * Indicates whether this accessibility object is displayed or interacted with in a vertical or a horizontal manner.
     *  The interpretation of an element, such as a slider, can change depending on whether it is oriented vertically
     *  or horizontally. Using the value of this attribute, an assistive application can communicate this information to the user.
     */
    default public String getOrientation() {
        return (String) getAttributeValueOrNull("AXOrientation");
    }

    default public boolean hasHorizontalOrientation() {
        return "AXHorizontalOrientation".equals(getOrientation());
    }

    default public boolean hasVerticalOrientation() {
        return "AXVerticalOrientation".equals(getOrientation());
    }

    default public boolean hasUnkownOrientation() {
        return "AXUnknownOrientation".equals(getOrientation());
    }
}
