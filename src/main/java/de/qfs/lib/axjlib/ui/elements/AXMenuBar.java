/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.actions.AXCancel;
import de.qfs.lib.axjlib.ui.attributes.AXHelp;
import de.qfs.lib.axjlib.ui.attributes.AXSelectedChildren;
import de.qfs.lib.axjlib.ui.attributes.AXTitleUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXTopLevelUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXVisibleChildren;

/**
 * An AXUIElement that represents a MenuBar
 */
public class AXMenuBar extends AXUIElement
    implements AXTopLevelUIElement, AXHelp, AXSelectedChildren, AXVisibleChildren, AXTitleUIElement,
               AXCancel {

    public static final String ROLE = "AXMenuBar";

    public AXMenuBar(final long reference) {
        super(reference);
    }
}