/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;
import de.qfs.lib.axjlib.ui.elements.AXApplication;

public interface AXFocusedApplication extends AttributeAccessor
{
    /**
     * Indicates the application element that is currently accepting keyboard input.
     * This attribute is supported by the system-wide accessibility object to help an
     * assistive application quickly determine the application that is accepting keyboard input.
     * After the assistive application gets the accessibility object representing this application,
     * it can send a message to the application asking for its focused accessibility object.
     */
    default public AXApplication getFocusedApplication() {
        return (AXApplication) getAttributeValueOrNull("AXFocusedApplication");
    }
}
