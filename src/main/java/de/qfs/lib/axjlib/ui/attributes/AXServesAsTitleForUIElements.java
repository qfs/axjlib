/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import java.util.List;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXServesAsTitleForUIElements extends AttributeAccessor
{
    /**
     * An array of accessibility objects for which this accessibility object serves as the title.
     * For example, a piece of static text can serve as a title for one or more user interface elements.
     * Because this static text string is not displayed as part of any user interface element’s visual interface,
     * an assistive application does not know the title is associated with user interface elements.
     * By including this attribute in the accessibility object representing the title, you specify the accessibility
     * objects with which this title is associated.
     */
    @SuppressWarnings("unchecked")
    default public List<? extends AXUIElement> getServesAsTitleForUIElements() {
        return (List<AXUIElement>) getAttributeValueOrNull("AXServesAsTitleForUIElements");
    }
}
