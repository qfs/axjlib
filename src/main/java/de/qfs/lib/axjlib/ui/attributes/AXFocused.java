/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXFocused extends AttributeAccessor
{
    /**
     * Indicates whether the accessibility object currently has the keyboard focus.
     * Note that you can set the value of the AXFocused attribute to true to accept
     * keyboard focus. This attribute is required for all accessibility objects representing
     * elements that can receive keyboard focus.
     */
    default public Boolean isFocused() {
        return (Boolean) getAttributeValueOrNull("AXFocused");
    }

    default public boolean setFocused(final Boolean focused) {
        return safeSetAttribute("AXFocused", focused);
    }
}
