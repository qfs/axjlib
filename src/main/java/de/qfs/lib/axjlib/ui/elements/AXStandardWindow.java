/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;


import de.qfs.lib.axjlib.ui.attributes.AXCancelButton;
import de.qfs.lib.axjlib.ui.attributes.AXCloseButton;
import de.qfs.lib.axjlib.ui.attributes.AXDefaultButton;
import de.qfs.lib.axjlib.ui.attributes.AXFullScreenButton;
import de.qfs.lib.axjlib.ui.attributes.AXMain;
import de.qfs.lib.axjlib.ui.attributes.AXMinimizeButton;
import de.qfs.lib.axjlib.ui.attributes.AXModal;
import de.qfs.lib.axjlib.ui.attributes.AXProxy;
import de.qfs.lib.axjlib.ui.attributes.AXSections;
import de.qfs.lib.axjlib.ui.attributes.AXTitleUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXToolbarButton;
import de.qfs.lib.axjlib.ui.attributes.AXZoomButton;

public class AXStandardWindow extends AXWindow
    implements AXCancelButton, AXCloseButton, AXDefaultButton, AXFullScreenButton, AXMain, AXMinimizeButton, AXModal, AXProxy, AXSections, AXTitleUIElement, AXToolbarButton, AXZoomButton {

    public static final String SUB_ROLE = "AXStandardWindow";

    public AXStandardWindow(final long reference) {
        super(reference);
    }
}
