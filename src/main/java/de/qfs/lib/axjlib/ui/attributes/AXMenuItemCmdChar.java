/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXMenuItemCmdChar extends AttributeAccessor
{
    /**
     * The primary key in the keyboard shortcut for the command represented by this accessibility object.
     * For example, “O” is the primary key in the keyboard shortcut for the Open command.
     *
     */
    default public String getMenuItemCmdChar() {
        return (String) getAttributeValueOrNull("AXMenuItemCmdChar");
    }
}
