/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;
import de.qfs.lib.axjlib.ui.elements.AXScrollBar;

public interface AXHorizontalScrollBar extends AttributeAccessor
{
    /**
     * The horizontal scroll bar displayed by the user interface element this accessibility object represents.
     * This is a convenience attribute an assistive application can use easily to find the scroll bar without
     * traversing the accessibility hierarchy. This attribute is recommended for all accessibility objects that
     * display a horizontal scroll bar.
     */
    default public AXScrollBar getHorizontalScrollBar() {
        return (AXScrollBar) getAttributeValueOrNull("AXHorizontalScrollBar");
    }
}
