/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import java.util.List;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXVisibleChildren extends AttributeAccessor
{
    /**
     * An array of first-order accessibility objects contained by this accessibility object that are visible to a
     * sighted user. For example, a list view’s AXVisibleChildren array would contain the list’s subelements that
     * are currently scrolled into view. The members of the AXVisibleChildren array are a subset of the members of
     * this accessibility object’s AXChildren array. This attribute is recommended for accessibility objects whose
     * child objects can be scrolled out of view or otherwise obscured.
     */
    @SuppressWarnings("unchecked")
    default public List<? extends AXUIElement> getVisibleChildren() {
        return (List<AXUIElement>) getAttributeValueOrNull("AXVisibleChildren");
    }
}
