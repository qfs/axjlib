/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;

import de.qfs.lib.axjlib.annotations.GeneralReturnType;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXMenuItemCmdModifiers extends AttributeAccessor
{
    static enum KeyModifier {
        SHIFT,
        OPTION,
        CONTROL,
        NO_COMMAND;

        static EnumSet<KeyModifier> getEnumSet(final Long mask) {
            if (mask == null) return null;
            int maskValue = mask.intValue();
            final List <KeyModifier> list = new LinkedList<>();
            for (KeyModifier value : KeyModifier.values()) {
              if ((maskValue & (1 << value.ordinal())) != 0) {
                list.add(value);
              }
            }
            return list.size() == 0 ? EnumSet.noneOf(KeyModifier.class) : EnumSet.copyOf(list);
        }
    }

    /**
     * An integer mask that represents the modifier keys held down in the keyboard shortcut for the command
     * represented by this accessibility object.
     *
     */
    @GeneralReturnType
    default public EnumSet<KeyModifier> getMenuItemCmdModifiers() {
        return KeyModifier.getEnumSet((Long) getAttributeValueOrNull("AXMenuItemCmdModifiers"));
    }
}
