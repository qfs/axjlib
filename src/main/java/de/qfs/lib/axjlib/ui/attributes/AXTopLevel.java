/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXTopLevel extends AttributeAccessor
{
    /**
     * The window, sheet, or drawer element that contains this accessibility object. An accessibility object that is
     * contained in a window, sheet, or drawer includes this attribute so an assistive application easily can find
     * that element without having to step through all intervening objects in the accessibility hierarchy.
     * This attribute is required for all accessibility objects whose parent or more distant ancestor represents a
     * window, drawer, or sheet.
     */
    default public AXUIElement getTopLevel() {
        return (AXUIElement) getAttributeValueOrNull("AXTopLevel");
    }
}
