/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXWindow extends AttributeAccessor
{
    /**
     * The window element that contains this accessibility object. An accessibility object that is contained in a
     * window includes this attribute so an assistive application easily can find the window without having to step
     * through all intervening objects in the accessibility hierarchy. Note that the value of the AXWindow attribute
     * must be an accessibility object that represents a window, not a sheet or drawer. For a similar attribute that
     * is less restrictive, see kAXTopLevelUIElementAttribute. The AXWindow attribute is required for all
     * accessibility elements whose parent or more distant ancestor represents a window.
     */
    default public AXUIElement getWindow() {
        return (AXUIElement) getAttributeValueOrNull("AXWindow");
    }
}
