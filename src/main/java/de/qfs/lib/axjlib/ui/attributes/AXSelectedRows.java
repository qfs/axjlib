/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import java.util.List;

import de.qfs.lib.axjlib.ui.AttributeAccessor;
import de.qfs.lib.axjlib.ui.elements.AXRow;

public interface AXSelectedRows extends AttributeAccessor
{
    /**
     * An array of the accessibility objects representing the currently selected rows in this table or outline view.
     */
    @SuppressWarnings("unchecked")
    default public List<AXRow> getSelectedRows() {
        return (List<AXRow>) getAttributeValueOrNull("AXSelectedRows");
    }

    default public boolean setSelectedRows(final List<AXRow> selectedRows) {
        return safeSetAttribute("AXSelectedRows", selectedRows);
    }
}
