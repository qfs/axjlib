package de.qfs.lib.axjlib.ui.elements;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.actions.AXRaise;
import de.qfs.lib.axjlib.ui.attributes.AXCancelButton;
import de.qfs.lib.axjlib.ui.attributes.AXDefaultButton;
import de.qfs.lib.axjlib.ui.attributes.AXSections;

public class AXSheet extends AXUIElement
    implements AXCancelButton, AXDefaultButton, AXSections,
               AXRaise {

    public static final String ROLE = "AXSheet";

    public AXSheet(final long reference) {
        super(reference);
    }
}
