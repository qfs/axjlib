/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.qfs.lib.axjlib.annotations.GeneralReturnType;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

// Description from https://lists.apple.com/archives/accessibility-dev/2016/Apr/msg00015.html and following posts
public interface AXSections extends AttributeAccessor
{
    /**
     * AXSections is a attribute designed to categorize key functional sections of any application window into a very
     * small number of core operations that are common to most applications, to help assistive applications guide
     * users to the most important parts of any application window.
     *
     */
    @GeneralReturnType
    @SuppressWarnings("unchecked")
    default public List<AXSection> getSections() {
        final List<Map<String, Object>> rawList = (List<Map<String, Object>>) getAttributeValueOrNull("AXSections");
        if (rawList == null) return null;

        return rawList.stream().filter((entry) -> entry != null)
                .map((entry) -> new AXSection(entry)).collect(Collectors.toList());
    }

    default public boolean setSections(final List<AXSection> sections) {
        return safeSetAttribute("AXSections", sections);
    }

    public static class AXSection extends HashMap<String, Object> {
        private static final long serialVersionUID = 7133706890016819769L;

        public AXSection(Map<? extends String, ? extends Object> map) {
            super(map == null ? Collections.emptyMap() : map);
        }

        /**
         * A description of the section, if defined
         */
        public String getDescription() {
            return (String) this.get("SectionDescription");
        }

        /**
         * The Object of the section, if defined
         */
        public AXUIElement getObject() {
            return (AXUIElement) this.get("SectionObject");
        }

        /**
         * The unique ID of the section, if defined
         */
        public String getUniqueID() {
            return (String) this.get("SectionUniqueID");
        }
    }
}
