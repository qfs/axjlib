/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import lombok.NonNull;

/**
 * Methods from AXUIElement, which are used in the default attribute implementations.
 *
 * @author Pascal Bihler
 *
 */
public interface AttributeAccessor
{
    public Object getAttributeValue(@NonNull final String attributeName);
    public Object getAttributeValueOrNull(@NonNull final String attributeName);
    public Boolean isAttributeSettable(@NonNull final String attributeName);
    public void setAttribute(@NonNull final String attributeName, final Object attributeValue);
    public boolean safeSetAttribute(@NonNull final String attributeName, final Object attributeValue);
}
