/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import java.util.List;

import de.qfs.lib.axjlib.AXException;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.ui.attributes.AXChildren;
import de.qfs.lib.axjlib.ui.attributes.AXChildrenInNavigationOrder;
import de.qfs.lib.axjlib.ui.attributes.AXDescription;
import de.qfs.lib.axjlib.ui.attributes.AXEnabled;
import de.qfs.lib.axjlib.ui.attributes.AXFocused;
import de.qfs.lib.axjlib.ui.attributes.AXFrame;
import de.qfs.lib.axjlib.ui.attributes.AXGrowArea;
import de.qfs.lib.axjlib.ui.attributes.AXIdentifier;
import de.qfs.lib.axjlib.ui.attributes.AXParent;
import de.qfs.lib.axjlib.ui.attributes.AXPosition;
import de.qfs.lib.axjlib.ui.attributes.AXRole;
import de.qfs.lib.axjlib.ui.attributes.AXRoleDescription;
import de.qfs.lib.axjlib.ui.attributes.AXSize;
import de.qfs.lib.axjlib.ui.attributes.AXSubrole;
import de.qfs.lib.axjlib.ui.attributes.AXTitle;
import de.qfs.lib.axjlib.ui.attributes.AXTopLevel;
import de.qfs.lib.axjlib.utils.AXUtils;
import de.qfs.lib.axjlib.utils.TypeConverter;

import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * A class which encapsulates an AXUIElementRef.
 *
 * When the object is disposed, its reference is CFReleased
 *
 */
@EqualsAndHashCode
public class AXUIElement
implements AXChildrenInNavigationOrder, AXChildren, AXParent,
           de.qfs.lib.axjlib.ui.attributes.AXWindow, AXDescription, AXEnabled, AXFocused, AXRole, AXRoleDescription,
           AXSubrole, AXTitle, AXIdentifier, AXPosition, AXSize, AXGrowArea, AXFrame, AXTopLevel {

    protected final long reference;
    protected final AXJLib axjlib;

    public static boolean referenceIsAXUIElement(final long reference) {
        return TypeConverter.instance().referenceIsOfType(reference, TypeConverter.AXUIElementTypeID);
    }

    protected AXUIElement(final long reference) {
        if (reference == 0) throw new IllegalArgumentException("Reference must not be 0");
        this.reference = reference;
        axjlib = AXJLib.INSTANCE;
        axjlib.CFRetain(reference);
    }

    @Override
    protected void finalize() throws Throwable
    {
        axjlib.CFRelease(reference);
        super.finalize();
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()
                + "[reference=0x" + Long.toHexString(reference) + ","
                + "Role=" + getRole() + ","
                + "RoleDescription=" + getRoleDescription() + ","
                + "Title=" + getTitle() + "]";
    }

    public String getObjectDescription() {
        return AXUtils.getDescription(reference);
    }


    /**
     * @return the process ID associated with the specified accessibility object.
     */
    public int getPid () {
        final int[] p_pid = new int[1];
        final int result = axjlib.AXUIElementGetPid(this.reference, p_pid);
        if (result != 0) throw new AXException(result,"getPid");
        return p_pid[0];
    }

    /**
     * Returns the attribute names of the given element
     * @throws AXException
     */
    @SuppressWarnings("unchecked")
    public List<String> getAttributeNames() {
        return (List<String>) AXUtils.callAXMethodWithResult("getAttributeNames",
                cfArrayRef -> axjlib.AXUIElementCopyAttributeNames(this.reference, cfArrayRef)
               );
    }

    /**
     * Returns the attribute value, converted to a java object
     * @param attributeName the attribute name
     * @throws AXException
     */
    public Object getAttributeValue(@NonNull final String attributeName) {
        return getAttributeValue(this.reference, attributeName);
    }

    static Object getAttributeValue(final long reference, @NonNull final String attributeName) {
        final long cfAttributeString = AXUtils.getCachedCFString(attributeName);

        return AXUtils.callAXMethodWithResult("getAttributeValue(" + attributeName + ")",
                (value) -> AXJLib.INSTANCE.AXUIElementCopyAttributeValue(reference, cfAttributeString, value)
                );
    }

    /**
     * Returns the attribute value, converted to a java object
     * @param attributeName the attribute name
     * @return null when the value is not available, otherwise the value
     * @throws AXException
     */
    public Object getAttributeValueOrNull(@NonNull final String attributeName) {
        return getAttributeValueOrNull(reference, attributeName);
    }

    static public Object getAttributeValueOrNull(final long reference, @NonNull final String attributeName) {
        try {
            return getAttributeValue(reference, attributeName);
        } catch (final AXException ex) {
            switch (ex.getError()) {
            case kAXErrorNoValue:
            case kAXErrorAttributeUnsupported:
            case kAXErrorCannotComplete:
                return null;
            default: throw ex;
            }
        }
    }

    /**
     * Returns true when the attribute is settable, false otherwise
     */
    public Boolean isAttributeSettable(@NonNull final String attributeName) {
        final long cfAttributeString = AXUtils.getCachedCFString(attributeName);

        try {
            return (Boolean) AXUtils.callAXMethodWithBooleanResult("isAttributeSettable(" + attributeName + ")",
                (cfBooleanRef) -> axjlib.AXUIElementIsAttributeSettable(this.reference, cfAttributeString, cfBooleanRef)
               );
        } catch (final AXException ex) {
            return false;
        }
    }

    /**
     * Sets the specified attribute to the value, if possible
     * @throws AXException
     */
    public void setAttribute(@NonNull final String attributeName, final Object attributeValue) {
        final long cfAttributeString = AXUtils.getCachedCFString(attributeName);
        final long cfAttributeValue = TypeConverter.instance().createCFTypeRef(attributeValue);

        AXUtils.callAXMethod("setAttribute(" + attributeName + ")",
                () -> axjlib.AXUIElementSetAttributeValue(this.reference, cfAttributeString, cfAttributeValue)
               );
    }

    /**
     * Sets the specified attribute to the value, if possible.
     * @return false if the attribute could not be set, true otherwise
     */
    public boolean safeSetAttribute(@NonNull final String attributeName, final Object attributeValue) {
        try {
            setAttribute(attributeName, attributeValue);
            return true;
        } catch (final AXException ex) {
            return false;
        }
    }

    /**
     * Returns the parameterized attribute names of the given element
     * @throws AXException
     */
    @SuppressWarnings("unchecked")
    public List<String> getParameterizedAttributeNames() {
        return (List<String>) AXUtils.callAXMethodWithResult("getParameterizedAttributeNames()",
                cfArrayRef -> axjlib.AXUIElementCopyParameterizedAttributeNames(this.reference, cfArrayRef)
               );
    }

    /**
     * Returns the action names of the given element
     * @throws AXException
     */
    @SuppressWarnings("unchecked")
    public List<String> getActionNames() {
        return (List<String>) AXUtils.callAXMethodWithResult("getActionNames()",
                cfArrayRef -> axjlib.AXUIElementCopyActionNames(this.reference, cfArrayRef)
               );
    }

    public String getActionDescription(@NonNull final String actionName) {
        final long cfActionNameString = AXUtils.getCachedCFString(actionName);
        return (String) AXUtils.callAXMethodWithResult("getActionDescription(" + actionName + ")",
                cfArrayRef -> axjlib.AXUIElementCopyActionDescription(this.reference, cfActionNameString, cfArrayRef)
               );
    }

    public void performAction(@NonNull final String actionName) {
        final long cfActionNameString = AXUtils.getCachedCFString(actionName);
        AXUtils.callAXMethod("performAction(" + actionName + ")",
                 () -> axjlib.AXUIElementPerformAction(this.reference, cfActionNameString)
                );
    }

    public boolean safePerformAction(@NonNull final String actionName) {
        try {
            performAction(actionName);
            return true;
        } catch (final AXException ex) {
            switch (ex.getError()) {
            case kAXErrorCannotComplete:
                return false;
            default: throw ex;
            }
        }
    }

    public void setMessagingTimeout(final int timeoutInMs) {
        final float timeoutInSeconds = timeoutInMs / 1000.0f;
        AXUtils.callAXMethod("setMessagingTimeout(" + timeoutInMs + ")",
                () -> axjlib.AXUIElementSetMessagingTimeout(this.reference, timeoutInSeconds)
        );
    }

}