/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.annotations.GeneralReturnType;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXTopLevelUIElement extends AttributeAccessor
{
    /**
     * This is very much like the AXWindow, except that the value of this
     * attribute can be an element with role kAXSheetRole or kAXDrawerRole. It is
     * a short cut for traversing an element's parent hierarchy until an element of
     * role kAXWindowRole, kAXSheetRole, or kAXDrawerRole is found.
     *
     */
    @GeneralReturnType
    default public AXUIElement getTopLevelUIElement() {
        return (AXUIElement) getAttributeValueOrNull("AXTopLevelUIElement");
    }
}
