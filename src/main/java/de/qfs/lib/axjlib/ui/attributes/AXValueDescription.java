/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXValueDescription extends AttributeAccessor
{
    /**
     * Used to supplement kAXValueAttribute. This attribute returns a string
     * description that best describes the current value stored in kAXValueAttribute. This is useful for
     * things like slider where the numeric value in kAXValueAttribute does not always convey
     * enough information about the adjustment made on the slider. As an example, a color slider that
     * adjusts thru various colors cannot be well-described by the numeric value in existing
     * AXValueAttribute. This is where the kAXValueDescriptionAttribute comes in handy. In this example, the
     * developer can provide the color information using this attribute.
     */
    default public String getValueDescription() {
        return (String) getAttributeValueOrNull("AXValueDescription");
    }
}
