/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXMaxValue extends AttributeAccessor
{
    /**
     * Only used in conjunction with kAXValueAttribute and kAXMinValueAttribute,
     * this attribute represents the maximum value that an element can display. This is
     * useful for things like sliders and scroll bars, where the user needs to have an
     * understanding of how much the kAXValueAttribute can vary.
     *
     */
    default public Object getMaxValue() {
        return getAttributeValueOrNull("AXMaxValue");
    }
}
