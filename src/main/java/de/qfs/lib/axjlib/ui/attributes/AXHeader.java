/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXHeader extends AttributeAccessor
{
    /**
     * A convenience attribute whose value is an element that is a header for
     * another element. For example, an outline element has a header attribute whose value
     * is a element of role AXGroup that contains the header buttons for each column.
     * Used for things like tables, outlines, columns, etc.
     *
     */
    default public AXUIElement getHeader() {
        return (AXUIElement) getAttributeValueOrNull("AXHeader");
    }
}
