/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.actions.AXScrollToVisible;
import de.qfs.lib.axjlib.ui.actions.AXShowMenu;
import de.qfs.lib.axjlib.ui.attributes.AXBlockQuoteLevel;
import de.qfs.lib.axjlib.ui.attributes.AXCaretBrowsingEnabled;
import de.qfs.lib.axjlib.ui.attributes.AXDOMClassList;
import de.qfs.lib.axjlib.ui.attributes.AXDOMIdentifier;
import de.qfs.lib.axjlib.ui.attributes.AXElementBusy;
import de.qfs.lib.axjlib.ui.attributes.AXEndTextMarker;
import de.qfs.lib.axjlib.ui.attributes.AXHelp;
import de.qfs.lib.axjlib.ui.attributes.AXLanguage;
import de.qfs.lib.axjlib.ui.attributes.AXLayoutCount;
import de.qfs.lib.axjlib.ui.attributes.AXLinkUIElements;
import de.qfs.lib.axjlib.ui.attributes.AXLinkedUIElements;
import de.qfs.lib.axjlib.ui.attributes.AXLoaded;
import de.qfs.lib.axjlib.ui.attributes.AXLoadingProgress;
import de.qfs.lib.axjlib.ui.attributes.AXPreventKeyboardDOMEventDispatch;
import de.qfs.lib.axjlib.ui.attributes.AXSelected;
import de.qfs.lib.axjlib.ui.attributes.AXSelectedTextMarkerRange;
import de.qfs.lib.axjlib.ui.attributes.AXStartTextMarker;
import de.qfs.lib.axjlib.ui.attributes.AXTopLevelUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXURL;
import de.qfs.lib.axjlib.ui.attributes.AXValue;
import de.qfs.lib.axjlib.ui.attributes.AXVisited;

public class AXWebArea extends AXUIElement
    implements AXBlockQuoteLevel, AXCaretBrowsingEnabled, AXDOMClassList, AXDOMIdentifier, AXElementBusy, AXEndTextMarker, AXHelp, AXLanguage, AXLayoutCount, AXLinkUIElements, AXLinkedUIElements, AXLoaded, AXLoadingProgress, AXPreventKeyboardDOMEventDispatch, AXSelected, AXSelectedTextMarkerRange, AXStartTextMarker, AXTopLevelUIElement, AXURL, AXValue, AXVisited,
               AXScrollToVisible, AXShowMenu {

    public static final String ROLE = "AXWebArea";

    public AXWebArea(final long reference) {
        super(reference);
    }
}
