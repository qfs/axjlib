/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.annotations.GeneralReturnType;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXValue extends AttributeAccessor
{
    /**
     * A catch-all attribute that represents a user modifiable setting of an
     * element. For example, the contents of an editable text field, the position of a scroll bar
     * thumb, and whether a check box is checked are all communicated by the
     * kAXValueAttribute of their respective elements.
     *
     * Value: Varies, but will always be the same type for a given kind of element.
     * Each  role that offers kAXValueAttribute will specify the type of data that will be
     * used for its value.
     *
     */
    @GeneralReturnType
    default public Object getValue() {
        return getAttributeValueOrNull("AXValue");
    }

    default public boolean setValue(final Object value) {
        return safeSetAttribute("AXValue", value);
    }
}
