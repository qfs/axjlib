/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXGrowArea extends AttributeAccessor
{
    /**
     * The grow area of the window represented by this accessibility object.
     * An accessibility object includes this attribute to help an assistive application easily find a window’s grow
     * area, without having to traverse the accessibility hierarchy. This attribute is recommended for all
     * accessibility objects that represent windows that contain a grow area.
     *
     */
    default public AXUIElement getGrowArea() {
        return (AXUIElement) getAttributeValueOrNull("AXGrowArea");
    }
}
