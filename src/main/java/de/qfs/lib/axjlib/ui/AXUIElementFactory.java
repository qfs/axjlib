/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui;

import de.qfs.lib.axjlib.ui.elements.*;

/**
 * A factory creating AXUIElement subclasses based on the role of the reference
 *
 * @author Pascal Bihler
 *
 */
public class AXUIElementFactory
{
    static AXUIElementFactory instance = new AXUIElementFactory();

    public static AXUIElementFactory instance() {
        return instance;
    }

    protected AXUIElementFactory() {/*Singleton*/}

    /**
     * The referenced object should be retained
     * @return the element referenced by the AXUIElementRef as java object
     */
    public AXUIElement getElement(long reference) {
        final String role = (String)AXUIElement.getAttributeValueOrNull(reference,"AXRole");
        if (role == null) return null;

        String subrole = (String)AXUIElement.getAttributeValueOrNull(reference,"AXSubrole");
        if (subrole == null) subrole = "";

        switch (role) {
        case AXApplication.ROLE:
            return new AXApplication(reference);
        case AXBrowser.ROLE:
            return new AXBrowser(reference);
        case AXBusyIndicator.ROLE:
            return new AXBusyIndicator(reference);
        case AXButton.ROLE:
            switch (subrole) {
            case AXCloseButton.SUB_ROLE:
                return new AXCloseButton(reference);
            case AXDecrementArrow.SUB_ROLE:
                return new AXDecrementArrow(reference);
            case AXDecrementPage.SUB_ROLE:
                return new AXDecrementPage(reference);
            case AXFullScreenButton.SUB_ROLE:
                return new AXFullScreenButton(reference);
            case AXIncrementArrow.SUB_ROLE:
                return new AXIncrementArrow(reference);
            case AXIncrementPage.SUB_ROLE:
                return new AXIncrementPage(reference);
            case AXMinimizeButton.SUB_ROLE:
                return new AXMinimizeButton(reference);
            case AXZoomButton.SUB_ROLE:
                return new AXZoomButton(reference);
            default:
                return new AXButton(reference);
            }
        case AXCell.ROLE:
            return new AXCell(reference);
        case AXCheckBox.ROLE:
            return new AXCheckBox(reference);
        case AXColumn.ROLE:
            return new AXColumn(reference);
        case AXComboBox.ROLE:
            return new AXComboBox(reference);
        case AXDisclosureTriangle.ROLE:
            return new AXDisclosureTriangle(reference);
        case AXGroup.ROLE:
            return new AXGroup(reference);
        case AXHeading.ROLE:
            return new AXHeading(reference);
        case AXImage.ROLE:
            return new AXImage(reference);
        case AXLink.ROLE:
            return new AXLink(reference);
        case AXList.ROLE:
            return new AXList(reference);
        case AXMenu.ROLE:
            return new AXMenu(reference);
        case AXMenuBar.ROLE:
            return new AXMenuBar(reference);
        case AXMenuBarItem.ROLE:
            switch (subrole) {
            case AXMenuExtra.SUB_ROLE:
                return new AXMenuExtra(reference);
            default:
                return new AXMenuBarItem(reference);
            }
        case AXMenuButton.ROLE:
            return new AXMenuButton(reference);
        case AXMenuItem.ROLE:
            return new AXMenuItem(reference);
        case AXOutline.ROLE:
            return new AXOutline(reference);
        case AXPopUpButton.ROLE:
            return new AXPopUpButton(reference);
        case AXRadioButton.ROLE:
            return new AXRadioButton(reference);
        case AXRadioGroup.ROLE:
            return new AXRadioGroup(reference);
        case AXRow.ROLE:
            switch (subrole) {
            case AXOutlineRow.SUB_ROLE:
                return new AXOutlineRow(reference);
            case AXTableRow.SUB_ROLE:
                return new AXTableRow(reference);
            default:
                return new AXRow(reference);
            }
        case AXSafariAddressAndSearchField.ROLE:
            return new AXSafariAddressAndSearchField(reference);
        case AXScrollArea.ROLE:
            return new AXScrollArea(reference);
        case AXScrollBar.ROLE:
            return new AXScrollBar(reference);
        case AXSheet.ROLE:
            return new AXSheet(reference);
        case AXSplitGroup.ROLE:
            return new AXSplitGroup(reference);
        case AXSplitter.ROLE:
            return new AXSplitter(reference);
        case AXStaticText.ROLE:
            return new AXStaticText(reference);
        case AXSystemWide.ROLE:
            return new AXSystemWide(reference);
        case AXTabGroup.ROLE:
            return new AXTabGroup(reference);
        case AXTable.ROLE:
            return new AXTable(reference);
        case AXTextArea.ROLE:
            return new AXTextArea(reference);
        case AXTextField.ROLE:
            switch (subrole) {
            case AXSearchField.SUB_ROLE:
                return new AXSearchField(reference);
            default:
                return new AXTextField(reference);
            }
        case AXToolbar.ROLE:
            return new AXToolbar(reference);
        case AXValueIndicator.ROLE:
            return new AXValueIndicator(reference);
        case AXWebArea.ROLE:
            return new AXWebArea(reference);
        case AXWindow.ROLE:
            switch (subrole) {
            case AXDialog.SUB_ROLE:
                return new AXDialog(reference);
            case AXStandardWindow.SUB_ROLE:
                return new AXStandardWindow(reference);
            case AXSystemDialog.SUB_ROLE:
                return new AXSystemDialog(reference);
            default:
                return new AXWindow(reference);
            }
        }
        return new AXUIElement(reference);
    }
}
