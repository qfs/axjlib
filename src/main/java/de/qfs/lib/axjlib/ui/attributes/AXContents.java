/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import java.util.List;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXContents extends AttributeAccessor
{
    /**
     * A convenience attribute so assistive apps can find interesting child elements
     * of a given element, while at the same time avoiding non-interesting child
     * elements. For example, the contents of a scroll area are the children that
     * get scrolled, and not the horizontal and/or vertical scroll bars. The contents of
     * a tab group does not include the tabs themselves.
     *
     */
    @SuppressWarnings("unchecked")
    default public List<AXUIElement> getContents() {
        return (List<AXUIElement>) getAttributeValueOrNull("AXContents");
    }
}
