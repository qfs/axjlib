/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.actions.AXScrollDownByPage;
import de.qfs.lib.axjlib.ui.actions.AXScrollLeftByPage;
import de.qfs.lib.axjlib.ui.actions.AXScrollRightByPage;
import de.qfs.lib.axjlib.ui.actions.AXScrollUpByPage;
import de.qfs.lib.axjlib.ui.attributes.AXContents;
import de.qfs.lib.axjlib.ui.attributes.AXHelp;
import de.qfs.lib.axjlib.ui.attributes.AXHorizontalScrollBar;
import de.qfs.lib.axjlib.ui.attributes.AXMain;
import de.qfs.lib.axjlib.ui.attributes.AXTopLevelUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXVerticalScrollBar;

public class AXScrollArea extends AXUIElement
    implements AXContents, AXHelp, AXHorizontalScrollBar, AXMain, AXTopLevelUIElement, AXVerticalScrollBar,
               AXScrollDownByPage, AXScrollLeftByPage, AXScrollRightByPage, AXScrollUpByPage {

    public static final String ROLE = "AXScrollArea";

    public AXScrollArea(final long reference) {
        super(reference);
    }
}
