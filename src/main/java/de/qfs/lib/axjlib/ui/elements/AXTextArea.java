/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.elements;

import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.actions.AXPress;
import de.qfs.lib.axjlib.ui.attributes.AXHelp;
import de.qfs.lib.axjlib.ui.attributes.AXInsertionPointLineNumber;
import de.qfs.lib.axjlib.ui.attributes.AXNumberOfCharacters;
import de.qfs.lib.axjlib.ui.attributes.AXSelectedText;
import de.qfs.lib.axjlib.ui.attributes.AXSelectedTextRange;
import de.qfs.lib.axjlib.ui.attributes.AXTopLevelUIElement;
import de.qfs.lib.axjlib.ui.attributes.AXValue;
import de.qfs.lib.axjlib.ui.attributes.AXVisibleCharacterRange;

public class AXTextArea extends AXUIElement
    implements AXHelp, AXInsertionPointLineNumber, AXNumberOfCharacters, AXSelectedText, AXSelectedTextRange, AXTopLevelUIElement, AXValue, AXVisibleCharacterRange,
               AXPress {

    public static final String ROLE = "AXTextArea";

    public AXTextArea(final long reference) {
        super(reference);
    }
}
