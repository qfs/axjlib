/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.ui.attributes;

import de.qfs.lib.axjlib.ui.AttributeAccessor;

public interface AXMenuItemCmdGlyph extends AttributeAccessor
{
    /**
     * The glyph displayed for a physical key in the keyboard shortcut for the command represented by this
     * accessibility object, if it is different from the visible result of pressing the key. The Delete key,
     * for example, produces an invisible character, but it is associated with a visible glyph.
     *
     */
    default public Long getMenuItemCmdGlyph() {
        return (Long) getAttributeValueOrNull("AXMenuItemCmdGlyph");
    }
}
