/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

/**
 * An exception which can occur during the use of AX methods.
 * Encapsulates an AXerror return value
 */
public class AXException extends RuntimeException {
    private static final long serialVersionUID = 8350418473874129768L;
    final AXError error;
    final String action;

    public AXException(final int errorCode, final String action) {
        this(AXError.forIntValue(errorCode), action);
    }

    AXException(final AXError error, final String action) {
        super(createMessage(error, action));
        this.error = error;
        this.action = action;
    }

    public AXError getError() {
        return error;
    }

    private static String createMessage(final AXError error, final String action)
    {
        String message = (error == null ? null : error.toString());
        if (action != null) {
            message = (message == null ? "On " : message + " on ") + action;
        }
        return message;
    }

}