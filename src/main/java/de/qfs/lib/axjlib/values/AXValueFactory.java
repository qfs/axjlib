/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.values;

import de.qfs.lib.axjlib.AXJLib;

public class AXValueFactory
{
    static AXValueFactory instance = new AXValueFactory();

    public static AXValueFactory instance() {
        return instance;
    }

    protected AXValueFactory() {/*Singleton*/}

    public AXValue getValue(final long reference) {

        final AXValue value = AXJLib.INSTANCE.AXValueGetValue(reference);

        if (value != null) {
            return value;
        }

        return new AXValue.Unknown(reference);
    }

}
