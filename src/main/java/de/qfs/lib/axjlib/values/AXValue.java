/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.values;

import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.utils.AXUtils;

import lombok.EqualsAndHashCode;
import lombok.ToString;

public abstract class AXValue
{

    public static final long kAXValueCGPointType = 1;
    public static final long kAXValueCGSizeType = 2;
    public static final long kAXValueCGRectType = 3;
    public static final long kAXValueCFRangeType = 4;
    public static final long kAXValueAXErrorType = 5;
    public static final long kAXValueIllegalType = 0;


    public long createAXValueRef()
    {
        return AXJLib.INSTANCE.AXValueCreate(this);
    }

    @EqualsAndHashCode(callSuper=false)
    @ToString
    public static class Unknown extends AXValue {
        final String description;
        final long reference;

        /**
         * The reference is not retained or released by this data type
         *
         * @param reference
         */
        Unknown(final long reference) {
            this.reference = reference;
            description = AXUtils.getDescription(reference);
        }
    }
}
