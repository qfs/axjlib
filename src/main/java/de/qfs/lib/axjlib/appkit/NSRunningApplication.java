/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.appkit;

import java.io.File;
import java.io.UnsupportedEncodingException;

import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

/**
 * An object that can provide information for a single instance of an app.
 *
 * <p>In opposite to its Objective-C counterpart, this object is (currently)
 * read only and static (only represents the state of an application at object
 * creation time.
 *
 * <p>Some properties of an app are fixed, such as the bundle identifier.
 * Other properties may vary over time, such as whether the app is hidden.
 *
 * <p>Properties that vary over time are inherently race-prone. For example, a hidden app may unhide
 * itself at any time.
 *
 * <p>An NSRunningApplication instance remains valid after the app exits. However, most properties lose
 * their significance, and some properties may not be available on a terminated application.
 *
 * <p>The java version of this class is slightly extended in comparison to its Objective-C counterpart
 * to provide easier access to some properties.
 */
@Data
public class NSRunningApplication
{
    @RequiredArgsConstructor
    public enum ExecutableArchitecture {
        Unknown(0),

        /** The 32-bit PowerPC architecture. */
        NSBundleExecutableArchitecturePPC(0x00000012),

        /** The 32-bit Intel architecture. */
        NSBundleExecutableArchitectureI386(0x00000007),

        /** The 64-bit PowerPC architecture. */
        NSBundleExecutableArchitecturePPC64(0x01000012),

        /** The 64-bit Intel architecture. */
        NSBundleExecutableArchitectureX86_64(0x01000007);

        @Getter private final Integer intValue;

        private static Map<Integer,ExecutableArchitecture> lookupTable = new HashMap<>();
        static {
            for (ExecutableArchitecture architecture: values()) {
                lookupTable.put(architecture.getIntValue(),architecture);
            }
        }

        static ExecutableArchitecture byIntValue(final Integer intValue) {
            return lookupTable.getOrDefault(intValue, Unknown);
        }

    }

    final boolean active;
    final boolean hidden;
    final String name;
    final String localizedName;
    final String bundleIdentifier;
    final URL bundleURL;
    final File bundleFile;
    final ExecutableArchitecture executableArchitecture;
    final URL executableURL;
    final File executableFile;
    final Date launchDate;
    final boolean finishedLaunching;
    final int processIdentifier;
    @Getter(AccessLevel.PRIVATE) final boolean ownsMenuBar; // correct Name is ownsMenuBar()
    final boolean terminated;

    public NSRunningApplication(final boolean active,
                                final boolean hidden,
                                final String localizedName,
                                final String bundleIdentifier,
                                final String bundleURLString,
                                final int executableArchitectureValue,
                                final String executableURLString,
                                final long launchDateTimestamp,
                                final boolean finishedLaunching,
                                final int processIdentifier,
                                final boolean ownsMenuBar,
                                final boolean terminated) {
        this.active = active;
        this.hidden = hidden;
        this.localizedName = localizedName;
        this.bundleIdentifier = bundleIdentifier;
        this.bundleURL = stringToURL(bundleURLString);
        this.bundleFile = fileForURL(this.bundleURL);
        this.executableArchitecture = ExecutableArchitecture.byIntValue(executableArchitectureValue);
        this.executableURL = stringToURL(executableURLString);
        this.executableFile = fileForURL(this.executableURL);
        this.launchDate = launchDateTimestamp == 0 ? null : new Date(launchDateTimestamp);
        this.finishedLaunching = finishedLaunching;
        this.processIdentifier = processIdentifier;
        this.ownsMenuBar = ownsMenuBar;
        this.terminated = terminated;

        this.name = getUnlocalizedName();
    }

    public boolean ownsMenuBar() {
        return ownsMenuBar;
    }

    @SneakyThrows
    private URL stringToURL(final String urlString)
    {
        if (urlString == null) return null;
        return new URL(urlString);
    }

    @SneakyThrows
    private File fileForURL(final URL url)
    {
        if (url == null) return null;
        URI uri = url.toURI();
        if (! "file".equalsIgnoreCase(uri.getScheme())) return null;
        // Sanitize URI
        uri = new URI("file",null,uri.getPath(),null,null);
        return new File(uri);
    }

    private String getUnlocalizedName()
    {
        if (this.bundleURL != null) {
            String path = this.bundleURL.getPath();
            try {
                path = URLDecoder.decode(path,"UTF-8");
            } catch (final UnsupportedEncodingException e) {
                assert false : "Should not happen, since UTF-8 is default";
            }

            final int slashIndex = path.lastIndexOf("/");
            if (slashIndex > -1) {
                path = path.substring(slashIndex+1);
            }

            final int dotIndex = path.lastIndexOf(".");
            if (dotIndex > -1) {
                path = path.substring(0, dotIndex);
            }

            return path;
        }
        return localizedName; // fallback
    }
}
