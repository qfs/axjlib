/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib;

import java.util.HashMap;
import java.util.Map;


/**
 * An enum representation of the error codes returned by the AX methods
 */
public enum AXError {
    /** No error occurred. */
    kAXErrorSuccess(0),
    /** A system error occurred, such as the failure to allocate an object. */
    kAXErrorFailure(-25200),
    /** An illegal argument was passed to the function. */
    kAXErrorIllegalArgument(-25201),
    /** The AXUIElementRef passed to the function is invalid. */
    kAXErrorInvalidUIElement(-25202),
    /** The AXObserverRef passed to the function is not a valid observer. */
    kAXErrorInvalidUIElementObserver(-25203),
    /** The function cannot complete because messaging failed in some way or because the application with which the function is communicating is busy or unresponsive. */
    kAXErrorCannotComplete(-25204),
    /** The attribute is not supported by the AXUIElementRef. */
    kAXErrorAttributeUnsupported(-25205),
    /** The action is not supported by the AXUIElementRef. */
    kAXErrorActionUnsupported(-25206),
    /** The notification is not supported by the AXUIElementRef. */
    kAXErrorNotificationUnsupported(-25207),
    /** Indicates that the function or method is not implemented (this can be returned if a process does not support the accessibility API). */
    kAXErrorNotImplemented(-25208),
    /** This notification has already been registered for. */
    kAXErrorNotificationAlreadyRegistered(-25209),
    /** Indicates that a notification is not registered yet. */
    kAXErrorNotificationNotRegistered(-25210),
    /** The accessibility API is disabled (as when, for example, the user deselects "Enable access for assistive devices" in Universal Access Preferences). */
    kAXErrorAPIDisabled(-25211),
    /** The requested value or AXUIElementRef does not exist. */
    kAXErrorNoValue(-25212),
    /** The parameterized attribute is not supported by the AXUIElementRef. */
    kAXErrorParameterizedAttributeUnsupported(-25213),
    /** Not enough precision. */
    kAXErrorNotEnoughPrecision(-25214);

    final Integer intValue;
    private static final Map<Integer, AXError> lookup = new HashMap<Integer, AXError>();

    AXError(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    static {
        for (final AXError error: values()) {
            lookup.put(error.intValue, error);
        }
    }

    public static AXError forIntValue(int intValue) {
        return lookup.get(intValue);
    }
}