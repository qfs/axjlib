/*******************************************************************************
 * Copyright 2018 Quality First Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.qfs.lib.axjlib.demos;

import java.util.List;

import de.qfs.lib.axjlib.AXAPI;
import de.qfs.lib.axjlib.AXJLib;
import de.qfs.lib.axjlib.appkit.NSRunningApplication;
import de.qfs.lib.axjlib.ui.AXUIElement;
import de.qfs.lib.axjlib.ui.elements.AXApplication;
import de.qfs.lib.axjlib.ui.elements.AXMenuBar;
import de.qfs.lib.axjlib.ui.elements.AXSystemWide;
import de.qfs.lib.axjlib.ui.elements.AXWindow;
import de.qfs.lib.axjlib.values.CGSize;

public class SimpleDemo
{
    public static void main(String[] args) throws Exception
    {
        System.out.println("Trusted? " + AXAPI.isProcessTrusted(true));

        {
            final AXSystemWide sw = AXAPI.getSystemWideAccessibilityObject();
            final AXApplication focusedApplication = sw.getFocusedApplication();
            System.out.println(focusedApplication);
            if (focusedApplication != null) {
                final AXMenuBar menuBar = focusedApplication.getMenuBar();
                System.out.println(menuBar);
            }
        }

        {
            final AXUIElement element = AXAPI.elementAtPosition(100, 100);
            System.out.println(element);
            System.out.println(element.getAttributeNames());
            final AXWindow window = element instanceof AXWindow ? (AXWindow) element : (AXWindow) element.getWindow();
            window.setSize(new CGSize(600,600));
            window.setMinimized(true);
        }

        {
            for(final NSRunningApplication app: AXJLib.INSTANCE.runningApplications()) {
                System.out.println(app);
            }
        }

        {
            NSRunningApplication[] finders = AXJLib.INSTANCE.runningApplicationsWithBundleIdentifier("com.apple.finder");
            int pid = Integer.valueOf(finders[0].getProcessIdentifier());
            final AXApplication app = AXAPI.getApplicationAccessibilityObject(pid);
            app.setFrontmost(true);
            final List<AXUIElement> windows = app.getWindows();
            System.out.println(windows);
            final AXUIElement window = windows.get(0);
            if (window instanceof AXWindow) {
                ((AXWindow)window).raise();
            }
        }
    }
}
